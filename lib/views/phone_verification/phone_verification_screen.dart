import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kauction/app/routes/router.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/validators.dart';
import 'package:kauction/app/verify_phone/verify_member_exist_event.dart';
import 'package:kauction/app/verify_phone/verify_member_exist_state.dart';
import 'package:kauction/app/verify_phone/verify_user_exist_bloc.dart';
import 'package:kauction/views/custom_widgets/app_textfield.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';

class PhoneVerificationScreen extends StatefulWidget {
  const PhoneVerificationScreen({Key? key}) : super(key: key);

  @override
  _PhoneVerificationScreenState createState() =>
      _PhoneVerificationScreenState();
}

class _PhoneVerificationScreenState extends State<PhoneVerificationScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final resetPassFormKey = GlobalKey<FormState>();
  bool _autovalidate = false;
  FocusNode _focusphone = new FocusNode();
  final _phonecontroller = new TextEditingController();
  bool is_validated = false;
  bool isLoading = false;
  var deviceSize;
  String text_error = "";

  bool show = false;
  late VerifyMemberBloc _veriInBloc;
  final _textController = new TextEditingController();

  @override
  initState() {
    super.initState();
    _veriInBloc = BlocProvider.of<VerifyMemberBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    void showInSnackBar(String value) {
      _scaffoldKey.currentState?.showSnackBar(new SnackBar(
        content: new Text(
          value,
          style: TextStyle(),
        ),
        backgroundColor: Colors.red[900],
      ));
    }

    Widget phone_twidget = TextFieldWidget(
      "phone".tr(),
      _focusphone,
      _phonecontroller,
      TextInputType.phone,
      Validators.validatePhone,
    );
    submit() {
      final FormState? form = _formKey.currentState;
      if (form?.validate() == false) {
        _autovalidate = true; // Start validating on every change.
        //  showInSnackBar(
        //  "Veuillez corriger les erreurs en rouge");
      } else {
        _veriInBloc.add(
          VerifyMembreEventRequested(phone: _phonecontroller.text),
        );
      }
    }

    return Scaffold(
        backgroundColor: ColorsConst.col_app_white,
        appBar: KauctionAppBar(
          actions: [],
        ),
        body: BlocListener<VerifyMemberBloc, VerifyMemberState>(
            listener: (BuildContext context, VerifyMemberState verifyInState) {
          if (verifyInState is VerifyMemberFailure) {
            final message = verifyInState.errorMessage;

            showCupertinoDialog(
              context: context,
              builder: (context) => CupertinoAlertDialog(
                title: Text(''),
                content: Text(
                  message,
                  style: TextStyle(fontSize: 18.sp),
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('Ok'),
                  ),
                ],
              ),
            );
          } else if (verifyInState is VerifyMemberSuccess) {
            print(verifyInState.code);

            if (verifyInState.code == "")
              Navigator.pushNamed(context, AppRouter.LOGIN,
                  arguments: _phonecontroller.text);
            else
              Navigator.pushReplacementNamed(context, AppRouter.PHONECODE,
                  arguments: _phonecontroller.text);
          }
        }, child: BlocBuilder<VerifyMemberBloc, VerifyMemberState>(
                builder: (context, verifyInState) {
          return new Form(
              key: _formKey,
              //  autovalidate: _autovalidate,
              //onWillPop: _warnUserAboutInvalidData,
              child: Padding(
                  padding: EdgeInsets.all(24.w),
                  child: ListView(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(height: 60.h),
                      Text(
                        "phone".tr(),
                        style: AppTextStyles.title(context.locale),
                      ),
                      Container(height: 8.h),
                      Text("code".tr()),
                      Container(height: 54.h),
                      phone_twidget,
                      Container(height: 10.h),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                child: PrimaryButton(
                              textStyle: TextStyle(),
                              disabledColor: ColorsConst.col_app,
                              fonsize: 16.5.sp,
                              icon: "",
                              isLoading: verifyInState is VerifyMemberProgress,

                              colorText: ColorsConst.col_app_white,
                              prefix: Container(),

                              color: ColorsConst.col_app,
                              text: "send_verif".tr(),
                              //signInState is SignInInProgress,
                              onTap: () {
                                submit();
                              },
                            ))
                          ]),
                      Container(height: 80.h),
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 42.w),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: [
                              TextSpan(
                                  text: 'by'.tr(),
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: 'terms'.tr(),
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                      decoration: TextDecoration.underline)),
                              TextSpan(
                                  text: 'and'.tr(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: 'privacy'.tr(),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                      fontSize: 14.sp,
                                      decoration: TextDecoration.underline))
                            ]),
                          ))
                    ],
                  )));
        })));
  }
}
