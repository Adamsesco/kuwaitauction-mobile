import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pin_code_fields/flutter_pin_code_fields.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/routes/router.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/verify_phone/verify_member_exist_event.dart';
import 'package:kauction/app/verify_phone/verify_member_exist_state.dart';
import 'package:kauction/app/verify_phone/verify_user_exist_bloc.dart';
import 'package:kauction/model/repositories/verify_phone_repository.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';

class VerificationCodePage extends StatefulWidget {
  VerificationCodePage(this.phone, {Key? key}) : super(key: key);
  String phone;

  @override
  _VerificationCodePageState createState() => _VerificationCodePageState();
}

class _VerificationCodePageState extends State<VerificationCodePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();
  final resetPassFormKey = GlobalKey<FormState>();
  bool is_validated = false;
  bool isLoading = false;
  var deviceSize;
  String text_error = "";
  FocusNode _focusphone = new FocusNode();
  final _phonecontroller = new TextEditingController();
  bool show = false;
  late VerifyMemberBloc _veriInBloc;
  final _textController = new TextEditingController();
  VerifyNumberRepository verify_repos = VerifyNumberRepository();

  @override
  initState() {
    super.initState();
    _veriInBloc = BlocProvider.of<VerifyMemberBloc>(context);
  }

  bool load = false;

  submit() async {
    final FormState? form = _formKey.currentState;
    if (form?.validate() == false) {
      //  _autovalidate = true; // Start validating on every change.
      //  showInSnackBar(
      //  "Veuillez corriger les erreurs en rouge");
    } else {
      setState(() {
        load = true;
      });
      var res =
          await verify_repos.verify_code(widget.phone, _textController.text.substring(0,4));
      print(res);

      if (res["status"].toString() == "200") {
        Navigator.pushNamed(context, AppRouter.REGISTER,
            arguments: widget.phone);
      } else {}
      setState(() {
        load = false;
      });

      /* _veriInBloc.add(
        VerifyMembreEventRequestedCode(code: _phonecontroller.text,phone: widget.phone ),
      );*/
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorsConst.col_app_white,
        appBar: KauctionAppBar(
          actions: [],
        ),
        body: BlocListener<VerifyMemberBloc, VerifyMemberState>(
            listener: (BuildContext context, VerifyMemberState verifyInState) {
          if (verifyInState is VerifyMemberFailure) {
            final message = verifyInState.errorMessage;

            showCupertinoDialog(
              context: context,
              builder: (context) => CupertinoAlertDialog(
                title: Text(''),
                content: Text(
                  message,
                  style: TextStyle(fontSize: 18.sp),
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('Ok'),
                  ),
                ],
              ),
            );
          } else if (verifyInState is VerifyMemberSuccess) {
            print(verifyInState.code);
          }
        }, child: BlocBuilder<VerifyMemberBloc, VerifyMemberState>(
                builder: (context, verifyInState) {
          return new Form(
              key: _formKey,
              //  autovalidate: _autovalidate,
              //onWillPop: _warnUserAboutInvalidData,
              child: Padding(
                  padding: EdgeInsets.all(24.w),
                  child: ListView(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(height: 60.h),
                      Text(
                        "enter_code".tr(),
                        style: AppTextStyles.title(context.locale),
                      ),
                      Container(height: 8.h),
                      Text("enter_Digit".tr()),
                      Container(height: 54.h),
                      // phone_twidget,
                      Container(height: 10.h),
                      PinCodeFields(
                        controller: _textController,
                        length: 4,
                        fieldBorderStyle: FieldBorderStyle.Square,
                        responsive: false,
                        fieldHeight: 50.0.w,
                        fieldWidth: 50.0.w,
                        borderWidth: 1.0,
                        activeBorderColor: ColorsConst.border_color,
                        activeBackgroundColor: ColorsConst.fill_color,
                        borderRadius: BorderRadius.circular(12.0.r),
                        keyboardType: TextInputType.number,
                        autoHideKeyboard: false,
                        fieldBackgroundColor: ColorsConst.fill_color,
                        borderColor: ColorsConst.border_color,
                        textStyle: TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                        ),
                        onComplete: (output) {
                          // Your logic with pin code
                          print(output);
                        },
                      ),
                      Container(
                        height: 36.h,
                      ),
                      Center(
                        child: Text(
                          "didnt".tr(),
                          style: TextStyle(fontWeight: FontWeight.w400),
                        ),
                      ),
                      Container(height: 4.h),
                      FlatButton(
                          onPressed: () {},
                          child: Text(
                            "resend".tr(),
                            style: AppTextStyles.title2(context.locale),
                          )),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                child: PrimaryButton(
                              textStyle: TextStyle(),
                              disabledColor: ColorsConst.col_app,
                              fonsize: 16.5.sp,
                              icon: "",
                              isLoading: load,

                              colorText: ColorsConst.col_app_white,
                              prefix: Container(),

                              color: ColorsConst.col_app,
                              text: "confirm".tr(),
                              //signInState is SignInInProgress,
                              onTap: () {
                                submit();
                              },
                            ))
                          ]),
                    ],
                  )));
        })));
  }
}
