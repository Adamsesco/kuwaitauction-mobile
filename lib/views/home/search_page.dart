import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/model/repositories/product_repository.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/custom_widgets/search_item.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:easy_localization/easy_localization.dart';
class SearchPage extends StatefulWidget {
  SearchPage(this.text, {Key? key}) : super(key: key);
  String text;

  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<SearchPage> {
  List<Product> products = [];
  ProductSServices prodserv = new ProductSServices();
  final TextEditingController _searchQuery = new TextEditingController();
  bool load = true;
  late String token;



  static const pattern = [
    QuiltedGridTile(1, 2),
    QuiltedGridTile(2, 2),
    QuiltedGridTile(1, 1),
    QuiltedGridTile(1, 1),
  ];

  //type

  get_product_by_fav() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    token = prefs.getString("token").toString();
    List<Product> prods = await prodserv.get_product_result_search(widget.text);
    if (!this.mounted) return;
    setState(() {
      products = prods;

      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_product_by_fav();
  }

  @override
  Widget build(BuildContext context) {
    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(23),
          bottom: ScreenUtil().setHeight(23),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_black,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget page = load == true
            ? Center(
                child: CupertinoActivityIndicator(),
              )
            : products.isEmpty
                ? Center(
                    child: Text("no results found".tr()),
                  )
                /**
        MasonryGridView.count(
        crossAxisCount: 4,
        mainAxisSpacing: 4,
        crossAxisSpacing: 4,
        itemBuilder: (context, index) {
        return Tile(
        index: index,
        extent: (index % 5 + 1) * 100,
        );
        },
        )
     */
                : GridView.custom(
                    gridDelegate: SliverQuiltedGridDelegate(
                      crossAxisCount: 4,
                      mainAxisSpacing: 4,
                      crossAxisSpacing: 4,
                      repeatPattern: QuiltedGridRepeatPattern.inverted,
                      pattern: pattern,
                    ),
                    childrenDelegate: SliverChildBuilderDelegate(
                      (context, index) {
                        final tile = pattern[index % pattern.length];
                        return ImageTile(
                          product: products[index],
                          index: index,
                          width: tile.crossAxisCount * 100,
                          height: tile.mainAxisCount * 100,
                        );
                      },
                      childCount: products.length,

                    ),
                  ) /*MasonryGridView.count(
                    crossAxisCount: 3,
                  //  physics: NeverScrollableScrollPhysics(),
                    crossAxisSpacing: ScreenUtil().setWidth(12).toDouble(),
                    mainAxisSpacing: ScreenUtil().setHeight(12).toDouble(),
                    itemCount: products.length,
                    itemBuilder: (context, index) {
                      return
                          Container(
                              color: Colors.redAccent,
                              height: (index % 5 + 1) * 50,
                            /*ProductItem(products[index])*/);
                    }) */ /*ListView(
                children: products.map((e) => ProductItem(e)).toList(),
              )*/
        ;

    return Scaffold(
        appBar: KauctionAppBar(
            title_widget: Container(
              height: 52.h,
              margin: EdgeInsets.symmetric(vertical: 4.h),
              padding:
                  const EdgeInsets.symmetric(vertical: 2.0, horizontal: 2.0),
              child: TextField(


                controller: _searchQuery,
                onSubmitted: (val) {
                  widget.text = val;
                  get_product_by_fav();
                },
                decoration: InputDecoration(
                    counterStyle: TextStyle(color: ColorsConst.col_app),
                    isDense: true,
                    contentPadding: EdgeInsets.symmetric(horizontal: 6.0),
                    hintText: 'search'.tr(),
                    enabledBorder: OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide:
                          BorderSide(color: ColorsConst.col_grey, width: 0.0),
                      borderRadius: BorderRadius.circular(32.0.r),
                    ),
                    hintStyle: TextStyle(color: ColorsConst.col_grey),
                    suffixIcon: Padding(
                      padding: const EdgeInsetsDirectional.only(end: 12.0),
                      child: InkWell(
                          onTap: (){
                            widget.text = _searchQuery.text;
                            get_product_by_fav();
                          },
                          child: Icon(
                        Icons.search,
                        color: ColorsConst.col_grey_fon,
                        size: 24.0,
                      )), // icon is 48px widget.
                    ),
                    border: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: ColorsConst.col_app, width: 0.0),
                      borderRadius: BorderRadius.circular(60.0),
                    ),
                    filled: true),
              ),
            ),
            /*Text(
              widget.text,
              style: TextStyle(color: Colors.black),
            )*/
            actions: [],
            returnn: true),
        body: page);
  }
}

class Tile extends StatelessWidget {
  const Tile({
    Key? key,
    required this.index,
    this.extent,
    this.backgroundColor,
    this.bottomSpace,
  }) : super(key: key);

  final int index;
  final double? extent;
  final double? bottomSpace;
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    final child = Container(
      color: backgroundColor,
      height: extent,
      child: Center(
        child: CircleAvatar(
          minRadius: 20,
          maxRadius: 20,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          child: Text('$index', style: const TextStyle(fontSize: 20)),
        ),
      ),
    );

    if (bottomSpace == null) {
      return child;
    }

    return Column(
      children: [
        Expanded(child: child),
        Container(
          height: bottomSpace,
          color: Colors.green,
        )
      ],
    );
  }
}
