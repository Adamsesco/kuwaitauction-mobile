import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/model/repositories/product_repository.dart';
import 'package:kauction/views/home/favorites/product_item.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';
class FavoritesPage extends StatefulWidget {
  const FavoritesPage({Key? key}) : super(key: key);

  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage> {
  List<Product> products = [];
  ProductSServices prodserv = new ProductSServices();
  bool load = true;
  late String token;

  //type

  get_product_by_fav() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    token = prefs.getString("token").toString();
    List<Product> prods = await prodserv.get_product_by_favorites(token);
    if (!this.mounted) return;
    setState(() {
      products = prods;

      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_product_by_fav();
  }

  @override
  Widget build(BuildContext context) {
    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(23),
          bottom: ScreenUtil().setHeight(23),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_black,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    return load == true
        ? Center(
            child: CupertinoActivityIndicator(),
          )
        : products.isEmpty
            ? Center(
                child: Text("no results found".tr()),
              )
            : ListView(
                children: products.map((e) => ProductItem(e)).toList(),
              );
  }
}
