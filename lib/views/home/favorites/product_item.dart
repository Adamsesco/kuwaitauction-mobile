import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/views/home/home_first_screen/details_products.dart';
import 'package:kauction/views/home/home_first_screen/favorite_widget.dart';
import 'package:easy_localization/easy_localization.dart';

class ProductItem extends StatefulWidget {
  ProductItem(this.product, {Key? key}) : super(key: key);

  Product product;

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  String _date = "";

  getDuration() {
    if (widget.product.close_date == null)
      return "";
    else {
      Duration d = widget.product.close_date!.difference(DateTime.now());

      if (d.isNegative == true)
        return "";
      else {
        return (d.inDays.toString() + " d" + d.inHours.toString() + " h");
      }
    }
  }

  @override
  void initState() {
    super.initState();

    print(widget.product.date.toString());
  }

  /**
      date
   */
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
            return DetailsProduct(widget.product);
          }));
        },
        child: Container(
            padding: EdgeInsets.only(left: ScreenUtil().setWidth(16)),
            child: Column(
              children: [
                Divider(),
                Row(
                  children: [
                    ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(16)),
                        child: Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage(
                                        "assets/images/placeholder.png"),
                                    fit: BoxFit.contain)),
                            child: widget.product.images.isEmpty
                                ? Container()
                                : widget.product.images[0].url == ""
                                    ? Container()
                                    : FadeInImage.assetNetwork(
                                        image: widget.product.images[0].url,
                                        placeholder:
                                            "assets/images/placeholder.png",
                                        width: ScreenUtil().setWidth(111),
                                        height: ScreenUtil().setHeight(110),
                                        fit: BoxFit.cover,
                                      ))),
                    Container(
                      width: ScreenUtil().setWidth(12),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            width: ScreenUtil().setWidth(220),
                            child: Text(
                              widget.product.name.toString(),
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                height: 1.4,
                                fontWeight: FontWeight.w800,
                                fontSize: ScreenUtil().setSp(13.5),
                              ),
                            )),
                        Container(height: 10.h),

                        Container(
                            width: ScreenUtil().setWidth(220),
                            child: Text(
                              widget.product.bids.length.toString() +
                                  " Bids - ends " +
                                  getDuration(),
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: ColorsConst.col_app_blue,
                                height: 1.4,
                                fontSize: ScreenUtil().setSp(11.5),
                              ),
                            )),

                        /* Container(
                            height: ScreenUtil().setHeight(40),
                            width: ScreenUtil().setWidth(220),
                            child: Text(
                              product.description.toString(),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                height: 1.4,
                                fontWeight: FontWeight.w400,
                                fontSize: ScreenUtil().setSp(11.5),
                              ),
                            )),*/
                        Container(
                          height: ScreenUtil().setHeight(2),
                        ),
                        Container(
                            width: ScreenUtil().setWidth(220),
                            child: Row(
                              children: [
                                Text(
                                  (widget.product.last_price == null ||
                                          widget.product.last_price == "")
                                      ? ""
                                      : double.parse(widget.product.last_price)
                                              .toStringAsFixed(2) +
                                          ((widget.product.last_price
                                                      .toString() ==
                                                  "")
                                              ? ""
                                              : " KD"),
                                  style: TextStyle(
                                      color: ColorsConst.col_black2,
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins_bold"
                                              : "cairo_bold",
                                      fontSize: ScreenUtil().setSp(17.0),
                                      fontWeight: FontWeight.w800),
                                )
                              ],
                            )),
                        //Expanded(child: Container(),),
                      ],
                    ),
                    FavoriteButton(widget.product)
                  ],
                ),
              ],
            )));
  }
}
