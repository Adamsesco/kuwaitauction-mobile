import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/app/utils/validators.dart';
import 'package:kauction/views/custom_widgets/app_textfield.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/views/custom_widgets/round_button_icon.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:kauction/views/notifi/notif_list.dart';

class PasswordReset extends StatefulWidget {
  const PasswordReset({Key? key}) : super(key: key);

  @override
  _PasswordResetState createState() => _PasswordResetState();
}

class _PasswordResetState extends State<PasswordReset> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _usernameController = new TextEditingController();
  RestService restservice = new RestService();

  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _telController = new TextEditingController();
  FocusNode _focuspass2 = new FocusNode();

  FocusNode _focuspass = new FocusNode();
  TextEditingController _passwordController2 = new TextEditingController();

  bool _obscureText = true;
  bool _obscureText2 = true;
  bool _obscureText3 = true;

  //AppService ap = new AppService();
  FocusNode _focusemail = new FocusNode();
  FocusNode _focustel = new FocusNode();

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  void _toggle3() {
    setState(() {
      _obscureText3 = !_obscureText3;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget pass_twidget = TextFieldWidget("password".tr(), _focuspass,
        _passwordController, TextInputType.text, Validators.validatePassword,
        obscure: _obscureText,
        prefixWidget: Container(
          padding: EdgeInsets.all(4.w),
          height: 36.0.h,
          width: 36.0.w,
          child: RawMaterialButton(
            onPressed: _toggle,
            child: SvgPicture.asset(
              _obscureText
                  ? "assets/icons/show.svg"
                  : "assets/icons/hidden.svg",
              color: ColorsConst.col_grey_fon,
            ),
          ),
        ));

    Widget pass_twidget2 = TextFieldWidget("New password".tr(), _focuspass,
        _passwordController, TextInputType.text, Validators.validatePassword,
        obscure: _obscureText3,
        prefixWidget: Container(
          padding: EdgeInsets.all(4.w),
          height: 36.0.h,
          width: 36.0.w,
          child: RawMaterialButton(
            onPressed: _toggle3,
            child: SvgPicture.asset(
              _obscureText3
                  ? "assets/icons/show.svg"
                  : "assets/icons/hidden.svg",
              color: ColorsConst.col_grey_fon,
            ),
          ),
        ));

    Widget confirm_twidget = TextFieldWidget(
        "confirmpass".tr(),
        _focuspass2,
        _passwordController2,
        TextInputType.text,
        Validators.validatePassword,
        obscure: _obscureText2,
        prefixWidget: Container(
          padding: EdgeInsets.all(4.w),
          height: 36.0.h,
          width: 36.0.w,
          child: RawMaterialButton(
            onPressed: _toggle2,
            child: SvgPicture.asset(
              _obscureText2
                  ? "assets/icons/show.svg"
                  : "assets/icons/hidden.svg",
              color: ColorsConst.col_grey_fon,
            ),
          ),
        ));

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1.4,
      color: ColorsConst.col_grey.withOpacity(0.15),
    );

    return Scaffold(
      backgroundColor: ColorsConst.col_app_white,
      appBar: KauctionAppBar(
          title_widget: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Account".tr().toUpperCase(),
                style: AppTextStyles.appBar_tt(context.locale),
              ),
              Container(
                height: 0.h,
              ),
              Text("Password reset".tr(), style: AppTextStyles.small_title2)
            ],
          ),
          actions: [

            RoundButtonIcon(onPress: () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                    return NotificationsList();
                  }));
            }, icon: "assets/icons/notif.svg"),
            Container(width: 10.w),
          ],
          returnn: true),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        children: [
          Container(
            height: 8.h,
          ),
          divid,
          Container(
            height: 24.h,
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              child: Text(
                "Password reset".tr(),
                maxLines: 8,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  height: 1.2,
                  color: const Color(0xff323045),
                  fontFamily: "poppins_bold",
                  fontWeight: FontWeight.w700,
                  fontSize: ScreenUtil().setSp(17.5),
                ),
              )),
          Container(
            height: 24.h,
          ),
          pass_twidget,
          Container(height: 24.h),
          pass_twidget2,
          Container(height: 24.h),
          confirm_twidget
        ],
      ),
    );
  }
}
