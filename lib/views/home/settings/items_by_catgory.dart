import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/model/repositories/product_repository.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/custom_widgets/round_button_icon.dart';
import 'package:kauction/views/home/favorites/product_item.dart';
import 'package:kauction/views/notifi/notif_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';

class IByCategorytemsPage extends StatefulWidget {
  IByCategorytemsPage(this.id, this.name, this.search,{Key? key}) : super(key: key);
  String id;
  String name;
  String search ;


  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<IByCategorytemsPage> {
  List<Product> products = [];
  ProductSServices prodserv = new ProductSServices();
  bool load = true;
  late String token;
  final TextEditingController _searchQuery = new TextEditingController();

  //type

  get_product_by_fav() async {
    List<Product> prods = await prodserv.get_product_by_id(widget.id);
    if (!this.mounted) return;
    setState(() {
      products = prods;

      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.search != "")
      {
        _searchQuery.text = widget.search;
        get_product_search();
      }
    else {
      get_product_by_fav();
    }
  }

  get_product_search() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    token = prefs.getString("token").toString();
    List<Product> prods =
        await prodserv.get_product_result_search(_searchQuery.text);
    if (!this.mounted) return;
    setState(() {
      products = prods;

      load = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget bar = Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 16.w,
          ),
          InkWell(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    builder: (context) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(
                            //   leading: new Icon(Icons.photo),
                            title: new Text('by_old'.tr()),
                            onTap: () {
                              Navigator.pop(context);
                              setState(() {
                                products.sort((a, b) {
                                  DateTime adate =
                                      a.date!; //before -> var adate = a.expiry;
                                  DateTime bdate =
                                      b.date!; //before -> var bdate = b.expiry;
                                  return adate.compareTo(
                                      bdate); //to get the order other way just switch `adate & bdate`
                                });
                              });
                            },
                          ),
                          ListTile(
                            title: new Text('by_new'.tr()),
                            onTap: () {
                              Navigator.pop(context);
                              setState(() {
                                products.sort((a, b) {
                                  DateTime adate =
                                      a.date!; //before -> var adate = a.expiry;
                                  DateTime bdate =
                                      b.date!; //before -> var bdate = b.expiry;
                                  return bdate.compareTo(
                                      adate); //to get the order other way just switch `adate & bdate`
                                });
                              });
                            },
                          ),
                          ListTile(
                            title: new Text('by_ex'.tr()),
                            onTap: () {
                              Navigator.pop(context);
                              setState(() {
                                products.sort((a, b) {
                                  DateTime adate = a
                                      .close_date!; //before -> var adate = a.expiry;
                                  DateTime bdate = b
                                      .close_date!; //before -> var bdate = b.expiry;
                                  return bdate.compareTo(
                                      adate); //to get the order other way just switch `adate & bdate`
                                });
                              });
                            },
                          ),
                          ListTile(
                            title: new Text('asc_price'.tr()),
                            onTap: () {
                              Navigator.pop(context);
                              setState(() {
                                products.sort((a, b) {
                                  var adate = double.parse(a
                                      .last_price); //before -> var adate = a.expiry;
                                  var bdate = double.parse(b
                                      .last_price); //before -> var bdate = b.expiry;
                                  return adate.compareTo(
                                      bdate); //to get the order other way just switch `adate & bdate`
                                });
                              });
                            },
                          ),
                          ListTile(
                              title: new Text('desc_price'.tr()),
                              onTap: () {
                                Navigator.pop(context);
                                setState(() {
                                  products.sort((a, b) {
                                    var adate = double.parse(a
                                        .last_price); //before -> var adate = a.expiry;
                                    var bdate = double.parse(b
                                        .last_price); //before -> var bdate = b.expiry;
                                    return bdate.compareTo(
                                        adate); //to get the order other way just switch `adate & bdate`
                                  });
                                });
                              }),
                        ],
                      );
                    });
                /* setState(() {

             });*/
              },
              child: Column(
                children: [
                  Container(
                    height: 8.h,
                  ),
                  SvgPicture.asset("assets/icons/sort.svg"),
                  Text("sort".tr())
                ],
              )),
          Container(
            width: 8.w,
          ),
          Expanded(
              child: Container(
                  // width: MediaQuery.of(context).size.width * 0.8,
                  height: 52.h,
                  margin: EdgeInsets.symmetric(vertical: 4.h),
                  padding: const EdgeInsets.symmetric(
                      vertical: 2.0, horizontal: 2.0),
                  child: TextField(
                    controller: _searchQuery,
                    onSubmitted: (val) {
                      get_product_search();
                    },
                    decoration: InputDecoration(
                        counterStyle: TextStyle(color: ColorsConst.col_app),
                        isDense: true,
                        contentPadding: EdgeInsets.symmetric(horizontal: 6.0),
                        hintText: '  ' + "search".tr(),
                        enabledBorder: OutlineInputBorder(
                          // width: 0.0 produces a thin "hairline" border
                          borderSide: BorderSide(
                              color: ColorsConst.col_grey, width: 0.0),
                          borderRadius: BorderRadius.circular(32.0.r),
                        ),
                        hintStyle: TextStyle(color: ColorsConst.col_grey),
                        suffixIcon: Padding(
                          padding: const EdgeInsetsDirectional.only(end: 12.0),
                          child: InkWell(
                              onTap: () {
                                // widget.text = _searchQuery.text;
                                get_product_search();
                              },
                              child: Icon(
                                Icons.search,
                                color: ColorsConst.col_grey_fon,
                                size: 24.0,
                              )), // icon is 48px widget.
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: ColorsConst.col_app, width: 0.0),
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        filled: true),
                  ))),
          Container(
            width: 8.w,
          ),
          Column(
            children: [
              Container(
                height: 8.h,
              ),
              SvgPicture.asset("assets/icons/filter.svg"),
              Text("filter".tr())
            ],
          ),
          Container(
            width: 16.w,
          ),
        ]);
    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(23),
          bottom: ScreenUtil().setHeight(23),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_black,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    return Scaffold(
        appBar: KauctionAppBar(
            title_widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.name,
                  style: AppTextStyles.appBar_tt(context.locale),
                ),
              ],
            ),
            actions: [
              RoundButtonIcon(onPress: () {
                Navigator.push(context,
                    new MaterialPageRoute(builder: (BuildContext context) {
                      return NotificationsList();
                    }));
              }, icon: "assets/icons/notif.svg"),
              Container(width: 10.w),
            ],
            returnn: true),
        body: load == true
            ? Center(
                child: CupertinoActivityIndicator(),
              )
            : products.isEmpty
                ? Center(
                    child: Text("No item found !"),
                  )
                : Column(children: [
                    bar,
                    Expanded(
                        child: ListView(
                      children: products.map((e) => ProductItem(e)).toList(),
                    ))
                  ]));
  }
}
