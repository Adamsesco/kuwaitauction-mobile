import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/app/utils/validators.dart';
import 'package:kauction/languages_page.dart';
import 'package:kauction/messenger/util/settings.dart';
import 'package:kauction/model/models/user.dart';
import 'package:kauction/model/repositories/publish_repository.dart';
import 'package:kauction/views/custom_widgets/app_textfield.dart';
import 'package:kauction/views/custom_widgets/round_button_icon.dart';
import 'package:kauction/views/home/home_screen.dart';
import 'package:kauction/views/home/settings/face_id_page.dart';
import 'package:kauction/views/home/settings/my_items.dart';
import 'package:kauction/views/home/settings/password_reset_page.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _telController = new TextEditingController();
  TextEditingController _passwordController1 = new TextEditingController();
  FocusNode _focuspass = new FocusNode();
  FocusNode _focuspass1 = new FocusNode();
  RestService restservice = new RestService();
  bool show = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool _autovalidate = false;
  FocusNode _focusname = new FocusNode();
  FocusNode _focusemail = new FocusNode();
  FocusNode _focustel = new FocusNode();
  Settings_Services settings = new Settings_Services();

  /* go_settings() {
    Navigator.push(context,
        new MaterialPageRoute(builder: (BuildContext context) {
          return SettingsList();
        }));
  }*/

  Future compress_image(File image) async {
    File compressedFile =
    (await FlutterNativeImage.compressImage(image.path, quality: 70))
    as File;
    await save_image(compressedFile);

  }



  bool load = true;
  late User user =
      new User(image: "", created: DateTime.now(), userename: "", email: "");

  getUserinfo() async {
    setState(() {
      load = true;
    });

    var a = await settings.user_info();
    if (!this.mounted) return;
    setState(() {
      user = a;
      load = false;
      _nameController.text = user.userename;
      _telController.text = user.mobile;
      _emailController.text = user.email;
    });
  }

  logout() {
    Alert(
        context: context,
        title: "logout".tr(),
        desc: "logout title".tr(),
        buttons: [
          DialogButton(
              color: ColorsConst.col_app,
              child: Text(
                "confirm".tr(),
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
              onPressed: () async {
                Navigator.pop(context);

                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.clear();

                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => HomeScreen()),
                    (Route<dynamic> route) => false);
              })
        ]).show();
  }

  @override
  void initState() {
    super.initState();
    getUserinfo();
  }

  Future _cropImage(image) async {
    File croppedFile = await ImageCropper.cropImage(
      sourcePath: image.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
    );
    if (croppedFile != null) {
      image = croppedFile;
      //ImageProperties properties = await FlutterNativeImage.getImageProperties(image.path);
      File compressedFile =
          await FlutterNativeImage.compressImage(image.path, quality: 70);
      save_image(compressedFile);
    }
  }

  bool uploading = false;

  save_image(image) async {
    setState(() {
      uploading = true;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token").toString();
    int timestamp = new DateTime.now().millisecondsSinceEpoch;
    String imge = await PublishRepository.upload_image(image);

    setState(() {
      user.image = imge;
    });
    var js = {
       "image": user.image.toString(),
      "token": token,
      "username": user.userename,
      "phone": user.mobile,
      "email": user.email
      // "password": user.password
    };

    var a = await restservice.post("editProfile/", js);
    print(a);

    if (!mounted) return;

    setState(() {
      // user.image = val.toString();

      //appstate change user
      uploading = false;
    });
  }

  _handleCameraButtonPressed() async {
    Navigator.of(context).pop(true);
    var image =
        await ImagePicker.platform.pickImage(source: ImageSource.camera);
    _cropImage(File(image!.path));
  }

  _handleGalleryButtonPressed() async {
    Navigator.of(context).pop(true);
    PickedFile? image =
        await ImagePicker.platform.pickImage(source: ImageSource.gallery);
    _cropImage(File(image!.path));
  }

  open_bottomsheet() {
    showModalBottomSheet<bool>(
        context: context,
        builder: (BuildContext context) {
          return new Container(
              height: 112.0,
              child: new Container(
                  // padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                  child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                    new ListTile(
                        onTap: () async{
                          PickedFile? image = (await ImagePicker.platform
                              .pickImage(source: ImageSource.camera));
                          compress_image(File(image!.path));
                        },
                        title: new Text("Camera".tr())),
                    new ListTile(
                        onTap:( ) async{

                          PickedFile? image = (await ImagePicker.platform
                              .pickImage(source: ImageSource.gallery));
                          compress_image(File(image!.path));
                        },
                        title: new Text("Photo Gallery".tr())),
                  ])));
        });
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text1 = TextStyle(
        color: ColorsConst.col_black2,
        fontSize: ScreenUtil().setSp(17),
        fontWeight: FontWeight.w800);

    TextStyle text1_btn = TextStyle(
        color: ColorsConst.col_black2,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);
    TextStyle text2 = TextStyle(
        color: ColorsConst.col_black2,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w400);

    TextStyle text3 = TextStyle(
        color: ColorsConst.col_black2,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w500);

    TextStyle text4 = TextStyle(
        color: ColorsConst.col_black2,
        fontSize: ScreenUtil().setSp(16),
        fontWeight: FontWeight.w600);

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1.4,
      color: ColorsConst.col_grey.withOpacity(0.15),
    );

    Widget row_widget(String text, String image, onPressed) => InkWell(
          child: Padding(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(12),
                bottom: ScreenUtil().setHeight(12),
                left: ScreenUtil().setWidth(12),
                right: ScreenUtil().setWidth(12),
              ),
              child: Row(children: [
                SvgPicture.asset(image),
                Container(
                  width: ScreenUtil().setWidth(12),
                ),
                Expanded(
                    child: Text(
                  text,
                  style: text4,
                )),
                SvgPicture.asset("assets/images/icons/arrow.svg"),
              ])),
          onTap: () {
            onPressed();
          },
        );

    Widget email_twidget = TextFieldWidget(
      "Email".tr(),
      _focusemail,
      _emailController,
      TextInputType.emailAddress,
      Validators.validateEmail,
    );

    Widget name_twidget = TextFieldWidget(
      "Username".tr(),
      _focusname,
      _nameController,
      TextInputType.text,
      Validators.validatefirstname,
    );

    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 16.w),
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(width: 16.w),
                GestureDetector(
                  child: SizedBox(
                      height: ScreenUtil().setWidth(52),
                      width: ScreenUtil().setWidth(52),
                      child: Stack(
                        children: <Widget>[
                          CircleAvatar(
                            radius: ScreenUtil().setWidth(43.5),
                            child: ClipOval(
                              child: Image.network(
                                user.image,
                                height: ScreenUtil().setWidth(47),
                                width: ScreenUtil().setWidth(47),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          uploading
                              ? Align(
                                  alignment: Alignment.center,
                                  child: CupertinoActivityIndicator(),
                                )
                              : Container(),
                          Positioned(
                              top: 1,
                              right: 1,
                              child: Container(
                                height: 22,
                                width: 22,
                                //  padding: EdgeInsets.all(8),
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.camera_alt,
                                      color: Colors.black,
                                      size: 14,
                                    )),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(20))),
                              )),
                        ],
                      )),
                  onTap: () {
                    open_bottomsheet();
                  },
                ),
                Container(width: 28.w),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "welcome2".tr(),
                      style: text2,
                    ),
                    Text(user.userename, style: text1),
                    Container(
                      height: 2,
                    ),
                    SizedBox(
                      height: ScreenUtil().setHeight(8),
                    ),
                  ],
                ),
                Expanded(child: Container()),
                RoundButtonIcon(
                    onPress: () {
                      logout();
                    },
                    icon: "assets/icons/logout.svg"),
              ],
            ),
            SizedBox(
              height: ScreenUtil().setHeight(30),
            ),
            Padding(
              padding: EdgeInsets.only(left: 16.w, bottom: 12.h),
              child: Text("Your informations".tr()),
            ),
            name_twidget,
            Container(
              height: 10.h,
            ),
            email_twidget,
            Container(
              height: 38.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 16.w, bottom: 12.h),
              child: Text("Security settings".tr()),
            ),
            row_widget("Password reset".tr(), "assets/icons/passr.svg", () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return PasswordReset();
              }));
            }),
            divid,
            /*row_widget("Face ID sign in".tr(), "assets/icons/face.svg", () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return FaceIdPage();
              }));
            }),*/
            divid,
            Container(
              height: 38.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 16.w, bottom: 12.h),
              child: Text("Operations".tr()),
            ),
            row_widget("My items".tr(), "assets/icons/list1.svg", () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return MyOtemsPage();
              }));
            }),
            divid,
            row_widget("My purchases".tr(), "assets/icons/purchase.svg", null),
            divid,
            SizedBox(
              height: ScreenUtil().setHeight(12),
            ),
            row_widget("changel".tr(), "assets/icons/list1.svg", () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                    return LanguagesPage();
                  }));
            }),
            divid,
            SizedBox(
              height: ScreenUtil().setHeight(50),
            ),

            /**
             *
             */
          ],
        )
      ],
    );
  }
}
