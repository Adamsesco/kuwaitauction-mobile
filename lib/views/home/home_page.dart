import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/category.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/model/repositories/product_repository.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/custom_widgets/search_item.dart';
import 'package:kauction/views/home/home_first_screen/details_products.dart';
import 'package:kauction/views/home/search_page.dart';
import 'package:kauction/views/home/settings/items_by_catgory.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePagePageState createState() => _HomePagePageState();
}

class _HomePagePageState extends State<HomePage> {
  List<CategoryModel> cats = [];
  ProductSServices prodserv = new ProductSServices();
  final TextEditingController _searchQuery = new TextEditingController();
  bool load = true;
  late String token;

  String text = "";

  static const pattern = [
    QuiltedGridTile(1, 2),
    QuiltedGridTile(2, 2),
    QuiltedGridTile(1, 1),
    QuiltedGridTile(1, 1),
  ];

  //type

  get_product_by_fav() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    token = prefs.getString("token").toString();
    List<CategoryModel> prods = await prodserv.homescreen("");
    if (!this.mounted) return;
    setState(() {
      cats = prods;

      load = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_product_by_fav();
  }

  @override
  Widget build(BuildContext context) {
    Widget page = load == true
        ? Center(
            child: CupertinoActivityIndicator(),
          )
        : cats.isEmpty
            ? Center(
                child: Text("no results found".tr()),
              )
            /**
        MasonryGridView.count(
        crossAxisCount: 4,
        mainAxisSpacing: 4,
        crossAxisSpacing: 4,
        itemBuilder: (context, index) {
        return Tile(
        index: index,
        extent: (index % 5 + 1) * 100,
        );
        },
        )
     */
            : Padding(
                padding: EdgeInsets.symmetric(horizontal: 12.w),
                child: MasonryGridView.count(
                  crossAxisCount: 2,
                  mainAxisSpacing: 12.w,
                  crossAxisSpacing: 12.h,
                  itemCount: cats.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                        onTap: () {
                          Navigator.push(context, new MaterialPageRoute(
                              builder: (BuildContext context) {
                            return IByCategorytemsPage(
                                cats[index].category_id,
                                context.locale.toString() == "en"
                                    ? cats[index].category_name
                                    : cats[index].category_name_ar,
                                "");
                          }));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: ColorsConst.col_grey.withOpacity(0.3),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: Column(
                            children: [
                              Container(
                                height: 32.h,
                              ),
                              ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  child: Image.network(
                                    cats[index].image,
                                    fit: BoxFit.fitWidth,
                                    width:
                                        MediaQuery.of(context).size.width * 0.4,
                                  )),
                              Container(
                                height: 12.h,
                              ),
                              Text(
                                  cats[index].category_name == null
                                      ? ""
                                      : context.locale.toString() == "en"
                                          ? cats[index]
                                              .category_name
                                              .toString()
                                              .toUpperCase()
                                          : cats[index].category_name_ar,
                                  style:
                                      AppTextStyles.cat_title2(context.locale)),
                              Container(
                                height: 0.h,
                              ),
                              Text(
                                  cats[index].items.toString() +
                                      " total items".toUpperCase(),
                                  style: AppTextStyles.small_title),
                              Container(
                                height: 24.h,
                              ),
                            ],
                          ),
                          //height: (index % 5 + 1) * 100,
                        ));
                  },
                ));

    return Container(
        child: Column(children: [
      Row(children: [
        Container(width: 12.w),
        Expanded(
            child: InkWell(
                onTap: () {
                  Navigator.push(context,
                      new MaterialPageRoute(builder: (BuildContext context) {
                    return SearchPage(_searchQuery.text);
                  }));
                },
                child: Container(
                  height: 52.h,
                  margin: EdgeInsets.symmetric(vertical: 4.h),
                  padding: const EdgeInsets.symmetric(
                      vertical: 2.0, horizontal: 2.0),
                  child: IgnorePointer(
                      child: TextField(
                    controller: _searchQuery,
                    onSubmitted: (val) {
                      text = val;
                      Navigator.push(context, new MaterialPageRoute(
                          builder: (BuildContext context) {
                        return SearchPage(_searchQuery.text);
                      }));
                      /* Navigator.push(context,
                  new MaterialPageRoute(
                      builder: (BuildContext context) {
                        return IByCategorytemsPage(
                           "", "", text);
                      }));*/
                      // get_product_by_fav();
                    },
                    decoration: InputDecoration(
                        counterStyle: TextStyle(color: ColorsConst.col_app),
                        isDense: true,
                        contentPadding: EdgeInsets.symmetric(horizontal: 6.0),
                        hintText: 'search'.tr(),
                        enabledBorder: OutlineInputBorder(
                          // width: 0.0 produces a thin "hairline" border
                          borderSide: BorderSide(
                              color: ColorsConst.col_grey, width: 0.0),
                          borderRadius: BorderRadius.circular(32.0.r),
                        ),
                        hintStyle: TextStyle(color: ColorsConst.col_grey),
                        prefixIcon: Padding(
                          padding: const EdgeInsetsDirectional.only(
                              end: 8.0, start: 4.0),
                          child: InkWell(
                              onTap: () {
                                text = _searchQuery.text;
                                get_product_by_fav();
                              },
                              child: Padding(
                                  padding: EdgeInsets.all(8.w),
                                  child: SvgPicture.asset(
                                    "assets/icons/search.svg",
                                    color: ColorsConst.col_grey_fon,
                                    width: 24.0.w,
                                  ))), // icon is 48px widget.
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: ColorsConst.col_app, width: 0.0),
                          borderRadius: BorderRadius.circular(60.0),
                        ),
                        filled: true),
                  )),
                ))),
        Container(width: 12.w),
        Column(
          children: [
            SvgPicture.asset("assets/icons/filter.svg"),
          ],
        ),
        Container(width: 12.w),
      ]),
      Expanded(child: page)
    ]));
  }
}

class Tile extends StatelessWidget {
  const Tile({
    Key? key,
    required this.index,
    this.extent,
    this.backgroundColor,
    this.bottomSpace,
  }) : super(key: key);

  final int index;
  final double? extent;
  final double? bottomSpace;
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    final child = Container(
      color: backgroundColor,
      height: extent,
      child: Center(
        child: CircleAvatar(
          minRadius: 20,
          maxRadius: 20,
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          child: Text('$index', style: const TextStyle(fontSize: 20)),
        ),
      ),
    );

    if (bottomSpace == null) {
      return child;
    }

    return Column(
      children: [
        Expanded(child: child),
        Container(
          height: bottomSpace,
          color: Colors.green,
        )
      ],
    );
  }
}

class _Tile extends StatelessWidget {
  final int index;
  final String url;

  _Tile(this.index, this.url);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(5),
        child: Stack(children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: Container(
                child: CircularProgressIndicator(),
                alignment: Alignment.center,
                height: 60),
          ),
          Align(
            alignment: Alignment.center,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              child:
                  Container() /*FadeInImage.memoryNetwork(
                placeholder: "",
                image: url,
              )*/
              ,
            ),
          ),
        ]));
  }
}
