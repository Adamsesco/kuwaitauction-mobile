import 'dart:async';
import 'dart:convert';
import 'dart:math' as math;

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/signin_bloc/signin_bloc.dart';
import 'package:kauction/app/signin_bloc/signin_state.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/messenger/conversation.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/model/models/user.dart';
import 'package:kauction/model/repositories/product_repository.dart';
import 'package:kauction/views/custom_widgets/dotted_widget.dart';
import 'package:kauction/views/custom_widgets/gallery.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/custom_widgets/no_login.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';
import 'package:kauction/views/custom_widgets/round_button_icon.dart';
import 'package:kauction/views/home/home_first_screen/bid_page.dart';
import 'package:kauction/views/home/profile_other_user.dart';
import 'package:kauction/views/notifi/notif_list.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailsProduct extends StatefulWidget {
  DetailsProduct(this.product);

  Product product;

  @override
  _DetailsProductState createState() => _DetailsProductState();
}

class _DetailsProductState extends State<DetailsProduct>
    with TickerProviderStateMixin {
  RestService rest = new RestService();
  late StreamSubscription _onDestroy;
  late StreamSubscription<String> _onUrlChanged;
  bool lod = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var price = 0.0;
  var _value = 0.0;
  User? user_last_bid;
  bool start = false;
  late AnimationController controller;
  ProductSServices prod = new ProductSServices();
  late User user_me;
  bool end_time = false;

  getProductDetails() async {
    Product ppr = await prod.get_details_product(widget.product.id);
    if (!this.mounted) return;
    setState(() {
      widget.product = ppr;
    });

    print("heeeeey");
    print(widget.product.token_notification);

    controller2 = CountdownTimerController(
        endTime: widget.product.close_date!.millisecondsSinceEpoch,
        onEnd: () {
          if (!this.mounted) return;
          // setState(() {
          end_time = true;
          // });
        });

    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    );

    controller.reverse(from: controller.value == 0.0 ? 1.0 : controller.value);

    print("--");
    print(widget.product.type.toString());

    if (widget.product.type.toString() == "0") {
      get_price();
    }
    if (widget.product.type.toString() == "0" &&
        widget.product.close_date != null) {
      //startTimer();
    }
  }

  load() {
    setState(() {
      lod = false;
    });
  }

  String get timerString {
    Duration duration = controller.duration! * controller.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  /*Future<AudioPlayer> playLocalAsset() async {
    AudioCache cache = new AudioCache();
    return await cache.play("sound.mpeg");
  }*/

  get_price() {
    FirebaseDatabase.instance
        .reference()
        .child("last_price")
        .child(widget.product.id)
        .onValue
        .listen((val) {
      if (!this.mounted) return;
      var d;
      // try {

      if (val.snapshot.value.toString() == "null") {
        setState(() {
          price = double.parse(widget.product.last_price);
        });
      } else {
        if (this.mounted) {
          setState(() {
            controller = AnimationController(
              vsync: this,
              duration: Duration(seconds: 5),
            );

            controller.reverse(
                from: controller.value == 0.0 ? 1.0 : controller.value);
            if (start == true) {
              //  playLocalAsset();
            }

            setState(() {
              start = true;
            });
            d = val.snapshot.value;
            price = double.parse(Map<String, dynamic>.from(
                    val.snapshot.value as dynamic)["price"]
                .toString());
            _value = price;

            user_last_bid = new User(
              id: Map<String, dynamic>.from(
                  val.snapshot.value as dynamic)["id"],
              image: Map<String, dynamic>.from(
                  val.snapshot.value as dynamic)["avatar"],
              created: DateTime.now(),
              email: "",
              userename: Map<String, dynamic>.from(
                  val.snapshot.value as dynamic)["name"],
            );

            /// _startprice.text = _value.toString();
          });
        }
      }
      /*  } catch (e) {}*/
    });
  }

  ccc() async {
    var b = await rest.get("checkPayment/" + widget.product.id.toString());

    if (b["ispaied"] == "1") {
      Alert(
        context: context,
        type: AlertType.success,
        title: "Payment with succeess",
        desc: "Successfully added !",
        buttons: [
          DialogButton(
            color: ColorsConst.col_app,
            child: Text(
              "Return",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              Navigator.pop(context);
              //  Navigator.pop(context);
              /// widget.end_func();
              //widget.func(1);
            },
            width: 120,
          )
        ],
      ).show();
    } else {
      _scaffoldKey.currentState!.showSnackBar(new SnackBar(
          backgroundColor:
              b["ispaied"].toString() == "1" ? Colors.green : Colors.red[800],
          content: new Text(
            b["ispaied"].toString() == "1" ? "Success" : "Payment failed",
            style: TextStyle(color: Colors.white, fontSize: 16),
          )));
    }
  }

/*
  get_details() async {
    var a = await prod.get_product_deetails(widget.product.id);
    if (!this.mounted) return;
    setState(() {
      widget.product.user_id = a.user_id;
      widget.product.avatar = a.avatar;
    });
  }
*/
  String id = "";

  getuserid() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!this.mounted) return;

    setState(() {
      id = prefs.getString("id").toString();
    });

    print("mmmmmm");
    print(id);
  }

  please_login() {
    showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        content: Text('Please login to conitnue.'),
        actions: <Widget>[
          CupertinoDialogAction(
            onPressed: () {
              Navigator.of(context).pop();
              /*  Navigator.push(context,
                      new MaterialPageRoute(builder: (BuildContext context) {
                        return OtpVerification();
                      }));*/
            },
            child: Text('Ok'),
          ),
        ],
      ),
    );
  }

  Duration remaining = DateTime.now().difference(DateTime.now());
  late Timer t;
  int days = 0, hrs = 0, mins = 0;

  /*startTimer() async {
    t = Timer.periodic(Duration(seconds: 1), (timer) {
      if (!this.mounted) return;
      setState(() {
        remaining = widget.product.close_date.difference(DateTime.now());
        mins = remaining.inMinutes;
        hrs = mins >= 60 ? mins ~/ 60 : 0;
        days = hrs >= 24 ? hrs ~/ 24 : 0;
        hrs = hrs % 24;
        mins = mins % 60;
      });
    });
  }*/

  late CountdownTimerController? controller2;
  int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 30;

  @override
  void initState() {
    super.initState();

    //get_details();
    getuserid();
    print("-------------------------------------");
    print(widget.product.type.toString());

    SigninBloc _signInBloc = BlocProvider.of<SigninBloc>(context);
    final appState = _signInBloc.state;

    if (appState is InitialSignintate) {
      setState(() {
        // user_me = null;
      });
    } else if (appState is SigninSuccess) {
      user_me = appState.user;
    }
    getProductDetails();
  }

  counter_wid() => Container(
        width: 32.w,
        height: 32.w,
        child: AnimatedBuilder(
            animation: controller,
            builder: (BuildContext context, Widget? child) {
              return controller.value.toString() == "0.0"
                  ? Container()
                  : CustomPaint(
                      painter: CustomTimerPainter(
                      animation: controller,
                      color: Colors.white,
                      backgroundColor: Colors.grey[700],
                    ));
            }),
      );

  sho_bottom() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(height: 310.h, child: NoLogin(false));
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    controller2!.dispose();
    controller.dispose();

    //  if (controller != null) controller.dispose();
  }

  getbid(res) {
    if (res != null) {
      setState(() {
        widget.product.bids.add(Bid(
            username: user_me.userename,
            date: DateTime.now(),
            avatar: user_me.image,
            price: res.toString(),
            user_id: user_me.id));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text1_btn(color) => TextStyle(
        color: color,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w500);
    TextStyle text3 = TextStyle(
        color: ColorsConst.col_grey,
        fontSize: ScreenUtil().setSp(11),
        fontWeight: FontWeight.w500);

    return Scaffold(
        appBar: KauctionAppBar(
            title_widget: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.product.name.toString(),
                  style: AppTextStyles.appBar_tt(context.locale),
                ),
                Container(
                  height: 0.h,
                ),
                Text(
                    widget.product.category_name_en == null
                        ? ""
                        : context.locale.toString() == "en"
                            ? widget.product.category_name_en.toString()
                            : widget.product.category_name_ar,
                    style: AppTextStyles.small_title2)
              ],
            ),
            actions: [
              Container(width: 10.w),
              RoundButtonIcon(
                  onPress: () {
                    Navigator.push(context,
                        new MaterialPageRoute(builder: (BuildContext context) {
                      return NotificationsList();
                    }));
                  },
                  icon: "assets/icons/notif.svg"),
              Container(width: 10.w),
            ],
            returnn: true),
        key: _scaffoldKey,
        extendBodyBehindAppBar: true,
        body: ListView(children: [
          Stack(
            children: [
              DottedSlider(
                  color: Colors.white,
                  maxHeight: ScreenUtil().setHeight(288),
                  children: widget.product.images
                      .map<Widget>((Photo im) => InkWell(
                            child: FadeInImage.assetNetwork(
                              image: im.url,
                              placeholder: "assets/images/placeholder.png",
                              width: MediaQuery.of(context).size.width,
                              height: ScreenUtil().setHeight(428),
                              fit: BoxFit.cover,
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        GalleryPhotoViewWrapper(
                                      galleryItems: widget.product.images,
                                      backgroundDecoration: const BoxDecoration(
                                        color: Colors.black,
                                      ),
                                      initialIndex:
                                          widget.product.images.indexOf(im),
                                    ),
                                  ));
                            },
                          ))
                      .toList()),
              /* Positioned(
                  bottom: ScreenUtil().setHeight(42),
                  left: ScreenUtil().setWidth(27),
                  child: Container(
                    child: user_last_bid == null
                        ? SizedBox()
                        : Container(
                            width: 321.59.w,
                            padding: EdgeInsets.only(
                                left: 10.w, right: 10.w, top: 6.h, bottom: 6.h),
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(32.0)),
                                color: Colors.grey[700]!.withOpacity(0.62)),
                            child: Row(
                              children: [
                                CircleAvatar(
                                  child: ClipOval(
                                    child: Image.network(
                                      user_last_bid!.image,
                                      height: ScreenUtil().setWidth(43),
                                      width: ScreenUtil().setWidth(43),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 12.w,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      user_last_bid!.userename.toString(),
                                      style: TextStyle(
                                        fontSize: 16.sp,
                                        color: Colors.white,
                                        fontFamily: "Roboto Condensed",
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Container(
                                      height: 4.h,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          "Placed a bid of ",
                                          style: TextStyle(
                                              fontFamily: "Roboto light",
                                              fontSize: 14.sp,
                                              color: Colors.white,
                                              fontWeight: FontWeight.w300),
                                        ),
                                        Container(
                                          width: 2.w,
                                        ),
                                        Text(
                                          (price.toString() + " KD"),
                                          style: TextStyle(
                                            fontSize: 14.sp,
                                            color: Colors.white,
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                                Stack(children: [
                                  counter_wid(),
                                  Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      timerString,
                                      style: TextStyle(
                                          fontSize: 12.0, color: Colors.white),
                                    ),
                                  )
                                ])
                              ],
                            )),
                  )),*/
              /*
              Positioned(
                  top: ScreenUtil().setHeight(18),
                  left: ScreenUtil().setWidth(12),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        IconButton(
                          color: Colors.white,
                          padding: EdgeInsets.zero,
                          onPressed: () => Navigator.pop(context),
                          icon: SvgPicture.asset(
                            "assets/images/icons/arrow_back.svg",
                            height: 15,
                            color: Colors.white,
                          ),
                        ),
                        Spacer(),
                        IconButton(
                          color: Colors.white,
                          padding: EdgeInsets.zero,
                          onPressed: () => Navigator.pop(context),
                          icon: SvgPicture.asset(
                            "assets/images/icons/down.svg",
                            color: Colors.white,
                          ),
                        ),
                        /*IconButton(
                          color: Colors.white,
                          padding: EdgeInsets.zero,
                          onPressed: () => Navigator.pop(context),
                          icon: SvgPicture.asset(
                            "assets/images/icons/like.svg",
                            color: Colors.white,
                          ),
                        )*/

                        Container(
                          width: ScreenUtil().setWidth(24),
                        )
                      ],
                    ),
                  ))
              */
            ],
          ),
          Container(
              padding: EdgeInsets.only(
                  left: ScreenUtil().setWidth(16),
                  right: ScreenUtil().setWidth(16)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: ScreenUtil().setHeight(16),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        widget.product.name.toString(),
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          height: 1.2,
                          fontWeight: FontWeight.w500,
                          fontSize: ScreenUtil().setSp(16.5),
                        ),
                      )),
                  Container(
                    height: ScreenUtil().setHeight(20),
                  ),
                  widget.product.bids.length == 0
                      ? Container()
                      : Row(children: [
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("current".tr()),
                                Container(height: 2.h),
                                Text(price.toString() + " KWD",
                                    style: TextStyle(
                                        fontSize: 16.sp,
                                        height: 0.9,
                                        fontFamily:
                                            context.locale.toString() == "en"
                                                ? "poppins_bold"
                                                : "cairo_bold",
                                        fontWeight: FontWeight.bold)),
                                Text(
                                  "totalb".tr() +
                                      " : ${widget.product.bids.length}",
                                  style: TextStyle(
                                      // fontFamily: "poppins_bold",
                                      height: 1.2,
                                      fontWeight: FontWeight.w800,
                                      color: Color(0xff4263EB)),
                                )
                              ]),
                          Expanded(child: Container()),
                          widget.product.close_date == null
                              ? Container()
                              : Container(
                                  padding: EdgeInsets.all(8.w),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(12.0)),
                                      color: Color(0xffFDECEC)),
                                  child: CountdownTimer(
                                    controller: controller2,
                                    endWidget: Text("ct".tr()),
                                    onEnd: () {
                                      setState(() {
                                        end_time = true;
                                      });
                                    },
                                    endTime: endTime,
                                  )),
                        ]),
                  Container(height: 26.h),
                  Padding(
                      padding: EdgeInsets.only(bottom: 12.h),
                      child: Row(
                        children: [
                          CircleAvatar(
                            child: ClipOval(
                              child: Image.network(
                                widget.product.avatar.toString(),
                                height: ScreenUtil().setWidth(43),
                                width: ScreenUtil().setWidth(43),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Container(width: 12.w),
                          Expanded(
                              child: widget.product.username == null
                                  ? Container()
                                  : Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          widget.product.username.toString(),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            height: 1.2,
                                            color: const Color(0xff323045),
                                            fontFamily:
                                                context.locale.toString() ==
                                                        "en"
                                                    ? "poppins_bold"
                                                    : "cairo_bold",
                                            fontWeight: FontWeight.w700,
                                            fontSize: ScreenUtil().setSp(15.0),
                                          ),
                                        ),
                                        Container(
                                          height: 2.h,
                                        ),
                                        Text(widget.product.totalitems
                                                .toString() +
                                            " items")
                                      ],
                                    )),
                          Expanded(child: Container()),
                          id == widget.product.user_id
                              ? Container()
                              : RoundButtonIcon(
                                  icon: "assets/icons/chat_n.svg",
                                  onPress: () {
                                    if (id.toString() == "null") {
                                      sho_bottom();
                                    } else {
                                      user_me.id = id;
                                      User user = new User(
                                          userename: widget.product.username,
                                          email: "",
                                          created: DateTime.now(),
                                          image: widget.product.avatar,
                                          id: widget.product.user_id);
                                      Navigator.push(
                                        context,
                                        new MaterialPageRoute(
                                          builder: (BuildContext context) {
                                            return Conversation(
                                              user_me,
                                              user,
                                              widget.product.id,
                                              post: widget.product,
                                            );
                                          },
                                        ),
                                      );
                                    }
                                  },
                                ),
                          Container(width: 8.w),
                          id == widget.product.user_id
                              ? Container()
                              : RoundButtonIcon(
                                  icon: "assets/icons/phone.svg",
                                  color: Color(0xff1BB759),
                                  onPress: () async {
                                    if (id.toString() == "null") {
                                      sho_bottom();
                                    } else {
                                      user_me.id = id;

                                      print(widget.product.phone);
                                      final Uri params = Uri(
                                        scheme: 'tel',
                                        path: '${widget.product.phone}',
                                      );
                                      String url = params.toString();
                                      if (await canLaunch(url)) {
                                        await launch(url);
                                      } else {
                                        print('Could not launch $url');
                                      }
                                    }
                                  },
                                )
                        ],
                      )),

                  widget.product.priceless == "1"
                      ? Container()
                      : (id == widget.product.user_id &&
                              widget.product.type.toString() != "1")
                          ? widget.product.enable_extend == false
                              ? Container()
                              : PrimaryButton(
                                  textStyle: TextStyle(),
                                  disabledColor: ColorsConst.col_app,
                                  fonsize: 16.5.sp,
                                  icon: "",
                                  isLoading: lod,
                                  colorText: ColorsConst.col_app_white,
                                  prefix: Container(),
                                  color: ColorsConst.col_app,
                                  text: "extend".tr(),
                                  onTap: () async {
                                    SharedPreferences prefs =
                                        await SharedPreferences.getInstance();

                                    String token =
                                        prefs.getString("token").toString();

                                    var a = await rest.post("extendTime", {
                                      "token": token,
                                      "id": widget.product.id
                                    });

                                    _scaffoldKey.currentState!.showSnackBar(
                                        SnackBar(
                                            backgroundColor:
                                                ColorsConst.col_app_blue,
                                            content: Text(a["message"])));

                                    print(a);
                                  },
                                )
                          : Container(),

                  widget.product.priceless == "1"
                      ? Container()
                      : (widget.product.type.toString() != "1" &&
                              id != widget.product.user_id &&
                              (widget.product.close_date != null &&
                                  DateTime.now().isAfter(
                                          widget.product.close_date!) ==
                                      false))
                          ? PrimaryButton(
                              textStyle: TextStyle(),
                              disabledColor: ColorsConst.col_app,
                              fonsize: 16.5.sp,
                              icon: "",
                              isLoading: lod,
                              colorText: ColorsConst.col_app_white,
                              prefix: Container(),
                              color: ColorsConst.col_app,
                              text: "Place bid".tr(),
                              onTap: () async {
                                if (id.toString() == "null") {
                                  sho_bottom();
                                } else {
                                  var res = await Navigator.push(context,
                                      new MaterialPageRoute(
                                          builder: (BuildContext context) {
                                    return BidPage(
                                        widget.product.id.toString(),
                                        widget.product.last_price,
                                        user_me,
                                        widget.product);
                                  }));

                                  getbid(res);
                                }
                              },
                            )
                          : Container(),
                  Container(height: 10.h),
                  (id == widget.product.user_id)
                      ? (widget.product.enable_close == true &&
                              widget.product.type.toString() != "1")
                          ? PrimaryButton(
                              textStyle: TextStyle(),
                              disabledColor: ColorsConst.col_app,
                              fonsize: 16.5.sp,
                              inverted: true,
                              icon: "",
                              isLoading: lod,
                              colorText: ColorsConst.col_app,
                              prefix: Container(),
                              color: ColorsConst.col_app_white,
                              text: "closeb".tr(),
                              onTap: () async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();

                                String token =
                                    prefs.getString("token").toString();

                                var a = await rest.post("closeBids",
                                    {"token": token, "id": widget.product.id});
                                _scaffoldKey.currentState!.showSnackBar(
                                    SnackBar(
                                        backgroundColor: Colors.red[900],
                                        content: Text("Bid closed !")));

                                print(a);
                              },
                            )
                          : Container()
                      : Container(),
                  Container(
                    height: 26.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: 1.5,
                        color: ColorsConst.col_grey.withOpacity(0.2),
                      ),
                    ],
                  ),
                  Container(
                    height: 26.h,
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        "desc".tr(),
                        maxLines: 8,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          height: 1.2,
                          color: const Color(0xff323045),
                          fontFamily: context.locale.toString() == "en"
                              ? "poppins_bold"
                              : "cairo_bold",
                          fontWeight: FontWeight.w700,
                          fontSize: ScreenUtil().setSp(17.5),
                        ),
                      )),
                  Container(
                    height: ScreenUtil().setHeight(19),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      widget.product.description.toString() == "null"
                          ? ""
                          : widget.product.description.toString(),
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w500,
                        fontSize: ScreenUtil().setSp(14.5),
                      ),
                    ),
                  ),
                  Container(
                    height: 16.h,
                  ),

                  Row(children: [
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "category".tr(),
                            style:
                                AppTextStyles.small_title_bold(context.locale),
                          ),
                          Container(height: 6.h),
                          Text("type".tr(),
                              style: AppTextStyles.small_title_bold(
                                  context.locale.toString())),
                          Container(height: 6.h),
                          Text("brand".tr(),
                              style: AppTextStyles.small_title_bold(
                                  context.locale.toString())),
                          Container(height: 6.h),
                          Text("model".tr(),
                              style: AppTextStyles.small_title_bold(
                                  context.locale.toString())),
                        ]),
                    Container(width: 32.w),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.product.category_name_en.toString(),
                            style: AppTextStyles.small_title4,
                          ),
                          Container(height: 6.h),
                          Text(widget.product.subcategory_name.toString(),
                              style: AppTextStyles.small_title4),
                          Container(height: 6.h),
                          Text(widget.product.brand_name.toString(),
                              style: AppTextStyles.small_title4),
                          Container(height: 6.h),
                          Text(widget.product.module_name.toString(),
                              style: AppTextStyles.small_title4),
                        ]),
                  ]),
                  Container(
                    height: 16.h,
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        height: 1.5,
                        color: ColorsConst.col_grey.withOpacity(0.2),
                      ),
                    ],
                  ),
                  Container(
                    height: 26.h,
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        "bids_history".tr(),
                        maxLines: 8,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          height: 1.2,
                          color: const Color(0xff323045),
                          fontFamily: context.locale.toString() == "en"
                              ? "poppins_bold"
                              : "cairo_bold",
                          fontWeight: FontWeight.w700,
                          fontSize: ScreenUtil().setSp(17.5),
                        ),
                      )),

                  Container(height: 22.h),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: widget.product.bids
                          .map((e) => Padding(
                              padding: EdgeInsets.only(bottom: 12.h),
                              child: Row(
                                children: [
                                  InkWell(
                                      onTap: () {
                                        if (widget.product.user_id == id) {
                                          Navigator.push(context,
                                              new MaterialPageRoute(builder:
                                                  (BuildContext context) {
                                            return ProfilOtherUser(
                                                e, id, user_me, widget.product);
                                          }));
                                        }
                                      },
                                      child: CircleAvatar(
                                        child: ClipOval(
                                          child: Image.network(
                                            e.avatar,
                                            height: ScreenUtil().setWidth(43),
                                            width: ScreenUtil().setWidth(43),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      )),
                                  Container(width: 12.w),
                                  Expanded(
                                      child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      InkWell(
                                          onTap: () {
                                            if (widget.product.user_id == id)
                                              Navigator.push(context,
                                                  new MaterialPageRoute(builder:
                                                      (BuildContext context) {
                                                return ProfilOtherUser(e, id,
                                                    user_me, widget.product);
                                              }));
                                          },
                                          child: Text(
                                            e.username,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              height: 1.2,
                                              color: const Color(0xff323045),
                                              fontFamily: context.locale == "en"
                                                  ? "poppins_bold"
                                                  : "cairo_bold",
                                              fontWeight: FontWeight.w700,
                                              fontSize:
                                                  ScreenUtil().setSp(15.0),
                                            ),
                                          )),
                                      Container(
                                        height: 2.h,
                                      ),
                                      Text(DateFormat('yyyy-MM-dd HH:mm')
                                          .format(e.date))
                                    ],
                                  )),
                                  Column(
                                    children: [
                                      Text(
                                        "+" + e.price.toString() + " KWD",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: ColorsConst.col_app),
                                      )
                                    ],
                                  )
                                ],
                              )))
                          .toList()),

                  Container(
                    height: ScreenUtil().setHeight(19),
                  ),
                  // FeaturedItems([])
                ],
              )),
        ]));
  }
}

class CustomTimerPainter extends CustomPainter {
  CustomTimerPainter({
    this.animation,
    this.backgroundColor,
    this.color,
  }) : super(repaint: animation);

  final Animation<double>? animation;
  final Color? backgroundColor, color;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = backgroundColor!
      ..strokeWidth = 3.0
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);
    paint.color = color!;
    double progress = (1.0 - animation!.value) * 2 * math.pi;
    canvas.drawArc(Offset.zero & size, math.pi * 1.5, -progress, false, paint);
  }

  @override
  bool shouldRepaint(CustomTimerPainter old) {
    return animation!.value != old.animation!.value ||
        color != old.color ||
        backgroundColor != old.backgroundColor;
  }
}
