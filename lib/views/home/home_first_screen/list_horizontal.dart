import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/views/home/home_first_screen/details_products.dart';

class FeaturedItems extends StatefulWidget {
  FeaturedItems(this.products);

  List<Product> products;

  @override
  _FeaturedItemsState createState() => _FeaturedItemsState();
}

class _FeaturedItemsState extends State<FeaturedItems> {
  @override
  Widget build(BuildContext context) {
    return MasonryGridView.count(
                // controller: widget._hideButtonController,
                crossAxisCount: 2,
               // mainAxisSpacing: 12.0,

               // shrinkWrap: true,
                primary: false,
                crossAxisSpacing: 0.0,
                itemCount: widget.products.length,
                itemBuilder: (context, index) => FeaturedItem(widget.products[
                    index]))/*Container(
        height: ScreenUtil().setHeight(210),
        child: ListView(
          padding: EdgeInsets.symmetric(horizontal: 16.w),
          scrollDirection: Axis.vertical,
          children:
              widget.products.map((Product e) => FeaturedItem(e)).toList(),
        ))*/
        ;
  }
}

class FeaturedItem extends StatelessWidget {
  FeaturedItem(this.product);

  Product product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
          //  height: ScreenUtil().setHeight(198),
            width: ScreenUtil().setWidth(230),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                    //<--clipping image

                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage("assets/images/placeholder.png"),
                                fit: BoxFit.contain)),
                        child: product.images == null || product.images.isEmpty
                            ? Container()
                            : Hero(
                                tag: product.images[0],
                                child: FadeInImage.assetNetwork(
                                  image: product.images[0].url,
                                  placeholder: "assets/images/placeholder.png",
                                  width: ScreenUtil().setWidth(190),
                                  height: ScreenUtil().setHeight(128),
                                  fit: BoxFit.cover,
                                )))),

                SizedBox(
                  height: 8,
                ),

                Container(
                    // height: ScreenUtil().setHeight(54),
                    width: ScreenUtil().setWidth(110),
                    child: Text(
                      product.name.toString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),
                SizedBox(
                  height: 4,
                ),
                /* Container(
                    height: ScreenUtil().setHeight(34),
                    width: ScreenUtil().setWidth(110),
                    child: Text(
                      product.description.toString(),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        height: 1.4,
                        fontWeight: FontWeight.w400,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),*/
                // Expanded(child: Container(),),
                Container(
                    width: ScreenUtil().setWidth(220),
                    child: Text(
                      product.bids.length.toString() + " Bids - ends ..",
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: ColorsConst.col_app_blue,
                        height: 1.4,
                        fontSize: ScreenUtil().setSp(11.5),
                      ),
                    )),
                Container(
                  child: Text(
                    "KD " + product.last_price.toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontFamily: "poppins_bold",
                      fontSize: ScreenUtil().setSp(17),
                    ),
                  ),
                ),
              ],
            )),
        onTap: () {
          Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
            return DetailsProduct(product);
          }));
        });
  }
}
