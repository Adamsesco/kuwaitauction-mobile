import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/home_bloc/home_bloc.dart';
import 'package:kauction/app/home_bloc/home_event.dart';
import 'package:kauction/app/home_bloc/home_state.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/views/home/home_first_screen/list_horizontal.dart';
import 'package:kauction/views/home/search_page.dart';
import 'package:kauction/views/home/settings/items_by_catgory.dart';
import 'package:easy_localization/easy_localization.dart';

class HomeFirstScreen extends StatefulWidget {
  HomeFirstScreen({Key? key}) : super(key: key);

  @override
  _HomeFirstScreenState createState() => _HomeFirstScreenState();
}

class _HomeFirstScreenState extends State<HomeFirstScreen> {
  late HomeBloc _homeBloc;
  final TextEditingController _searchQuery = new TextEditingController();

  @override
  void initState() {
    super.initState();

    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _homeBloc.add(Homeequested());
  }

  @override
  Widget build(BuildContext context) {
    print("jdjdjdjdjdjd");
    print(context.locale);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(8),
            right: ScreenUtil().setWidth(8),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              fontSize: ScreenUtil().setSp(17), fontWeight: FontWeight.w900),
        ));

    return BlocBuilder<HomeBloc, HomeState>(builder: (context, homeState) {
      if (homeState is FeedLoadInProgress) {
        return Center(
          child: CupertinoActivityIndicator(),
        );
      } else if (homeState is FeedLoadSuccess) {
        final all_items = homeState.homeResponse.home;

        return Column(children: [
          InkWell(
              onTap: () {
                Navigator.push(context,
                    new MaterialPageRoute(builder: (BuildContext context) {
                  return SearchPage("");
                }));
              },
              child: Column(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 4.0, horizontal: 4.0),
                    child: IgnorePointer(
                        child: TextField(
                      controller: _searchQuery,
                      onSubmitted: (val) {
                        Navigator.push(context, new MaterialPageRoute(
                            builder: (BuildContext context) {
                          return SearchPage(val);
                        }));
                      },
                      decoration: InputDecoration(
                          counterStyle: TextStyle(color: ColorsConst.col_app),
                          isDense: true,
                          contentPadding: EdgeInsets.symmetric(horizontal: 6.0),
                          hintText: '  ' + "search".tr(),
                          enabledBorder: OutlineInputBorder(
                            // width: 0.0 produces a thin "hairline" border
                            borderSide: BorderSide(
                                color: ColorsConst.col_grey, width: 0.0),
                            borderRadius: BorderRadius.circular(24.0.r),
                          ),
                          hintStyle: TextStyle(color: ColorsConst.col_grey),
                          suffixIcon: Padding(
                            padding:
                                const EdgeInsetsDirectional.only(end: 12.0),
                            child: Icon(
                              Icons.search,
                              color: ColorsConst.col_app,
                              size: 30.0,
                            ), // icon is 48px widget.
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: ColorsConst.col_app, width: 0.0),
                            borderRadius: BorderRadius.circular(60.0),
                          ),
                          filled: true),
                    )),
                  ),
                ],
              )),
          Expanded(
              child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 16.w),
                  children: all_items!
                      .map((e) => Column(children: [
//F7F8FA
                            Container(
                                child: Row(children: [
                              text(context.locale.toString() == "en"
                                  ? e.category_name
                                  : e.category_name_ar),
                              Expanded(
                                child: Container(),
                              ),
                              FlatButton(
                                  onPressed: () {
                                    Navigator.push(context,
                                        new MaterialPageRoute(
                                            builder: (BuildContext context) {
                                      return IByCategorytemsPage(
                                          e.category_id,
                                          context.locale.toString() == "en"
                                              ? e.category_name
                                              : e.category_name_ar,
                                          "");
                                    }));
                                  },
                                  child: Text(
                                    "view all".tr(),
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ))
                            ])),
                            e.products.isEmpty
                                ? Container(
                                    height: ScreenUtil().setHeight(80),
                                    child: Center(
                                        child: Text(
                                      "No item found !",
                                      style: AppTextStyles.hint,
                                    )))
                                : FeaturedItems(e.products),
                            Container(
                              height: ScreenUtil().setHeight(14),
                            ),
                          ]))
                      .toList() /*[
            text("categories".tr()),
            Categories_Horizontal(categories),
            Container(
              height: ScreenUtil().setHeight(14),
            ),
            Container(
                child: Row(children: [
                  text("Best Bids"),
                  Expanded(
                    child: Container(),
                  ),
                  FlatButton(
                      onPressed: () {

                        Navigator.push(context,
                            new MaterialPageRoute(builder: (BuildContext context) {
                              return Product_Best_Bids_All();
                            }));
                      },
                      child: Text(
                        "See more ..",
                        style: TextStyle(color: Config.col_blue,
                            fontWeight: FontWeight.bold
                        ),
                      ))
                ])),
            BestBidWidget(bids),
            Container(
              height: ScreenUtil().setHeight(10),
            ),
            Container(
                child: Row(children: [
                  text("Stores"),
                  Expanded(
                    child: Container(),
                  ),
                  FlatButton(
                      onPressed: () {

                        Navigator.push(context,
                            new MaterialPageRoute(builder: (BuildContext context) {
                              return Stores_All();
                            }));
                      },
                      child: Text(
                        "See more ..",
                        style: TextStyle(color: Config.col_blue,
                            fontWeight: FontWeight.bold
                        ),
                      ))
                ])),
            Stores_Horizontal(stores),
            Container(
              height: ScreenUtil().setHeight(10),
            ),


            Container(
                child: Row(children: [
                  text("Featured items"),
                  Expanded(
                    child: Container(),
                  ),
                  FlatButton(
                      onPressed: () {

                        Navigator.push(context,
                            new MaterialPageRoute(builder: (BuildContext context) {
                              return Product_products_All();
                            }));
                      },
                      child: Text(
                        "See more ..",
                        style: TextStyle(color: Config.col_blue,
                            fontWeight: FontWeight.bold
                        ),
                      ))
                ])),
            FeaturedItems(fea_items)
          ],*/
                  ))
        ]);
      } else {
        return Container();
      }
    });
  }
}
