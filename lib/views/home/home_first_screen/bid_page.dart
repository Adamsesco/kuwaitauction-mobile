import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/model/models/user.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

//import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
class BidPage extends StatefulWidget {
  BidPage(this.id, this.last_price, this.user, this.product);

  String id;
  String last_price;
  User user;
  Product product;


  @override
  _BidPageState createState() => _BidPageState();
}

class _BidPageState extends State<BidPage> {
  bool show = false;
  RestService rest = new RestService();

  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  final FocusNode _nodeText3 = FocusNode();
  final FocusNode _nodeText4 = FocusNode();
  final FocusNode _nodeText5 = FocusNode();
  final FocusNode _nodeText6 = FocusNode();

  /// Creates the [KeyboardActionsConfig] to hook up the fields
  /// and their focus nodes to our [FormKeyboardActions].
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      nextFocus: true,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeText1,
        ),
        KeyboardActionsItem(focusNode: _nodeText2, toolbarButtons: [
          (node) {
            return GestureDetector(
              onTap: () => node.unfocus(),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Icon(Icons.close),
              ),
            );
          }
        ]),
        KeyboardActionsItem(
          focusNode: _nodeText3,
          onTapAction: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    content: Text("Custom Action"),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("OK"),
                        onPressed: () => Navigator.of(context).pop(),
                      )
                    ],
                  );
                });
          },
        ),
        KeyboardActionsItem(
          focusNode: _nodeText4,
          //   displayCloseWidget: false,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText5,
          toolbarButtons: [
            //button 1
            (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "CLOSE",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              );
            },
            //button 2
            (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  color: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "DONE",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              );
            }
          ],
        ),
        KeyboardActionsItem(
          focusNode: _nodeText6,
          footerBuilder: (_) => PreferredSize(
              child: SizedBox(
                  height: 40,
                  child: Center(
                    child: Text('Custom Footer'),
                  )),
              preferredSize: Size.fromHeight(40)),
        ),
      ],
    );
  }

  var price = 0.0;
  var _value = 0.0;
  var _startprice = TextEditingController();
  var price_init = 0.0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  get_price() {
    FirebaseDatabase.instance
        .reference()
        .child("last_price")
        .child(widget.id)
        .onValue
        .listen((val) {
      var d;
      // try {

      if (val.snapshot.value.toString() == "null") {
        setState(() {
          price = double.parse(widget.last_price);
        });
      } else {
        if (this.mounted) {
          setState(() {
            d = val.snapshot.value;
            price = double.parse(new Map<String, dynamic>.from(
                    val.snapshot.value as dynamic)["price"]
                .toString());
            price_init = double.parse(new Map<String, dynamic>.from(
                    val.snapshot.value as dynamic)["price"]
                .toString());
            _value = price;
            _startprice.text = _value.toString();
          });
        }
      }
      /*  } catch (e) {}*/
    });
  }

  @override
  void initState() {
    super.initState();

    get_price();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text_btn2 = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_grey,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: ColorsConst.col_grey,
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w300),
        ));

    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: KauctionAppBar(
          returnn: true,
          /*true,
            leading_widget: IconButton(
              icon: SvgPicture.asset("assets/images/icons/arrow_back.svg"),
              onPressed: () {
                Navigator.pop(context);
              },
            ), */
          actions: [],
        ),
        body: KeyboardActions(
            config: _buildConfig(context),
            child: ListView(
                padding: EdgeInsets.only(
                    left: ScreenUtil().setWidth(16),
                    right: ScreenUtil().setWidth(16)),
                children: [
                  Container(height: 12.h),
                  Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      width: MediaQuery.of(context).size.width * 0.94,
                      child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: Column(children: [
                            Container(height: 42.h),
                            Center(
                                child: Text(
                              "your_bid".tr()+" ${price.toStringAsFixed(2)} KD",
                              style: TextStyle(color: ColorsConst.col_app_blue),
                            )),
                            Container(
                                //  width: MediaQuery.of(context).size.width * 0.9,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                  Text(
                                    "cur".tr()+" : ".toUpperCase(),
                                    style: TextStyle(
                                        color: Color(0xffA3AFD5),
                                        fontWeight: FontWeight.w400),
                                  ),
                                  Text(
                                    "${price.toStringAsFixed(2)}KD ",
                                    style: TextStyle(
                                        color: ColorsConst.col_app,
                                        fontWeight: FontWeight.bold),
                                  )
                                ])),
                            Container(
                              height: 20.h,
                            ),
                            /* Text(
                          _value.toStringAsFixed(2),
                          style: TextStyle(
                            fontSize: 39.sp,
                            fontFamily: "Roboto Condensed",
                            fontWeight: FontWeight.w900,
                          ),
                        ),*/

                            Row(mainAxisAlignment: MainAxisAlignment.center,
                                //width: 200.w,
                                children: [
                                  Expanded(
                                      child: TextFormField(
                                    cursorColor: Colors.black,
                                    focusNode: _nodeText5,
                                    keyboardType: TextInputType.number,
                                    controller: _startprice,
                                    style: TextStyle(
                                      fontSize: 26.sp,
                                      fontWeight: FontWeight.w900,
                                    ),
                                    decoration: new InputDecoration(
                                        border: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        hintStyle: TextStyle(
                                          fontSize: 32.sp,
                                          fontWeight: FontWeight.w900,
                                        ),
                                        labelStyle: TextStyle(
                                          fontSize: 32.sp,
                                          fontWeight: FontWeight.w900,
                                        ),
                                        enabledBorder: InputBorder.none,
                                        errorBorder: InputBorder.none,
                                        disabledBorder: InputBorder.none,
                                        contentPadding: EdgeInsets.only(
                                            left: 16.w, right: 16.w),
                                        hintText: "0.00"),
                                  ))
                                ]),
                            Container(
                              height: 58.h,
                            ),
                          ]))),
                  Container(
                    height: 12.h,
                  ),
                  Container(
                    height: 24.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          //   width: ScreenUtil().setWidth(168),
                          height: ScreenUtil().setHeight(46),
                          child: RaisedButton(
                              color: ColorsConst.col_app,
                              elevation: 1,
                              shape: new RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(8),
                                ),
                              ),
                              onPressed: () async {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                String id = prefs.getString("token").toString();

                                if (double.parse(_startprice.text) > price) {
                                  setState(() {
                                    show = true;
                                    _value = double.parse(_startprice.text);
                                    price = double.parse(_startprice.text);
                                  });
                                  await FirebaseDatabase.instance
                                      .reference()
                                      .child("last_price")
                                      .child(widget.id)
                                      .update({
                                    "name": widget.user.userename,
                                    "avatar": widget.user.image,
                                    "id": widget.user.id,
                                    "price":
                                        double.parse(_startprice.text) + 0.00
                                  });
//+ widget.id
                                  var a = await rest.post("makebid/", {
                                    "token": id,
                                    "post_id": widget.id,
                                 //   "token_notification": widget.product.token_notification,
                                    "price": double.parse(_startprice.text) ,
                                    "userid": widget.user.id,
                                    "date": DateTime.now().toString()
                                  });
                                  print(a);

                                  setState(() {
                                    show = false;
                                  });
                                  _scaffoldKey.currentState!.showSnackBar(SnackBar(
                                      backgroundColor: ColorsConst.col_app_blue,
                                      content: Container(
                                          height: 80.h,
                                          child: Text(
                                              "You have successfully bid the auction !",
                                              style: TextStyle(
                                                  fontSize: 30.sp)))));
                                  Navigator.pop(context, double.parse(_startprice.text) );
                                } else {
                                  _scaffoldKey.currentState!
                                      .showSnackBar(new SnackBar(
                                    backgroundColor: ColorsConst.col_app,
                                    content: new Text(
                                      "Your minimum bid cannot be inferior to ${double.parse(widget.last_price).toStringAsFixed(2)} KD",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18.sp),
                                    ),
                                  ));
                                }
                                /* Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => MainScreen()),
                                    (Route<dynamic> route) => false);*/
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  show //CupertinoActivityIndicator()
                                      ? CupertinoTheme(
                                          data: CupertinoTheme.of(context)
                                              .copyWith(
                                                  brightness: Brightness.dark),
                                          child: CupertinoActivityIndicator())
                                      : Container(),
                                  show //CupertinoActivityIndicator()
                                      ? Container(
                                          width: 4,
                                        )
                                      : Container(),
                                  Text(
                                    "confirm_bid".tr(),
                                    style: text_btn2,
                                  ),
                                ],
                              ))),
                    ],
                  ),
                  Container(
                    height: 32.h,
                  ),
                  /*  SfSlider(
                min: price,
                max: price + 40.0,
                value: _value,
                interval: 20,

                minorTicksPerInterval: 10,
                // showTicks: true,
                showLabels: true,
                enableTooltip: true,
                // minorTicksPerInterval: 1,
                onChanged: (dynamic value) {
                  setState(() {
                    _value = value;
                    _startprice.text = _value.toStringAsFixed(2);
                  });
                },
              ),*/
                ])));
  }
}
