import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/model/models/product.dart';

import 'package:shared_preferences/shared_preferences.dart';

class FavoriteButton extends StatefulWidget {
  FavoriteButton(this.prod);

  Product prod;

  @override
  _FavoriteButtonState createState() => new _FavoriteButtonState();
}

class _FavoriteButtonState extends State<FavoriteButton>
    with SingleTickerProviderStateMixin {
  late Animation<double> _heartAnimation;
  late AnimationController _heartAnimationController;
  Color _heartColor = ColorsConst.col_grey;
  bool favorite = false;
  late int fav_id;
  static const int _kHeartAnimationDuration = 300;
  RestService rest = new RestService();

  // AnimalsServices favservice = new AnimalsServices();

  _configureAnimation() {
    Animation<double> _initAnimation(
        {required double from,
        required double to,
        required Curve curve,
        required AnimationController controller}) {
      final CurvedAnimation animation = new CurvedAnimation(
        parent: controller,
        curve: curve,
      );
      return new Tween<double>(begin: from, end: to).animate(animation);
    }

    _heartAnimationController = new AnimationController(
      duration: const Duration(milliseconds: _kHeartAnimationDuration),
      vsync: this,
    );

    _heartAnimation = _initAnimation(
        from: 1.0,
        to: 1.6,
        curve: Curves.easeOut,
        controller: _heartAnimationController);
  }

  /*check_fav() async {
    var a = await rest.get_fav(widget.prod.id, id);
    if (!this.mounted) return;
    print("ressss");
    print(a["data"]);
    if (a["data"] == false) {
      setState(() {
        favorite = false;
      });
    } else {
      setState(() {
        favorite = true;
      });
    }
  }*/

  @override
  void initState() {
    super.initState();
    _configureAnimation();

    //check_fav();

    /* if (widget.prod.favorite == null) {
      setState(() {
        _heartColor = Colors.grey[400];
      });
    } else {
      fav_id = widget.product.favorite[0]["favourite_id"];
      //print(fav_id);
      setState(() {
        _heartColor = Colors.blue[400];
      });
    }*/
  }

  @override
  void dispose() {
    _heartAnimationController.dispose();
    super.dispose();
  }

  late String id;

  toggle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    id = prefs.getString("token").toString();

    var a = await rest.get_fav(widget.prod.id, id);

    if (a["message"] == "Deleted from saved") {
      setState(() {
        favorite = false;
      });
    } else {
      setState(() {
        favorite = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final Widget child = new InkWell(
      child: new Container(
          child: new ScaleTransition(
              scale: _heartAnimation,
              child: SvgPicture.asset(
                favorite ? "assets/icons/fav.svg" : "assets/icons/fav.svg",
                color: favorite ? ColorsConst.col_app : ColorsConst.col_grey,
                height: 20,
                width: 20,
              ))),
      onTap: (() {
        toggle();

        _heartAnimationController.forward().whenComplete(() {
          _heartAnimationController.reverse();
        });
      }),
    );

    return child;
  }
}
