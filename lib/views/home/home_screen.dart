import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/bottom_navigation_bloc/bottom_navigation_bloc.dart';
import 'package:kauction/app/signin_bloc/signin_bloc.dart';
import 'package:kauction/app/signin_bloc/signin_event.dart';
import 'package:kauction/app/signin_bloc/signin_state.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/messenger/chats.dart';
import 'package:kauction/messenger/conversation.dart';
import 'package:kauction/messenger/util/settings.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/model/models/user.dart';
import 'package:kauction/model/repositories/product_repository.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/views/custom_widgets/no_login.dart';
import 'package:kauction/views/custom_widgets/round_button_icon.dart';
import 'package:kauction/views/home/favorites/favorites_page.dart';
import 'package:kauction/views/home/home_first_screen/details_products.dart';
import 'package:kauction/views/home/home_first_screen/home_first_screen.dart';
import 'package:kauction/views/home/home_page.dart';
import 'package:kauction/views/home/publish_screens/publish1.dart';
import 'package:kauction/views/home/settings/settings.dart';
import 'package:kauction/views/notifi/notif_list.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:easy_localization/easy_localization.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int indx = 0;
  User? user_me;
  SigninBloc? _signInBloc;
  bool logged = false;
  Settings_Services setting = Settings_Services();
  bool verify_load = true;
  bool not = false;

  /**
      if (appState is InitialSignintate) {
      setState(() {
      logged = false;
      });
      } else if (appState is SigninSuccess) {
   */

  Settings_Services userp = new Settings_Services();
  ProductSServices prod = new ProductSServices();


  setNotif() async {
    //  FirebaseMessaging.instance.requestPermission

    if (Platform.isIOS) {
      FirebaseMessaging.instance.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true,
      );
    }

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) async {
      print('A new onMessageOpenedApp event was published!');
      print(message);

      print(message.data);

     /* Product ppr = await prod.get_details_product(message.data["product_id"]);


      Navigator.push(context,
          new MaterialPageRoute(builder: (BuildContext context) {
            return DetailsProduct(ppr);
          }));*/

      if (message.data.containsKey("keyy")) {
        List<String> ids = message.data["my_key"].split("_");

        var user_me = await userp.user_informations(ids[0]);
        var user_him = await userp.user_informations(ids[1]);
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => Conversation(user_him, user_me, ids[2])));
      } else if (message.data.containsKey("product_id")) {
        Product ppr = await prod.get_details_product(message.data["product_id"]);


        Navigator.push(context,
            new MaterialPageRoute(builder: (BuildContext context) {
              return DetailsProduct(ppr);
            }));
        /* */
        //NotifList data["id"]
      }
    });
  }

  verify_login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString("id").toString();

    if (id == "null") {
      setState(() {
        logged = false;
        verify_load = false;
      });
    } else {
      setNotif();
      _signInBloc = BlocProvider.of<SigninBloc>(context);
      user_me = await setting.user_info();

      print(user_me!.id);
      _signInBloc!.add(SignInSucccessEvent(user: user_me!));

      final appState = _signInBloc!.state;
      print(appState);
      setNotifi();
      setState(() {
        // user_me = appState.user;
        logged = true;
        verify_load = false;
      });
    }
  }

  setNotifi() async {
    FirebaseDatabase.instance
        .reference()
        .child("notif_new/" + user_me!.id)
        .onValue
        .listen((val) {
      var d = val.snapshot.value;
      setState(() {
        not = d;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    verify_login();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: KauctionAppBar(actions: [
        RoundButtonIcon(
            onPress: () {
              Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return NotificationsList();
              }));
            },
            icon: "assets/icons/notif.svg"),
        Container(width: 10.w),
      ]),
      body: verify_load == true
          ? Center(child: CupertinoActivityIndicator())
          : BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
              builder: (BuildContext context, BottomNavigationState state) {
                if (state is PageLoading) {
                  return HomePage() /*HomeFirstScreen()*/;
                }
                if (state is SecondPageLoaded) {
                  return logged == false
                      ? NoLogin(true)
                      : Container(
                          padding: EdgeInsets.only(
                              top: 22.h, left: 22.w, right: 22.w),
                          child: FavoritesPage());
                }
                if (state is ThiredPage) {
                  return logged == false ? NoLogin(true) : Publish1();
                }

                if (state is ForthPage) {
                  return logged == false ? NoLogin(true) : Chats();
                }

                if (state is LastPage) {
                  return logged == false ? NoLogin(true) : SettingsPage();
                }
                return Container();
              },
            ),
      bottomNavigationBar:
          BlocBuilder<BottomNavigationBloc, BottomNavigationState>(
              builder: (BuildContext context, BottomNavigationState state) {
        return BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: ColorsConst.col_app,
          showSelectedLabels: true,
          selectedLabelStyle: TextStyle(
              color: ColorsConst.col_app,
              fontWeight: FontWeight.bold,
              fontSize: 15.sp),
          unselectedLabelStyle: TextStyle(color: Colors.black, fontSize: 15.sp),
          // showUnselectedLabels: false,
          backgroundColor: ColorsConst.col_app_white,
          currentIndex:
              context.select((BottomNavigationBloc bloc) => bloc.currentIndex),
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
//
              icon: Padding(
                  padding: EdgeInsets.only(bottom: 12.h),
                  child: SvgPicture.asset(
                    context.select((BottomNavigationBloc bloc) =>
                                bloc.currentIndex) ==
                            0
                        ? "assets/icons/home.svg"
                        : "assets/icons/home_n.svg",
                  )),

              label: 'home'.tr(),
            ),
            BottomNavigationBarItem(
              icon: Padding(
                  padding: EdgeInsets.only(bottom: 12.h),
                  child: SvgPicture.asset(
                    context.select((BottomNavigationBloc bloc) =>
                                bloc.currentIndex) ==
                            1
                        ? "assets/icons/fav.svg"
                        : "assets/icons/fav_n.svg",
                  )),
              label: 'Favorites'.tr(),
            ),
            BottomNavigationBarItem(
              label: "",
              icon: FloatingActionButton(
                  splashColor: ColorsConst.col_app,
                  hoverColor: ColorsConst.col_app,
                  backgroundColor: ColorsConst.col_app,
                  child: SvgPicture.asset(
                    "assets/icons/add_n.svg",
                  ),
                  onPressed: () {
                    setState(() {
                      indx = 2;
                    });
                    context
                        .read<BottomNavigationBloc>()
                        .add(PageTapped(index: indx));
                  }),
            ),
            BottomNavigationBarItem(
              icon: Container(
                  padding: EdgeInsets.only(bottom: 12.h),
                  child: Stack(children: [
                    Container(
                        margin: EdgeInsets.all(not.toString() != "null" &&
                                not.toString() != "false"
                            ? 6.w
                            : 0),
                        child: SvgPicture.asset(
                          context.select((BottomNavigationBloc bloc) =>
                                      bloc.currentIndex) ==
                                  3
                              ? "assets/icons/chat.svg"
                              : "assets/icons/chat_n.svg",
                        )),
                    new Positioned(
                      // draw a red marble
                      top: 2.0.h,
                      right: 0.0.w,
                      child:
                          not.toString() != "null" && not.toString() != "false"
                              ? new Icon(Icons.brightness_1,
                                  size: 16.0.sp, color: Colors.redAccent)
                              : new Container(),
                    )
                  ])),
              label: 'Chat'.tr(),
            ),
            BottomNavigationBarItem(
              icon: Padding(
                  padding: EdgeInsets.only(bottom: 12.h),
                  child: SvgPicture.asset(
                    context.select((BottomNavigationBloc bloc) =>
                                bloc.currentIndex) ==
                            4
                        ? "assets/icons/account.svg"
                        : "assets/icons/param_n.svg",
                  )),
              label: 'Account'.tr(),
            ),
          ],
          onTap: (index) {
            setState(() {
              indx = index;
            });
            if (indx == 3) {
              if (user_me != null) {
                var gMessagesDbRef3 =
                    FirebaseDatabase.instance.reference().child("notif_new");
                gMessagesDbRef3.update({user_me!.id: false});
              }
            }
            context.read<BottomNavigationBloc>().add(PageTapped(index: index));
          },
        );
      }),
    );
  }
}
