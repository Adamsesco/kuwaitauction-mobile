import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/messenger/conversation.dart';
import 'package:kauction/messenger/util/settings.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/model/models/user.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:kauction/views/custom_widgets/round_button_icon.dart';
import 'package:kauction/views/home/home_first_screen/list_horizontal.dart';
import 'package:kauction/views/home/settings/my_items.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilOtherUser extends StatefulWidget {
  ProfilOtherUser(this.user, this.id, this.user_me, this.product, {Key? key})
      : super(key: key);
  Bid user;
  String id;
  User user_me;
  Product product;

  @override
  _ProfilOtherUserState createState() => _ProfilOtherUserState();
}

class _ProfilOtherUserState extends State<ProfilOtherUser> {
  Settings_Services setting = Settings_Services();

  bool load = true;

  User user =
      User(image: "", created: DateTime.now(), userename: "", email: "");

  user_info() async {
    var res = await setting.user_info_other(widget.user.user_id);
    if (!this.mounted) return;
    setState(() {
      user = res;
      load = false;
    });
  }

  @override
  void initState() {
    super.initState();
    user_info();
  }

  @override
  Widget build(BuildContext context) {
    print(context.locale);

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(8),
            right: ScreenUtil().setWidth(8),
            top: ScreenUtil().setHeight(16),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              fontSize: ScreenUtil().setSp(17), fontWeight: FontWeight.w900),
        ));

    return Scaffold(
      appBar: KauctionAppBar(
        returnn: true,
        actions: [],
      ),
      body: load == true
          ? Center(child: CupertinoActivityIndicator())
          : Column(children: [
              Padding(
                  padding: EdgeInsets.all(16.w),
                  child: Row(
                    children: [
                      Container(
                          height: ScreenUtil().setWidth(66),
                          width: ScreenUtil().setWidth(66),
                          child: CircleAvatar(
                            child: ClipOval(
                              child: Image.network(
                                widget.user.avatar.toString(),
                                height: ScreenUtil().setWidth(66),
                                width: ScreenUtil().setWidth(66),
                                fit: BoxFit.cover,
                              ),
                            ),
                          )),
                      Container(width: 12.w),
                      Expanded(
                          child: widget.user.username == null
                              ? Container()
                              : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      widget.user.username.toString(),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        height: 1.2,
                                        color: const Color(0xff323045),
                                        fontFamily:
                                            context.locale.toString() == "en"
                                                ? "poppins_bold"
                                                : "cairo_bold",
                                        fontWeight: FontWeight.w700,
                                        fontSize: ScreenUtil().setSp(15.0),
                                      ),
                                    ),

                                    /*Text(widget.user.totalitems
                            .toString() +
                            " items")*/
                                  ],
                                )),
                      Expanded(child: Container()),
                      RoundButtonIcon(
                        icon: "assets/icons/chat_n.svg",
                        onPress: () {
                          // widget.user_me.id = id;
                          User user = new User(
                              userename: widget.user.username,
                              email: "",
                              created: DateTime.now(),
                              image: widget.user.avatar,
                              id: widget.user.user_id);

                          ///jiji
                          Navigator.push(
                            context,
                            new MaterialPageRoute(
                              builder: (BuildContext context) {
                                return Conversation(
                                  widget.user_me,
                                  user,
                                  widget.product.id,
                                  post: widget.product,
                                );
                              },
                            ),
                          );
                        },
                      ),
                      Container(width: 8.w),
                      /* id == widget.product.user_id
                    ? Container()
                    : */
                      RoundButtonIcon(
                          icon: "assets/icons/phone.svg",
                          color: Color(0xff1BB759),
                          onPress: () async {
                            final Uri params = Uri(
                              scheme: 'tel',
                              path: '${user.mobile}',
                            );
                            String url = params.toString();
                            if (await canLaunch(url)) {
                              await launch(url);
                            } else {
                              print('Could not launch $url');
                            }
                          })
                    ],
                  )),
              Container(
                  child: Row(children: [
                Container(
                  width: 12.w,
                ),
                text("items".tr()),
                Expanded(
                  child: Container(),
                ),
                /*  FlatButton(
                    onPressed: () {
                      Navigator.push(context, new MaterialPageRoute(
                          builder: (BuildContext context) {
                        return MyOtemsPage();
                      }));
                      /*Navigator.push(context,
                                new MaterialPageRoute(
                                    builder: (BuildContext context) {
                                      return IByCategorytemsPage(
                                          e.category_id, e.category_name, "");
                                    }));*/
                    },
                    child: Text(
                      "view all".tr(),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: ColorsConst.col_app_blue),
                    )),
                SvgPicture.asset(context.locale != "en"
                    ? "assets/icons/arr2.svg"
                    : "assets/icons/arr.svg"),*/
                Container(
                  width: 16.w,
                )
              ])),
              Expanded(
                  child: FeaturedItems(user
                      .items!) /* ListView(
                      padding: EdgeInsets.symmetric(horizontal: 16.w),
                      children: user.items!
                          .map((e) => Column(children: [
                                FeaturedItems(user.items!),
                                Container(
                                  height: ScreenUtil().setHeight(14),
                                ),
                              ]))
                          .toList())*/
                  )
            ]),
    );
  }
}
