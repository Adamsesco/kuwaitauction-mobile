import 'dart:convert';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_toggle_tab/flutter_toggle_tab.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/app/utils/validators.dart';
import 'package:kauction/views/custom_widgets/app_textfield.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';
import 'package:kauction/views/custom_widgets/toggle_bar.dart';
import 'package:kauction/views/home/home_screen.dart';
import 'package:kauction/views/home/settings/my_items.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class Publish2 extends StatefulWidget {
  Publish2(this.json);

  var json;

  bool swit = true;

  @override
  _Publish2State createState() => _Publish2State();
}

class _Publish2State extends State<Publish2> {
  bool fixed = false;
  String condition = "";
  String duration = "";
  var _startprice = TextEditingController();
  var _price = TextEditingController();
  bool swit = true;
  String _selectedDate = '';
  RestService rest = RestService();
  bool show = false;
  final _formKey = GlobalKey<FormState>();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? d = await showDatePicker(
      initialDate: DateTime.now(),
      context: context,
      firstDate: DateTime(2015),
      lastDate: DateTime(2022),
    );
    if (d != null)
      setState(() {
        _selectedDate = new DateFormat.yMMMMd("en_US").format(d);
      });
  }

  final FocusNode _nodeText1 = FocusNode();
  final FocusNode _nodeText2 = FocusNode();
  final FocusNode _nodeText3 = FocusNode();
  final FocusNode _nodeText4 = FocusNode();
  final FocusNode _nodeText5 = FocusNode();
  final FocusNode _nodeText6 = FocusNode();
  final FocusNode _nodeTextp = FocusNode();
  final FocusNode _nodeTextp2 = FocusNode();

  /// Creates the [KeyboardActionsConfig] to hook up the fields
  /// and their focus nodes to our [FormKeyboardActions].
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      keyboardBarColor: Colors.grey[200],
      nextFocus: true,
      actions: [
        KeyboardActionsItem(
          focusNode: _nodeTextp,
        ),
        KeyboardActionsItem(focusNode: _nodeTextp2, toolbarButtons: [
          (node) {
            return GestureDetector(
              onTap: () => node.unfocus(),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Icon(Icons.close),
              ),
            );
          }
        ]),
        KeyboardActionsItem(
          focusNode: _nodeText3,
          onTapAction: () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    content: Text("Custom Action"),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("OK"),
                        onPressed: () => Navigator.of(context).pop(),
                      )
                    ],
                  );
                });
          },
        ),
        KeyboardActionsItem(
          focusNode: _nodeText4,
          //   displayCloseWidget: false,
        ),
        KeyboardActionsItem(
          focusNode: _nodeText5,
          toolbarButtons: [
            //button 1
            (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "CLOSE",
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              );
            },
            //button 2
            (node) {
              return GestureDetector(
                onTap: () => node.unfocus(),
                child: Container(
                  color: Colors.black,
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "DONE",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              );
            }
          ],
        ),
        KeyboardActionsItem(
          focusNode: _nodeText6,
          footerBuilder: (_) => PreferredSize(
              child: SizedBox(
                  height: 40,
                  child: Center(
                    child: Text('Custom Footer'),
                  )),
              preferredSize: Size.fromHeight(40)),
        ),
      ],
    );
  }

  submit() async {
    final FormState? form = _formKey.currentState;
    if (form?.validate() == false) {
      //  showInSnackBar(
      //  "Veuillez corriger les erreurs en rouge");
    } else {
      setState(() {
        show = true;
      });
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? id = prefs.getString("id");
      String? token = prefs.getString("token");
      widget.json["price"] = _price.text;
      //widget.json["user_id"] = "9";
      widget.json["type"] = fixed == true ? 1 : 0;
      widget.json["token"] = token;
      widget.json["buynow"] = _startprice.text;

      print(widget.json);

      // widget.json["model_id"]= 0;
      widget.json["year"] = 0;

      var a = await rest.post("addnewitem", widget.json);

      print(a);

      setState(() {
        show = false;
      });
      Navigator.push(context,
          new MaterialPageRoute(builder: (BuildContext context) {
        return MyOtemsPage();
      }));
    }
    /* Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => HomeScreen()),
        (Route<dynamic> route) => false);*/
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    print("hehheheh");
    print(widget.json["priceless"]);
  }

  @override
  Widget build(BuildContext context) {
    gt(val) {
      setState(() {
        swit = val;
      });
    }

    TextStyle text4 = TextStyle(
        fontSize: ScreenUtil().setSp(14), fontWeight: FontWeight.w500);

    TextStyle text_btn1 = TextStyle(
        fontSize: ScreenUtil().setSp(14), fontWeight: FontWeight.w700);

    TextStyle text_btn2 = TextStyle(
        color: Colors.white,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w700);

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: ColorsConst.col_grey.withOpacity(0.4),
    );

    Widget row_widget(String text, onPressed, {show}) => InkWell(
        onTap: () {
          if (text == "Scheduled for")
            _selectDate(context);
          else
            onPressed();
        },
        child: Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(10),
              bottom: ScreenUtil().setHeight(10),
              left: ScreenUtil().setWidth(13),
              right: ScreenUtil().setWidth(12),
            ),
            child: Row(children: [
              Expanded(
                  child: Text(
                text,
                style: TextStyle(
                    color: (show == true && fixed == true)
                        ? const Color(0xffCCCCCC)
                        : ColorsConst.col_grey,
                    fontSize: ScreenUtil().setSp(14),
                    fontWeight: FontWeight.w500),
              )),
              Text((condition != "" &&
                      condition.toString() != "null" &&
                      text == "Condition")
                  ? condition
                  : ""),
              Text((duration != "" &&
                      duration.toString() != "null" &&
                      text == "Duration")
                  ? duration
                  : ""),
              Text((_selectedDate != "" &&
                      _selectedDate.toString() != "null" &&
                      text == "Scheduled for")
                  ? _selectedDate
                  : ""),
              Container(
                width: 8,
              ),
              SvgPicture.asset("assets/icons/arrow_next.svg",
                  color: (show == true && fixed == true)
                      ? const Color(0xffCCCCCC)
                      : ColorsConst.col_grey),
            ])));

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(8),
          top: ScreenUtil().setHeight(8),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_black,
              fontSize: ScreenUtil().setSp(16.5),
              fontFamily: "poppins_bold",
              fontWeight: FontWeight.w500),
        ));
    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(8), bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              fontSize: ScreenUtil().setSp(12.5), fontWeight: FontWeight.w300),
        ));

    return Scaffold(
        backgroundColor: Colors.white,
        appBar: KauctionAppBar(
          actions: [],
        ),
        body: Container(
            padding: EdgeInsets.only(
                left: ScreenUtil().setWidth(16),
                right: ScreenUtil().setWidth(16)),
            child: KeyboardActions(
              config: _buildConfig(context),
              child: new Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                key: _formKey,
                child: Column(
                    //    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      text("Buying format".tr()),
                      Container(
                        height: 2,
                      ),
                      text2("pleased".tr()),
                      /**
                          TextFieldWidget(
                          "Username".tr(),
                          _focusname,
                          _nameController,
                          TextInputType.text,
                          Validators.validatefirstname,
                          );
                       */
                      ToggleBar(
                          backgroundColor: ColorsConst.fill_color,
                          labels: ["Auction".tr(), "Buy immediately".tr()],
                          onSelectionUpdated: (index) => {
                                if (index == 0)
                                  {setState(() => fixed = false)}
                                else
                                  setState(() => fixed = true)

                                // Do something with index
                              },
                          backgroundBorder: Border()),
                      Container(
                        height: ScreenUtil().setHeight(32),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          text("Price".tr()),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          text2(fixed == false
                              ? "pleasenter".tr()
                              : "addpe".tr()),
                        ],
                      ),
                      TextFieldWidget(
                        "Price".tr(),
                        _nodeTextp,
                        _price,
                        TextInputType.number,
                        widget.json["priceless"].toString() == "1"
                            ? null
                            : Validators.validateprice,
                      ),
                      Container(height: 18.h),
                      fixed == false ? text2("addpe".tr()) : Container(),
                      (fixed == true)
                          ? Container()
                          : TextFieldWidget(
                              "priceop".tr(),
                              _nodeTextp2,
                              _startprice,
                              TextInputType.number,
                              null,
                            ),
                      Container(
                        height: ScreenUtil().setHeight(23),
                      ),
                      Container(
                        height: ScreenUtil().setHeight(35),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                child: PrimaryButton(
                              textStyle: TextStyle(),
                              disabledColor: ColorsConst.col_app,
                              fonsize: 16.5.sp,
                              icon: "",
                              isLoading: false,
                              colorText: ColorsConst.col_app_white,
                              prefix: Container(),
                              color: ColorsConst.col_app,
                              text: "confirm".tr(),
                              onTap: () {
                                submit();
                              },
                            ))
                          ]),
                      Container(height: 32.h),
                      /* Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: ScreenUtil().setWidth(168),
                          height: ScreenUtil().setHeight(46),
                          child: RaisedButton(
                              // color: Config.col_blue,
                              elevation: 1,
                              shape: new RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(28),
                                ),
                              ),
                              onPressed: () async {
                                setState(() {
                                  show = true;
                                });
                                for (String i
                                    in List<String>.from(widget.json["images"])) {
                                  if (i == "") {
                                    widget.json["images"].remove(i);
                                  }
                                }

                                /* SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                                String id = prefs.getString("id");*/
                                var js = {
                                  "title": widget.json["title"],
                                  "description": widget.json["description"],
                                  "subtitle": widget.json["subtitle"],
                                  "user_id": "5",
                                  "category_id":
                                      widget.json["category"].id.toString(),
                                  "sub_category_id":
                                      widget.json["sub_category"].id.toString(),
                                  "type": fixed == true ? 0 : 1,
                                  "starting_price": _startprice.text == ""
                                      ? _price.text
                                      : _startprice.text,
                                  "price": _price.text,
                                  "duration": duration,
                                  "is_special_offer": swit == true ? "1" : false,
                                  "images": widget.json["images"],
                                  "scheduled": _selectedDate,
                                };

                                var a =
                                    await rest.post("addnewitem", json.encode(js));

                                print("------------------------");
                                print(a);
                                setState(() {
                                  show = false;
                                });
                                /*Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (context) => MainScreen()),
                                        (Route<dynamic> route) => false);*/
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  show //CupertinoActivityIndicator()
                                      ? CupertinoTheme(
                                          data: CupertinoTheme.of(context).copyWith(
                                              brightness: Brightness.dark),
                                          child: CupertinoActivityIndicator())
                                      : Container(),
                                  show //CupertinoActivityIndicator()
                                      ? Container(
                                          width: 4,
                                        )
                                      : Container(),
                                  Text(
                                    "CONFIRM",
                                    style: text_btn2,
                                  ),
                                ],
                              ))),
                    ],
                )*/
                    ]),
              ),
            )));
  }
}
