import 'package:flutter/material.dart';

import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kauction/app/routes/router.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/validators.dart';
import 'package:kauction/model/models/category.dart';
import 'package:kauction/model/repositories/publish_repository.dart';
import 'package:kauction/views/custom_widgets/app_textfield.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:kauction/views/home/publish_screens/publish2.dart';

class Publish1 extends StatefulWidget {
  Publish1();

  @override
  _Publish1State createState() => _Publish1State();
}

class _Publish1State extends State<Publish1> {
  var titleCtrl = TextEditingController();
  var subCtrl = TextEditingController();
  var description = TextEditingController();
  String path = "";
  FocusNode? _focus1 = FocusNode();
  FocusNode? _focus2 = FocusNode();
  final _formKey = GlobalKey<FormState>();

  //List<String> list_images = [];
  List<String> list_images = ["", "", "", ""];

  var json = {};

  @override
  void initState() {
    super.initState();
  }

  Future compress_image(File image, index) async {
    File compressedFile =
        (await FlutterNativeImage.compressImage(image.path, quality: 70))
            as File;
    await save_image(compressedFile, index, "image");
    setState(() {
      lst_bl = false;
    });
  }

  save_image(image, int index, String type) async {
    int timestamp = new DateTime.now().millisecondsSinceEpoch;

    if (type == "image") {
      path = "post/img_" + timestamp.toString() + ".jpg";
    } else {
      path = "post/img_" + timestamp.toString() + ".mp4";
    }

    String imge = await PublishRepository.upload_image(image);
    if (this.mounted) {
      setState(() {
        list_images.insert(index, imge.toString());
      });
    }
  }

  bool lst_bl = false;

  pick_image(index) async {
    await showCupertinoDialog(
      context: context,
      builder: (_) => CupertinoAlertDialog(
        content: Column(
          children: [
            Material(
                color: Colors.transparent,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    IconButton(
                      icon: Icon(Icons.close),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ],
                )),
            Text(
              "Publish".tr(),
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.sp),
            ),
            Container(
              height: 24,
            ),
            FlatButton(
              onPressed: () async {
                Navigator.of(context).pop();

                setState(() {
                  lst_bl = true;
                });

                var image = await ImagePicker.platform
                    .pickImage(source: ImageSource.gallery);

                compress_image(File(image!.path), index);
              },
              child: Text('Photo Gallery'.tr()),
            ),
            Divider(),
            FlatButton(
              onPressed: () async {
                Navigator.of(context).pop();
                PickedFile? image = (await ImagePicker.platform
                    .pickImage(source: ImageSource.camera));
                compress_image(File(image!.path), index);
              },
              child: Text('Camera'.tr()),
            ),
          ],
        ),
        actions: <Widget>[],
      ),
    );
  }

  Widget shpe(index) => InkWell(
      onTap: () {
        pick_image(index);
      },
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.r),
              color: ColorsConst.border_color
                  .withOpacity(0.3) //remove this to get plane rectange
              ),
          width: ScreenUtil().setWidth(82),
          height: ScreenUtil().setWidth(82),
          child: list_images[index] == ""
              ? Center(
                  child: SvgPicture.asset("assets/icons/upload.svg"),
                )
              : Stack(children: [
                  Positioned.fill(
                      child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          child: Image.network(
                            list_images[index],
                            fit: BoxFit.cover,
                            width: ScreenUtil().setWidth(100),
                            height: ScreenUtil().setWidth(100),
                          ))),
                  new Positioned(
                      top: 0.0,
                      right: 0.0,
                      child: new InkWell(
                        child: new Center(
                            child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12.r),
                                    color: ColorsConst
                                        .border_color //remove this to get plane rectange
                                    ),
                                padding: EdgeInsets.all(6.w),
                                child: new SvgPicture.asset(
                                  "assets/icons/remove.svg",
                                  width: 10.w,
                                  color: Colors.red[800],
                                ))),
                        onTap: () {
                          setState(() {
                            list_images.remove(list_images[index]);
                          });
                        },
                      ))
                ])));

  submit() async {
    final FormState? form = _formKey.currentState;
    if (form?.validate() == false) {
      //  showInSnackBar(
      //  "Veuillez corriger les erreurs en rouge");
    }
    else if(list_images[0]==""){
      /**
       * onep
       */
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("onep".tr())));

    }
    else {
      json['title'] = titleCtrl.text;
      json['description'] = description.text;
      List images = [];
      for (String i in list_images) {
        if (i != "") {
          images.add({"id": "", "url": i});
        }
      }
      json["images"] = images;

      print(json);

      Navigator.push(context,
          new MaterialPageRoute(builder: (BuildContext context) {
            return Publish2(json);
          }));
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget text_photo(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(8),
          top: ScreenUtil().setHeight(8),
        ),
        child: Text(
          text,
          style: TextStyle(
              //color: Config.col_grey_d,
              fontSize: ScreenUtil().setSp(18),
              fontWeight: FontWeight.w900),
        ));

    Widget text2_photo(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(8), bottom: ScreenUtil().setHeight(0)),
        child: Text(
          text,
          style: TextStyle(
              // color: Config.col_grey_d,
              //fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(14.5),
              fontWeight: FontWeight.w300),
        ));
    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(8), bottom: ScreenUtil().setHeight(8)),
        child: Text(
          text,
          style: TextStyle(
              //  color: Config.col_grey_d,
              //fontFamily: "Roboto light",
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w300),
        ));

    Widget textfield(
            String name, TextEditingController texted, FocusNode focus, max,val) =>
        TextFieldWidget(
          name,
          focus,
          texted,
          TextInputType.text,
          val,
          maxLines: max,
        );

    ;

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(8),
          top: ScreenUtil().setHeight(8),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_black,
              fontSize: ScreenUtil().setSp(16.5),
              fontFamily: "poppins_bold",
              fontWeight: FontWeight.w500),
        ));

    return  new Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formKey,

        child: ListView(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(16), right: ScreenUtil().setWidth(16)),
        children: [
          Container(
            height: 12.h,
          ),

          text("add_new".tr()),
          Container(
            height: 2,
          ),


          Container(height: 8.h),
          text("pics".tr()),
          text2_photo("Please upload real Pictures.".tr()),

         // Container(height: 12.h),

          (lst_bl == true)
              ? Center(child: CupertinoActivityIndicator())
              : Container(),
          Container(
            height: 6.h,
          ),
          Container(height: 20.h),
          Container(
              height: ScreenUtil().setHeight(100),
              child: Padding(
                  padding: EdgeInsets.only(
                      left: ScreenUtil().setWidth(8),
                      right: ScreenUtil().setWidth(8)),
                  child: GridView.count(
                      childAspectRatio:
                      ScreenUtil().setWidth(82) / ScreenUtil().setWidth(82),
                      crossAxisCount: 4,
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisSpacing: ScreenUtil().setWidth(4).toDouble(),
                      mainAxisSpacing: ScreenUtil().setHeight(10).toDouble(),
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(4).toDouble(),
                      ),
                      children: list_images
                          .map((String e) => shpe(list_images.indexOf(e)))
                          .toList()))),
          text2("add_d".tr()),
          Container(
            height: 20.h,
          ),
          textfield("title".tr(), titleCtrl, _focus1!, 1,Validators.validatetitle),
          Container(
            height: ScreenUtil().setHeight(10),
          ),
          //textfield("SubTitle", subCtrl, 1),
          Container(
            height: ScreenUtil().setHeight(8),
          ),
          textfield("desc".tr(), description, _focus2!, 5,Validators.validatedesc),
          Container(height: 20.h),
          Divider(color: ColorsConst.border_color, height: 1.5),
          Container(height: 12.h),
          text("category".tr()),
          text2_photo("please_c".tr()),

          //  Container(height: 20.h),

          Divider(color: ColorsConst.border_color),
          InkWell(
              onTap: () async {
                var js = await Navigator.pushNamed(
                    context, AppRouter.CAtEgoriesWidgetList) as Map;
                if (js != null) {
                  print("----------------------------");
                  json["category_id"] = int.parse(js["category"].id as String);
                  json["subcategory_id"] = int.parse(js["sub_category"]!.id);
                  json["brand_id"] = int.parse(js["brand"]!.id);
                  json["model_id"] = int.parse(js["module"]!.id);
                  json["priceless"] = int.parse(js["category"]!.priceless);

                  print(json);

                  //  json["category"] = js["category"]!;
                }
              },
              child: Row(
                children: [
                  Container(width: 8.w),
                  Text(
                    "all_c".tr(),
                    style: AppTextStyles.title2(context.locale),
                  ),
                  Expanded(
                    child: Container(),
                  ),
                  IconButton(
                      onPressed: () {},
                      icon: SvgPicture.asset(
                        context.locale == "en"
                            ? "assets/icons/arrow_next.svg"
                            : "assets/icons/return.svg",
                        color: Colors.black,
                      ))
                ],
              )),
          Container(height: 16.h),

          Divider(color: ColorsConst.border_color, height: 1.5),

          Column(children: [
            Container(
              height: 12,
            ),
            /* Container(
                height: ScreenUtil().setHeight(46),
                width: ScreenUtil().setWidth(135),
                child: RaisedButton(
                  //color: Config.col_blue,
                  elevation: 0,
                  shape: new RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(28),
                    ),
                  ),
                  onPressed: () {
                    /* widget.json['title'] = titleCtrl.text;
                        widget.json['subtitle'] = subCtrl.text;
                        widget.json['description'] = description.text;
                        widget.json["images"] = list_images;

                        Navigator.push(context, new MaterialPageRoute(
                            builder: (BuildContext context) {
                              return Publish4(widget.json);
                            }));*/
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "NEXT",
                        style: TextStyle(color: Colors.white),
                      ),
                      Container(
                        width: ScreenUtil().setWidth(36),
                      ),
                      SvgPicture.asset("assets/icons/arrow_next.svg",
                          color: Colors.white)
                    ],
                  ),
                )),*/
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Expanded(
                  child: PrimaryButton(
                textStyle: TextStyle(),
                disabledColor: ColorsConst.col_app,
                fonsize: 16.5.sp,
                icon: "",
                isLoading: false,

                colorText: ColorsConst.col_app_white,
                prefix: Container(),

                color: ColorsConst.col_app,
                text: "continue".tr(),
                //signInState is SignInInProgress,
                onTap: () {
                  submit();
                },
              ))
            ]),
            Container(
              height: 12,
            ),
          ])
        ]));
  }
}
