import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/category.dart';
import 'package:kauction/model/repositories/categories_services.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/home/publish_screens/sub_ctegories_page.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';

class CategoriesWidgetList extends StatefulWidget {
  @override
  _Publish1State createState() => _Publish1State();
}

class _Publish1State extends State<CategoriesWidgetList> {
  List<Category> cats = [];
  CategoriesService cat = new CategoriesService();
  bool load = true;
  var json = {};

  getList() async {
    var a = await cat.get_all_categories();
    if (!this.mounted) return;
    setState(() {
      cats = a;
      cats.add(Category(id: "0", name: "Other", image: "", check: false));
      load = false;
    });
  }

  Category? choice_category;

  click_cat(Category category) {
    /* setState(() {
      choice_category = null;
      for (Category ct in cats) {
        setState(() {
          ct.check = false;
        });
      }
    });*/
    if (category.check == false) {
      setState(() {
        for (Category ct in cats) {
          setState(() {
            ct.check = false;
          });
        }
        category.check = true;
        choice_category = category;
      });
    } else
      setState(() {
        category.check = false;
        choice_category = null;
      });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getList();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text4 = TextStyle(
        color: ColorsConst.col_black,
        fontSize: ScreenUtil().setSp(14),
        fontWeight: FontWeight.w500);

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: ColorsConst.col_text_grey.withOpacity(0.4),
    );
    Widget row_widget(Category e, onPressed) => Container(
        color: e.check == true
            ? ColorsConst.col_app.withOpacity(0.2)
            : Colors.white,
        child: InkWell(
          child: Column(children: [
            Padding(
                padding: EdgeInsets.only(
                  top: ScreenUtil().setHeight(11),
                  bottom: ScreenUtil().setHeight(11),
                  left: ScreenUtil().setWidth(23),
                  right: ScreenUtil().setWidth(23),
                ),
                child: Row(children: [
                 /* Image.network(e.image,
                      width: ScreenUtil().setWidth(22),
                      height: ScreenUtil().setWidth(22),
                      fit: BoxFit.cover),*/
                  Container(
                    width: ScreenUtil().setWidth(12),
                  ),
                  Expanded(
                      child: Text(
                    e.name,
                    style: text4,
                  )),
                  SvgPicture.asset("assets/images/icons/arrow.svg"),
                ])),
            divid
          ]),
          onTap: () {
            onPressed(e);
          },
        ));

    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_text_grey,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            right: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_black,
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w400),
        ));

    return Scaffold(
        appBar: KauctionAppBar(
          actions: [],
        ),
        body: load == true
            ? Center(
                child: CupertinoActivityIndicator(),
              )
            : ListView(children: [
                Container(
                  height: 2,
                ),
                text2("select_category".tr()),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: cats.map((e) => row_widget(e, click_cat)).toList(),
                ),
                Container(
                  height: ScreenUtil().setHeight(52),
                ),
                Column(children: [
                  Container(
                      height: ScreenUtil().setHeight(46),
                      width: ScreenUtil().setWidth(135),
                      child: RaisedButton(
                        color: choice_category == null
                            ? Color(0xffF1F0F5)
                            : ColorsConst.col_app,
                        elevation: 0,
                        shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(12.r),
                          ),
                        ),
                        onPressed: () {
                          if (choice_category != null) {
                            json["category"] = choice_category;

                            Navigator.push(context, new MaterialPageRoute(
                                builder: (BuildContext context) {
                              return subCategories(json);
                            }));
                          }
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "NEXT".tr(),
                              style: TextStyle(
                                  color: choice_category == null
                                      ? Color(0xffcccccc)
                                      : Colors.white),
                            ),
                            Container(
                              width: ScreenUtil().setWidth(36),
                            ),
                            SvgPicture.asset(
                                "assets/icons/arrow_next.svg",
                                color: choice_category == null
                                    ? Color(0xffcccccc)
                                    : Colors.white)
                          ],
                        ),
                      )),
                ])
              ]));
  }
}
