import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/category.dart';
import 'package:kauction/model/repositories/categories_services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:easy_localization/easy_localization.dart';
class MuduleList extends StatefulWidget {
  MuduleList(this.json);

  var json = {};

  @override
  _MuduleListState createState() => _MuduleListState();
}

class _MuduleListState extends State<MuduleList> {

  List<Category> cats = [];
  CategoriesService cat = new CategoriesService();
  bool load = true;

  getList() async {
    var a = await cat.get_modules_categories(widget.json["category"].id);
    if (!this.mounted) return;
    setState(() {
      cats = List<Category>.from(a);
      load = false;
    });

    if (cats.length == 0) {
      widget.json["module"] =
          Category(id: "0", name: "", image: "", check: false);

      Navigator.pop(context, widget.json);
      Navigator.pop(context, widget.json);
      Navigator.pop(context, widget.json);
      Navigator.pop(context, widget.json);

    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getList();
  }

  Category? choice_category;

  click_sub_cat(Category category) {
    if (category.check == false) {
      setState(() {
        for (Category ct in cats) {
          setState(() {
            ct.check = false;
          });
        }
        category.check = true;
        choice_category = category;
      });
    } else
      setState(() {
        category.check = false;
        choice_category = null;
      });
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text4(Category category) => TextStyle(
        color: category.check ? ColorsConst.col_app : ColorsConst.col_grey,
        fontSize: ScreenUtil().setSp(category.check ? 16 : 14),
        fontWeight: category.check ? FontWeight.w900 : FontWeight.w500);

    Widget divid = Container(
      margin: EdgeInsets.only(
        left: ScreenUtil().setWidth(12),
        right: ScreenUtil().setWidth(12),
      ),
      width: MediaQuery.of(context).size.width,
      height: 1,
      color: ColorsConst.col_grey.withOpacity(0.4),
    );
    Widget row_widget(Category category, onPressed) => InkWell(
      child: Column(children: [
        Padding(
            padding: EdgeInsets.only(
              top: ScreenUtil().setHeight(0),
              bottom: ScreenUtil().setHeight(0),
              left: ScreenUtil().setWidth(23),
              right: ScreenUtil().setWidth(23),
            ),
            child: Row(children: [
              Expanded(
                  child: Text(
                    category.name.toString(),
                    style: text4(category),
                  )),
              new IconButton(
                padding: EdgeInsets.all(0.0),
                onPressed: () {
                  //widget.func();

                  onPressed(category);
                },
                icon: new Container(
                    margin: new EdgeInsets.only(left: 0.0, right: 0.0),
                    child: Container(
                      child: Container(
                        width: 10,
                        height: 10,
                      ),
                      decoration: new BoxDecoration(
                        color: category.check
                            ? Colors.white
                            : Colors.grey[200],
                        border: new Border.all(
                            width: 5.0,
                            color: category.check
                                ? ColorsConst.col_app
                                : Colors.grey),
                        borderRadius: const BorderRadius.all(
                            const Radius.circular(36.0)),
                      ),
                    )),
              )
            ])),
        divid
      ]),
      onTap: () {
        onPressed(category);
      },
    );
    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_black,
              fontSize: ScreenUtil().setSp(16),
              fontWeight: FontWeight.w900),
        ));

    Widget text2(String text) => Container(
        padding: EdgeInsets.only(
            left: ScreenUtil().setWidth(28),
            bottom: ScreenUtil().setHeight(21)),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_black,
              fontSize: ScreenUtil().setSp(12.5),
              fontWeight: FontWeight.w500),
        ));

    return Scaffold(
      appBar: KauctionAppBar(
        actions: [],
      ),
      body: ListView(children: [
        text("Sell your items"),
        Container(
          height: 2,
        ),
        text2("Select a module"),
        load == true
            ? Center(
          child: CupertinoActivityIndicator(),
        )
            : cats.isEmpty
            ? Center(
          child: Text("No item found !"),
        )
            : Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children:
          cats.map((e) => row_widget(e, click_sub_cat)).toList(),
        ),
        Container(
          height: ScreenUtil().setHeight(52),
        ),
        Column(children: [
          Container(
              height: ScreenUtil().setHeight(46),
              width: ScreenUtil().setWidth(135),
              child: RaisedButton(
                color: choice_category == null
                    ? Color(0xffF1F0F5)
                    : ColorsConst.col_app,
                elevation: 0,
                shape: new RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(12.r),
                  ),
                ),
                onPressed: () {
                  //  if (choice_category != null) {
                  widget.json["module"] = choice_category;

                  Navigator.pop(context, widget.json);
                  Navigator.pop(context, widget.json);
                  Navigator.pop(context, widget.json);
                  Navigator.pop(context, widget.json);

                  /*Navigator.push(context,
                      new MaterialPageRoute(builder: (BuildContext context) {
                    return Publish3(widget.json);
                  }));*/
                  // }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "NEXT".tr(),
                      style: TextStyle(
                          color: choice_category == null
                              ? Color(0xffcccccc)
                              : Colors.white),
                    ),
                    Container(
                      width: ScreenUtil().setWidth(36),
                    ),
                    SvgPicture.asset("assets/icons/arrow_next.svg",
                        color: choice_category == null
                            ? Color(0xffcccccc)
                            : Colors.white)
                  ],
                ),
              )),
        ])
      ]),
    );
  }
}
