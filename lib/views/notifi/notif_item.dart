import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/model/models/notification.dart';
import 'package:easy_localization/easy_localization.dart';

class NotificationItem extends StatefulWidget {
  NotificationItem(this.notification, {Key? key}) : super(key: key);
  LNotification notification;

  /*

 ();

   */
  @override
  _NotificationItemState createState() => _NotificationItemState();
}

class _NotificationItemState extends State<NotificationItem> {
  RestService rest = new RestService();

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          ListTile(
            dense: true,
            contentPadding: EdgeInsets.all(6.w),
            leading: widget.notification.status == "1"
                ? Container(width: 1,height: 1,)
                : Container(
                    padding: EdgeInsets.only(top: 8.h, left: 20.w),
                    child: CircleAvatar(
                      radius: 5.r,
                      backgroundColor: ColorsConst.col_app,
                    ),
                  ) /*Container(
          width: ScreenUtil().setWidth(47).toDouble(),
          height: ScreenUtil().setWidth(47).toDouble(),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: NetworkImage(notification.image),
              fit: BoxFit.cover,
            ),
          ),
        ),*/
            ,
            subtitle: Container(
                child: Text(
              widget.notification.time.toString(),
              style: TextStyle(
                  fontSize: ScreenUtil().setSp(12.5),
                  color: ColorsConst.col_grey),
            )),
            title: Container(
                padding: EdgeInsets.only(bottom: 10.h, top: 6.h),
                width: ScreenUtil().setWidth(198),
                child: Text(
                  widget.notification.name.toString(),
                  maxLines: 2,
                  style: TextStyle(
                      fontFamily: context.locale != "en"
                          ? "cairo_bold"
                          : "poppins_bold",
                      fontSize: ScreenUtil().setSp(16.8),
                      color: ColorsConst.col_black),
                )),
            trailing: SvgPicture.asset("assets/images/icons/arrow.svg"),
          ),
          Container(
            margin: EdgeInsets.only(
                top: ScreenUtil().setHeight(20),
                bottom: ScreenUtil().setHeight(20)),
            width: MediaQuery.of(context).size.width,
            height: 1,
            color: ColorsConst.col_grey.withOpacity(0.5),
          )
        ]),
        onTap: () async {
          var b =
              await rest.get("notifications/read/${widget.notification.id}/");
          setState(() {
            widget.notification.status = "1";
          });

          //http://134.122.118.107/kuwaitauction/api/notifications/read/{$notificationId}
        });
  }
}
