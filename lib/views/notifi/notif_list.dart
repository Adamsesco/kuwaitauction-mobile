import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/messenger/util/settings.dart';
import 'package:kauction/model/models/notification.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/notifi/notif_item.dart';

class NotificationsList extends StatefulWidget {
  @override
  _NotificationsListState createState() => _NotificationsListState();
}

class _NotificationsListState extends State<NotificationsList> {
  List<LNotification> list = [];
  Settings_Services settings_Services = Settings_Services();
  bool load = true;

  get_results() async {
    var res = await settings_Services.get_notifs();
    if (!this.mounted) return;
    setState(() {
      list = res;
      load = false;
    });
    print(list);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_results();
  }

  @override
  Widget build(BuildContext context) {
    Widget text(String text) => Container(
        padding: EdgeInsets.only(
          left: ScreenUtil().setWidth(28),
          top: ScreenUtil().setHeight(16),
        ),
        child: Text(
          text,
          style: TextStyle(
              color: ColorsConst.col_app,
              fontSize: ScreenUtil().setSp(20),
              fontWeight: FontWeight.w900),
        ));
    return Scaffold(
        appBar: KauctionAppBar(
          returnn: true,
          actions: [],
        ),
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
          text("Notifications"),
          Container(
            height: 2,
          ),
          load == true
              ? Center(child: CupertinoActivityIndicator())
              : Expanded(
                  child: ListView(
                  children: list
                      .map((LNotification e) => NotificationItem(e))
                      .toList(),
                ))
        ]));
  }
}
