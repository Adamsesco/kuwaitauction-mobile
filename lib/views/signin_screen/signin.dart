import 'dart:async';
import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/routes/router.dart';
import 'package:kauction/app/signin_bloc/signin_bloc.dart';
import 'package:kauction/app/signin_bloc/signin_event.dart';
import 'package:kauction/app/signin_bloc/signin_state.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/validators.dart';
import 'package:kauction/model/models/user.dart';
import 'package:kauction/model/repositories/register_repository.dart';
import 'package:kauction/views/custom_widgets/app_textfield.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';
import 'package:kauction/views/home/home_screen.dart';

class SigninScreen extends StatefulWidget {
  SigninScreen(this.phone, {Key? key}) : super(key: key);
  String phone;

  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _usernameController = new TextEditingController();
  RestService restservice = new RestService();

  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _telController = new TextEditingController();
  FocusNode _focuspass2 = new FocusNode();

  FocusNode _focuspass = new FocusNode();
  TextEditingController _passwordController2 = new TextEditingController();

  FocusNode _focusname = new FocusNode();
  TextEditingController _emailController = new TextEditingController();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool show = false;
  bool _obscureText = true;

  //AppService ap = new AppService();
  FocusNode _focusemail = new FocusNode();
  FocusNode _focustel = new FocusNode();
  late SigninBloc _signbloc;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _signbloc = BlocProvider.of<SigninBloc>(context);
    _telController.text = widget.phone;
  }

  @override
  Widget build(BuildContext context) {
    void showInSnackBar(String value) {
      _scaffoldKey.currentState?.showSnackBar(new SnackBar(
        content: new Text(
          value,
          style: TextStyle(
            fontFamily: context.locale.toString() == "en" ? "poppins" : "cairo",
          ),
        ),
        backgroundColor: Colors.red[900],
      ));
    }

    Widget phone_twidget = TextFieldWidget(
      "phone".tr(),
      _focustel,
      _telController,
      TextInputType.phone,
      Validators.validatePhone,
    );

    Widget pass_twidget = TextFieldWidget("password".tr(), _focuspass,
        _passwordController, TextInputType.text, Validators.validatePassword,
        obscure: _obscureText,
        prefixWidget: Container(
          padding: EdgeInsets.all(4.w),
          height: 36.0.h,
          width: 36.0.w,
          child: RawMaterialButton(
            onPressed: _toggle,
            child: SvgPicture.asset(
              _obscureText
                  ? "assets/icons/show.svg"
                  : "assets/icons/hidden.svg",
              color: ColorsConst.col_grey_fon,
            ),
          ),
        ));

    submit() async {
      final FormState? form = _formKey.currentState;
      if (form?.validate() == false) {
        _autovalidate = true; // Start validating on every change.
        //  showInSnackBar(
        //  "Veuillez corriger les erreurs en rouge");
      } else {
       // FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

        String? tk = await FirebaseMessaging.instance.getToken();

        User me = User(
            userename: _nameController.text,
            email: _emailController.text,
            image: "",
            created: DateTime.now(),
            id: "",
            mobile: _telController.text,
            token: tk!);

        _signbloc.add(
          SignInRequested(
              phone: _telController.text,
              password: _passwordController.text,
              context: context),
        );

        /*   _veriInBloc.add(
            VerifyMembreEventRequested(phone: _phonecontroller.text),
          );*/
      }
    }

    return Scaffold(
        backgroundColor: ColorsConst.col_app_white,
        appBar: KauctionAppBar(
          actions: [],
        ),
        body: BlocListener<SigninBloc, SigninState>(
            listener: (BuildContext context, SigninState signs) {
          if (signs is SigninFailure) {
            final message = signs.errorMessage;

            showCupertinoDialog(
              context: context,
              builder: (context) => CupertinoAlertDialog(
                title: Text(''),
                content: Text(
                  message,
                  style: TextStyle(
                    fontSize: 18.sp,
                    fontFamily:
                        context.locale.toString() == "en" ? "poppins" : "cairo",
                  ),
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('Ok'),
                  ),
                ],
              ),
            );
          } else if (signs is SigninSuccess) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (Route<dynamic> route) => false);
          }
        }, child: BlocBuilder<SigninBloc, SigninState>(
                builder: (context, verifyInState) {
          return new Form(
              key: _formKey,
              //  autovalidate: _autovalidate,
              //onWillPop: _warnUserAboutInvalidData,
              child: Padding(
                  padding: EdgeInsets.all(24.w),
                  child: ListView(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(height: 60.h),
                      Text(
                        "login".tr(),
                        style: AppTextStyles.title(context.locale),
                      ),
                      Container(height: 8.h),
                      Text(
                        "welcome2".tr(),
                        style: TextStyle(
                          fontFamily: context.locale.toString() == "en"
                              ? "poppins"
                              : "cairo",
                        ),
                      ),
                      Container(height: 54.h),

                      phone_twidget,
                      Container(height: 10.h),

                      pass_twidget,

                      Container(height: 52.h),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                child: PrimaryButton(
                              textStyle: TextStyle(),
                              disabledColor: ColorsConst.col_app,
                              fonsize: 16.5.sp,
                              icon: "",
                              isLoading: verifyInState is SigninProgress,
                              colorText: ColorsConst.col_app_white,
                              prefix: Container(),
                              color: ColorsConst.col_app,
                              text: "login".tr(),
                              onTap: () {
                                if(verifyInState is SigninProgress == false) {
                                  print("yess");
                                  submit();
                                }
                              },
                            ))
                          ]),
                      Container(height: 20.h),

                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 42.w),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: [
                              TextSpan(
                                  text: "don't have account?".tr(),
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: 'sign up'.tr(),
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontWeight: FontWeight.w400,
                                      color: ColorsConst.col_app,
                                      decoration: TextDecoration.underline)),
                            ]),
                          )),
                      Container(height: 32.h),
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 42.w),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: [
                              TextSpan(
                                  text: 'by'.tr(),
                                  style: TextStyle(
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontSize: 14.sp,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: 'terms'.tr(),
                                  style: TextStyle(
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                      decoration: TextDecoration.underline)),
                              TextSpan(
                                  text: 'and'.tr(),
                                  style: TextStyle(
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      color: Colors.black,
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: 'privacy'.tr(),
                                  style: TextStyle(
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                      fontSize: 14.sp,
                                      decoration: TextDecoration.underline))
                            ]),
                          ))

                      //  Container(height: 80.h),
                    ],
                  )));
        })));
  }
}
