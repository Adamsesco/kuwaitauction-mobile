import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/routes/router.dart';
import 'package:kauction/app/signin_bloc/signin_bloc.dart';
import 'package:kauction/app/signin_bloc/signin_event.dart';
import 'package:kauction/app/signup_bloc/register_bloc.dart';
import 'package:kauction/app/signup_bloc/register_event.dart';
import 'package:kauction/app/signup_bloc/register_state.dart';
import 'package:kauction/app/utils/app_textstyles.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/validators.dart';
import 'package:kauction/model/models/user.dart';
import 'package:kauction/views/custom_widgets/app_textfield.dart';
import 'package:kauction/views/custom_widgets/kauction_appbar.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';
import 'package:kauction/views/home/home_screen.dart';

class SignupScreen extends StatefulWidget {
  SignupScreen(this.phone, {Key? key}) : super(key: key);
  String phone;

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController _nameController = new TextEditingController();
  TextEditingController _usernameController = new TextEditingController();
  RestService restservice = new RestService();

  TextEditingController _passwordController = new TextEditingController();
  TextEditingController _telController = new TextEditingController();
  FocusNode _focuspass2 = new FocusNode();
  SigninBloc? _signInBloc;

  FocusNode _focuspass = new FocusNode();
  TextEditingController _passwordController2 = new TextEditingController();

  FocusNode _focusname = new FocusNode();
  TextEditingController _emailController = new TextEditingController();
  bool _autovalidate = false;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  bool show = false;
  bool _obscureText = true;
  bool _obscureText2 = true;

  //AppService ap = new AppService();
  FocusNode _focusemail = new FocusNode();
  FocusNode _focustel = new FocusNode();
  late RegisterBloc _registerBloc;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);

    print("---------------------------" + widget.phone);
    setState(() {
      _telController.text = widget.phone;
    });
  }

  @override
  Widget build(BuildContext context) {
    void showInSnackBar(String value) {
      _scaffoldKey.currentState?.showSnackBar(new SnackBar(
        content: new Text(
          value,
          style: TextStyle(),
        ),
        backgroundColor: Colors.red[900],
      ));
    }

    Widget phone_twidget = TextFieldWidget(
      "phone".tr(),
      _focustel,
      _telController,
      TextInputType.phone,
      Validators.validatePhone,
    );

    Widget email_twidget = TextFieldWidget(
      "email".tr(),
      _focusemail,
      _emailController,
      TextInputType.emailAddress,
      Validators.validateEmail,
    );

    Widget name_twidget = TextFieldWidget(
      "Username".tr(),
      _focusname,
      _nameController,
      TextInputType.text,
      Validators.validatefirstname,
    );

    Widget pass_twidget = TextFieldWidget("password".tr(), _focuspass,
        _passwordController, TextInputType.text, Validators.validatePassword,
        obscure: _obscureText,
        prefixWidget: Container(
          padding: EdgeInsets.all(4.w),
          height: 36.0.h,
          width: 36.0.w,
          child: RawMaterialButton(
            onPressed: _toggle,
            child: SvgPicture.asset(
              _obscureText
                  ? "assets/icons/show.svg"
                  : "assets/icons/hidden.svg",
              color: ColorsConst.col_grey_fon,
            ),
          ),
        ));

    Widget confirm_twidget = TextFieldWidget("confirmpass".tr(), _focuspass2,
        _passwordController2, TextInputType.text, Validators.validatePassword,
        obscure: _obscureText2,
        prefixWidget: Container(
          padding: EdgeInsets.all(4.w),
          height: 36.0.h,
          width: 36.0.w,
          child: RawMaterialButton(
            onPressed: _toggle2,
            child: SvgPicture.asset(
              _obscureText2
                  ? "assets/icons/show.svg"
                  : "assets/icons/hidden.svg",
              color: ColorsConst.col_grey_fon,
            ),
          ),
        ));

    submit() {
      final FormState? form = _formKey.currentState;
      if (form?.validate() == false) {
        _autovalidate = true; // Start validating on every change.
        //  showInSnackBar(
        //  "Veuillez corriger les erreurs en rouge");
      } else {
        User me = User(
            userename: _nameController.text,
            email: _emailController.text,
            image: "",
            password: _passwordController.text,
            created: DateTime.now(),
            id: "",
            mobile: _telController.text,
            token: "");

        _registerBloc.add(
          RegisterRequested(user: me, context: context),
        );

        /*   _veriInBloc.add(
            VerifyMembreEventRequested(phone: _phonecontroller.text),
          );*/
      }
    }

    return Scaffold(
        backgroundColor: ColorsConst.col_app_white,
        appBar: KauctionAppBar(
          actions: [],
        ),
        body: BlocListener<RegisterBloc, RegisterState>(
            listener: (BuildContext context, RegisterState verifyInState) {
          if (verifyInState is RegisterFailure) {
            final message = verifyInState.errorMessage;

            showCupertinoDialog(
              context: context,
              builder: (context) => CupertinoAlertDialog(
                title: Text(''),
                content: Text(
                  message,
                  style: TextStyle(fontSize: 18.sp),
                ),
                actions: <Widget>[
                  CupertinoDialogAction(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('Ok'),
                  ),
                ],
              ),
            );
          } else if (verifyInState is RegisterSuccess) {
            _signInBloc = BlocProvider.of<SigninBloc>(context);
            _signInBloc!.add(SignInSucccessEvent(user: verifyInState.user));

            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (Route<dynamic> route) => false);

            /* print(verifyInState.code);

                  Navigator.pushReplacementNamed(context, AppRouter.PHONECODE,
                      arguments: _phonecontroller.text);*/
          }
        }, child: BlocBuilder<RegisterBloc, RegisterState>(
                builder: (context, verifyInState) {
          return new Form(
              key: _formKey,
              //  autovalidate: _autovalidate,
              //onWillPop: _warnUserAboutInvalidData,
              child: Padding(
                  padding: EdgeInsets.all(24.w),
                  child: ListView(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(height: 60.h),
                      Text(
                        "sign up".tr(),
                        style: AppTextStyles.title(context.locale),
                      ),
                      Container(height: 8.h),
                      Text("Create New Account".tr()),
                      Container(height: 54.h),
                      name_twidget,
                      Container(height: 10.h),
                      phone_twidget,
                      Container(height: 10.h),
                      email_twidget,
                      Container(height: 10.h),

                      pass_twidget,
                      Container(height: 10.h),

                      confirm_twidget,

                      Container(height: 52.h),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                child: PrimaryButton(
                              textStyle: TextStyle(),
                              disabledColor: ColorsConst.col_app,
                              fonsize: 16.5.sp,
                              icon: "",
                              isLoading: verifyInState is RegisterProgress,
                              colorText: ColorsConst.col_app_white,
                              prefix: Container(),
                              color: ColorsConst.col_app,
                              text: "Register".tr(),
                              onTap: () {
                                submit();
                              },
                            ))
                          ]),
                      Container(height: 20.h),

                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 42.w),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: [
                              TextSpan(
                                  text: 'already have an account?'.tr(),
                                  style: TextStyle(
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontSize: 14.sp,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: 'sign in'.tr(),
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontWeight: FontWeight.w400,
                                      color: ColorsConst.col_app,
                                      decoration: TextDecoration.underline)),
                            ]),
                          )),
                      Container(height: 32.h),
                      Padding(
                          padding: EdgeInsets.symmetric(horizontal: 42.w),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: [
                              TextSpan(
                                  text: 'by'.tr(),
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      color: Colors.black,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: 'terms'.tr(),
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                      decoration: TextDecoration.underline)),
                              TextSpan(
                                  text: 'and'.tr(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w400)),
                              TextSpan(
                                  text: 'privacy'.tr(),
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black,
                                      fontFamily:
                                          context.locale.toString() == "en"
                                              ? "poppins"
                                              : "cairo",
                                      fontSize: 14.sp,
                                      decoration: TextDecoration.underline))
                            ]),
                          ))

                      //  Container(height: 80.h),
                    ],
                  )));
        })));
  }
}
