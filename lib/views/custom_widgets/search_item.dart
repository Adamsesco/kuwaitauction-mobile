import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/product.dart';
import 'package:kauction/views/home/home_first_screen/details_products.dart';
import 'package:easy_localization/easy_localization.dart';

class ImageTile extends StatelessWidget {
  ImageTile({
    Key? key,
    required this.product,
    required this.index,
    required this.width,
    required this.height,
  }) : super(key: key);

  final int index;
  final double width;
  final double height;
  Product product;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        child: Stack(children: [
          Positioned.fill(
              child: product.images.isEmpty
                  ? Container()
                  : product.images[0].url == ""
                      ? Container()
                      : ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          child: FadeInImage.assetNetwork(
                            image: product.images[0].url,
                            placeholder: "assets/images/placeholder.png",
                            width: width.toDouble(),
                            height: height.toDouble(),
                            fit: BoxFit.cover,
                          ))),
          Positioned(
              bottom: 8.h,
              left: 8.w,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    product.bids.length.toString() + " Bids - ends ",
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: ColorsConst.col_app_white,
                      height: 1.4,
                      fontSize: ScreenUtil().setSp(11.5),
                    ),
                  ),
                  Container(
                    height: 4.h,
                  ),
                  Text(
                    product.last_price.toString() +
                        ((product.last_price.toString() == "") ? "" : " KD"),
                    style: TextStyle(
                        color: ColorsConst.col_app_white,
                        fontFamily: context.locale.toString() == "en"
                            ? "poppins_bold"
                            : "cairo_bold",
                        fontSize: ScreenUtil().setSp(18.5),
                        fontWeight: FontWeight.w900),
                  )
                ],
              ))
        ]) /*Image.network(
      'https://picsum.photos/$width/$height?random=$index',
      width: width.toDouble(),
      height: height.toDouble(),
      fit: BoxFit.cover,
    )*/
        ,
        onTap: () {
          Navigator.push(context,
              new MaterialPageRoute(builder: (BuildContext context) {
            return DetailsProduct(product);
          }));
        });
  }
}
