import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';

class RoundButtonIcon extends StatelessWidget {
  RoundButtonIcon({ required this.onPress,required this.icon, this.color,Key? key})
      : super(key: key);
//  Color color;
  Function onPress;
  String icon;
  Color? color;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 44.w,
        height: 44.w,
        child: FloatingActionButton(
            mini: true,
            elevation: 0,
            heroTag: null,
            backgroundColor:  color==null?ColorsConst.col_back_button:color,
            onPressed: () {
              onPress();
            },
            child: SvgPicture.asset(icon)/*Icon(
              Icons.arrow_forward,
              color: Colors.white,
            )*/));
  }
}
