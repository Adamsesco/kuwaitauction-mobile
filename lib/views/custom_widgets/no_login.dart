import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/routes/router.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';

class NoLogin extends StatelessWidget {
  NoLogin(this.go_home, {Key? key}) : super(key: key);
  bool go_home = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(22.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            go_home == false
                ? Container()
                : ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    child:
                        SvgPicture.asset("assets/images/logo.svg", width: 100.w)),
            Container(height: go_home == false ? 0.0.h : 64.h),
            Container(
                child: Text(
              "touse".tr(),
              style: TextStyle(fontSize: 12.sp),
            )),
            Container(height: 22.h),
            PrimaryButton(
              textStyle: TextStyle(),
              disabledColor: ColorsConst.col_app,
              fonsize: 16.5.sp,
              icon: "",
              isLoading: false,
              colorText: ColorsConst.col_app_white,
              prefix: Container(),
              color: ColorsConst.col_app,
              text: "login".tr(),
              onTap: () async {
                Navigator.pushNamed(
                  context,
                  AppRouter.PHONEVerification,
                );
              },
            ),
            Container(height: 20.h),
            PrimaryButton(
              textStyle: TextStyle(),
              disabledColor: ColorsConst.col_app,
              fonsize: 16.5.sp,
              inverted: true,
              icon: "",
              isLoading: false,
              colorText: ColorsConst.col_app,
              prefix: Container(),
              color: ColorsConst.col_app_white,
              text: "sign up".tr(),
              onTap: () {
                Navigator.pushNamed(
                  context,
                  AppRouter.PHONEVerification,
                );
              },
            ),
            Container(
              height: 10.h,
            ),
            go_home == false
                ? FlatButton(
                    onPressed: () {},
                    child: Text(
                      "willdo".tr(),
                      style: TextStyle(
                          color: ColorsConst.col_app_blue,
                          fontWeight: FontWeight.w700),
                    ))
                : Container(),

          ],
        ));
  }
}
