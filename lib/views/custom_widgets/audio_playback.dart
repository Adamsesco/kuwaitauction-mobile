//*************   © Copyrighted by Thinkcreative_Technologies. An Exclusive item of Envato market. Make sure you have purchased a Regular License OR Extended license for the Source Code from Envato to use this product. See the License Defination attached with source code. *********************

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/views/custom_widgets/audioplayer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

typedef void OnError(Exception exception);

enum PlayerState { stopped, playing, paused }

class AudioPlayback extends StatefulWidget {
  final String? url;
  final Widget downloadwidget;

  AudioPlayback({this.url, required this.downloadwidget});

  @override
  _AudioPlaybackState createState() => _AudioPlaybackState();
}

class _AudioPlaybackState extends State<AudioPlayback> {
  Duration? duration;
  Duration? position;

  late AudioPlayer1 audioPlayer;

  String? localFilePath;

  PlayerState playerState = PlayerState.stopped;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';

  get positionText =>
      position != null ? position.toString().split('.').first : '';

  bool isMuted = false;

  late StreamSubscription _positionSubscription;
  late StreamSubscription _audioPlayerStateSubscription;

  @override
  void initState() {
    super.initState();
    initAudioPlayer();
  }

  @override
  void dispose() {
    _positionSubscription.cancel();
    _audioPlayerStateSubscription.cancel();
    audioPlayer.stop();
    super.dispose();
  }

  void initAudioPlayer() {
    audioPlayer = AudioPlayer1();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = Duration(seconds: 0);
        position = Duration(seconds: 0);
      });
    });
  }

  Future play() async {
    await audioPlayer.play(widget.url);
    setState(() {
      playerState = PlayerState.playing;
    });
  }

  Future pause() async {
    await audioPlayer.pause();
    setState(() => playerState = PlayerState.paused);
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      playerState = PlayerState.stopped;
      position = Duration();
    });
  }

  Future mute(bool muted) async {
    await audioPlayer.mute(muted);
    setState(() {
      isMuted = muted;
    });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.min,
          children: [
            Material(color: Colors.white70, child: _buildPlayer()),
          ],
        ),
      ),
    );
  }

  Widget _buildPlayer() => Container(
        // width: 300,
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Color(0xfff5F5F5),
          borderRadius: BorderRadius.all(Radius.circular(32)),
        ),
        child: Row(children: [
          Container(
            width: 6.w,
          ),

          CircleAvatar(
              backgroundColor: ColorsConst.col_app,
              child: Center(
                  child: IconButton(
                onPressed: isPlaying ? () => pause() : () => play(),
                // iconSize: 34.0,
                icon: Icon(isPlaying ? Icons.pause : Icons.play_arrow),
                color: Colors.white,
              ))),

          Expanded(
              child: Container(
                  // width: 106.w,
                  child: Column(
            children: [
              Slider(
                  activeColor: ColorsConst.col_app_blue,
                  inactiveColor: ColorsConst.col_grey,
                  value: duration == null
                      ? 0.0
                      : position?.inMilliseconds.toDouble() ?? 0.0,
                  onChanged: (double value) {
                    audioPlayer.seek((value / 1000).roundToDouble());
                  },
                  min: 0.0,
                  max: duration == null
                      ? 0.0
                      : duration!.inMilliseconds.toDouble()),
            ],
          ))),
          Text(
            position != null
                ? "${positionText ?? ''} / ${durationText ?? ''}"
                : duration != null
                    ? durationText
                    : '',
            style: TextStyle(
                fontSize: 11.0,
                fontWeight: FontWeight.w500,
                color: ColorsConst.col_grey),
          ),
          Container(
            width: 12.w,
          ),
          SvgPicture.asset(
            "assets/icons/audio.svg",
            color: ColorsConst.col_grey,
          ),
          Container(
            width: 12.w,
          )
          /*  IconButton(
                onPressed: isPlaying || isPaused ? () => stop() : null,
                iconSize: 34.0,
                icon: Icon(Icons.stop),
                color: Colors.cyan,
              ),*/
          //  widget.downloadwidget
        ]),

        // if (position != null) _buildMuteButtons(),
      );
}
