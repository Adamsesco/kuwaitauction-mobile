import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:kauction/views/home/home_screen.dart';
class KauctionAppBar extends StatelessWidget implements PreferredSizeWidget {
  KauctionAppBar(
      {this.title_widget,
      required this.actions,
      this.returnn = false,
      Key? key})
      : super(key: key);
  List<Widget> actions = [];
  bool returnn;
  Widget? title_widget;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      title: title_widget == null
          ? Text(
              "Kauction",
              style: TextStyle(
                  fontFamily: "poppins_medium",
                  color: ColorsConst.col_black,
                  fontSize: 24.5.sp,
                  fontWeight: FontWeight.w900),
            )
          : title_widget,
      leading: returnn == true
          ? Padding(
              padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 6.h),
              child: FloatingActionButton(
                  mini: true,
                  heroTag: null,
                  elevation: 0,
                  backgroundColor: ColorsConst.col_back_button,
                  child: Center(
                      child: SvgPicture.asset(
                    context.locale.toString() == "en"
                        ? "assets/icons/return.svg"
                        : "assets/icons/arrow_next.svg",
                    color: Colors.black,
                  )),
                  onPressed: () {
                    Navigator.pop(context);
                  }))
          : IconButton(
              onPressed: () {
                // Get.back();
              },
              icon:InkWell(
                  onTap: (){
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => HomeScreen()),
                            (Route<dynamic> route) => false);
                  },
                  child:  ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  child: SvgPicture.asset(
                    "assets/images/logo.svg",
                  )))),
      backgroundColor: ColorsConst.col_app_white,
      shadowColor: Colors.transparent,
      actions: actions,
    );
  }

  static final _appBar = AppBar();

  @override
  Size get preferredSize => _appBar.preferredSize;
}
