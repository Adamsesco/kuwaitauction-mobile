import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/colors_const.dart';

class TextFieldWidget extends StatelessWidget {
  TextFieldWidget(
    this.name,
    this.focus,
    this.myController,
    this.type,
    this.validator, {
    this.obscure = false,
    this.prefixWidget,
    this.submit,
    this.suffixIcon,
    this.maxLines = 1,
  });

  String name;
  FocusNode focus;
  final prefixWidget;
  TextEditingController myController = new TextEditingController();
  var validator;
  late String? suffixIcon;
  var type;
  bool obscure;
  int maxLines = 1;

  var submit;

  @override
  Widget build(BuildContext context) {
    Widget textfield = TextFormField(
      style: new TextStyle(fontSize: 16.0.sp, color: Colors.black),
      obscureText: obscure,
      controller: myController,
      focusNode: focus,
      maxLines: maxLines,
      textInputAction: TextInputAction.done,

      decoration: InputDecoration(
          suffixIcon: prefixWidget == null
              ? SizedBox(width: 1, height: 1)
              : prefixWidget,
          contentPadding: new EdgeInsets.all(12.0.w),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.red, width: 5.0),
          ),
          fillColor: ColorsConst.fill_color,
          filled: true,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.r)),
            borderSide: BorderSide(
                width: 1, color: ColorsConst.border_color),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12.r)),
            borderSide: BorderSide(width: 1, color: ColorsConst.border_color),
          ),
          hintText: name,
          hintStyle: TextStyle(color: ColorsConst.hintColor)),
      keyboardType: type,
      validator: validator,
      onChanged: (val) {
        if (submit != null) {
          submit();
        }
      },
      /* onSaved: (val) {
        myController.text = val;
        submit();
      },
      onFieldSubmitted: (val) {
        myController.text = val;
        submit();
      },*/
    );

    return textfield;
  }
}
