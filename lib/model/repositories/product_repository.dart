import 'dart:convert';
import 'package:easy_localization/easy_localization.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/model/models/category.dart';
import 'package:kauction/model/models/product.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProductSServices {
  RestService rest = new RestService();

  get_purchases() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var id = prefs.getString("id");
    var pr_resp = await rest.get('purchases/4');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    var sp = pr_resp;
    print(sp);

    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }

  get_details_product(String id_product) async {
    var pr_resp = await rest.get('showitem?id=${id_product}');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    var sp = pr_resp;

    print(pr_resp);
    if (sp == null)
      return null;
    else
      return new Product.fromMap(sp["result"]);
  }

  get_bids() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var id = prefs.getString("id");
    var pr_resp = await rest.get('bids/$id');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    var sp = pr_resp;

    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }

  get_product_by_sub_category(String id) async {
    var pr_resp = await rest.get('subcategory/$id');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    if (pr_resp != null) {
      List sp = pr_resp["products"];
      if (sp == null)
        return List<Product>.from([]);
      else
        return sp
            .map((var contactRaw) => new Product.fromMap(contactRaw))
            .toList();
    } else
      return List<Product>.from([]);
  }

  get_product_by_category(String id) async {
    var pr_resp = await rest.get('category/$id');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    if (pr_resp != null) {
      List sp = pr_resp["products"];
      if (sp == null)
        return List<Product>.from([]);
      else
        return sp
            .map((var contactRaw) => new Product.fromMap(contactRaw))
            .toList();
    } else
      return List<Product>.from([]);
  }

  get_all_product() async {
    var pr_resp = await rest.get('Allproducts');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["products"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }

  get_product_all_bids() async {
    var pr_resp = await rest.get('AllBids');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["products"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }

  get_product_by_id(String id) async {
    var pr_resp = await rest.get('category/$id');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["result"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw["items"][""]))
          .toList();
  }

  get_product_by_myitems(String token) async {
    var pr_resp = await rest.get('myitems?token=$token');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["result"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }

  get_product_by_favorites(String token) async {
    var pr_resp = await rest.get('favorites?token=$token');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["data"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }

  homescreen(String text) async {
    //{{BASEURL}}/api/search?q=Tesla
    var pr_resp = await rest.get('HomeScreen');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["result"];
    if (sp == null)
      return List<CategoryModel>.from([]);
    else
      return sp
          .map((var contactRaw) => new CategoryModel.fromMap(contactRaw))
          .toList();
  }

  get_product_result_search(String text) async {
    //{{BASEURL}}/api/search?q=Tesla
    var pr_resp = await rest.get('search?q=$text');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["result"];
    if (sp == null)
      return List<Product>.from([]);
    else
      return sp
          .map((var contactRaw) => new Product.fromMap(contactRaw))
          .toList();
  }
}
