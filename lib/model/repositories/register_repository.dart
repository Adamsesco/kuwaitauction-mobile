import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/model/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterRepository {
  RestService restservice = new RestService();

  Future<RegisterResponse> registeer_account(User a_user) async {
    print(a_user.email);
    var res = await save_user_informations(a_user);
    return res;
  }

  signin(String phone, String password) async {
    print({"username": phone, "password": password});
    String? tk = await FirebaseMessaging.instance.getToken();
    print(tk.toString());

    var a = await restservice
        .post("Login", {"username": phone, "password": password,"token":tk!});

    print("yessssssss");
    print(a);

    User user = User(
        image: "",
        created: DateTime.now(),
        userename: "",
        email: "",
        token: "");

    if (a["status"].toString() == "400") {
      return SigninResponse(
          message: "Faux email / password", code: 0, user: user);
    } else if (a["status"] == 190) {
      return SigninResponse(message: "Error", code: 0, user: user);
    } else {
      print(a["results"]);
      user = User.fromMap(a["results"],a["results"]["id"]);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("id", user.id);
      prefs.setString("token", user.token);
      return SigninResponse(message: "Success", code: 1, user: user);
    }
  }

  /*Future<RegisterResponse>*/
  save_user_informations(User user /*, String  id*/) async {
    var a = await restservice.post_register(
        "Signup/",
        json.encode({
          "username": "${user.userename}",
          "password": "${user.password}",
          "phone": "${user.mobile}",
          "email": "${user.email}"
        }));

    print("---------------------");
    print(a);
    if (a["status"] == 400) {
      return RegisterResponse(message: "Error", code: 0, user: user);
    } else if (a["status"] == 190) {
      return RegisterResponse(message: "Error", code: 0, user: user);
    } else {
      user.id = a["results"]["id"];
      user.token = a["results"]["token"];

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("id", user.id);
      prefs.setString("token", user.token);
      return RegisterResponse(message: "Success", code: 1, user: user);
    }
  }
}

class RegisterResponse {
  String message;
  User user;
  int code;

  RegisterResponse(
      {required this.user, required this.message, required this.code});
}

class SigninResponse {
  String message;
  User user;
  int code;

  SigninResponse(
      {required this.user, required this.message, required this.code});
}
