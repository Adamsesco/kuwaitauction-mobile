import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:kauction/model/models/signup_response.dart';
import 'package:kauction/model/models/user.dart';
import 'package:dio/dio.dart';

const _kApiBaseUrl = 'https://www.vairon.app/api';

class UserRepository {
  Future<SignupResponse> signup({
    required String username,
    required String password,
    required String email,
    required String device_token,
  }) async {
    String platformName;
    if (Platform.isAndroid) {
      platformName = 'Android';
    } else if (Platform.isIOS) {
      platformName = 'iOS';
    } else if (Platform.isLinux) {
      platformName = 'Linux';
    } else if (Platform.isMacOS) {
      platformName = 'MacOS';
    } else if (Platform.isWindows) {
      platformName = 'Windows';
    } else {
      platformName = 'Unknown';
    }

    print('-----------------------------------------');
    print({
      'username': username,
      'password': password,
      'email': email,
//      'mobile': phoneNumber,
    });
    final formData = FormData.fromMap(<String, dynamic>{
      'username': username,
      'password': password,
      'device_token': device_token,
      'email': email,
/*     'mobile': phoneNumber,
      if (auth_type != null && auth_type != "")
        'auth_type': auth_type,
      if (firstName != null)
        'first_name': firstName,
      if (lastName != null)
        'last_name': lastName,
      if (avatarUrl != null)
        'avatar': avatarUrl,
      'lang': languageCode?.toUpperCase() ?? 'EN',*/
      'device_type': platformName,
    });

    final request =
        await Dio().post<String>('$_kApiBaseUrl/signup', data: formData);
    if (request == null || request.statusCode != 200 || request.data == null) {
      return SignupResponse(
        user: User(
          email: "",
          created: DateTime.now(),
          userename: "",
          image: "",
        ),
        responseCode: 0,
        message: "",
      );
    }

    final dynamic dataJson = jsonDecode(request.data.toString());

    print("------------------------------------------------------");
    print(dataJson);

    /*if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return SignupResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }*/

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return SignupResponse(
        user: User(
          email: "",
          created: DateTime.now(),
          userename: "",
          image: "",
        ),
        responseCode: 0,
        message: "",
      );
    }
    if (code != 1) {
      return SignupResponse(
        user:
            User(email: "", created: DateTime.now(), userename: "", image: ""),
        responseCode: code,
        message: message,
      );
    }

    final dynamic userData = dataJson['data'];
    if (userData == null || !(userData is Map<String, dynamic>)) {
      return SignupResponse(
        user:
            User(email: "", created: DateTime.now(), userename: "", image: ""),
        responseCode: 0,
        message: "",
      );
    }

    final user = User.fromMap(userData as Map<String, dynamic>, userData['id']);
    return SignupResponse(
      user: user,
      responseCode: code,
      message: message,
    );
  }
/*
  Future<LoginResponse> login(
      {@required String username, @required String password}) async {
    String tk = await _firebaseMessaging.getToken();

    final formData = FormData.fromMap(<String, dynamic>{
      'username': username,
      'password': password,
      'device_token': tk
    });

    print({
      'username': username,
      'password': password,
    });

    print("-----------------------------------------------------------------");
    print('$_kApiBaseUrl/login');
    final request =
    await Dio().post<String>('$_kApiBaseUrl/login', data: formData);

    print("jihad");
    print(request.data);
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final dynamic dataJson = jsonDecode(request.data);
    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }
    if (code != 1) {
      return LoginResponse(
        user: null,
        responseCode: code,
        message: message,
      );
    }

    print("udududduddududududuududududududu");
    print(dataJson['data']);
    final dynamic userData = dataJson['data'];

    if (userData == null || !(userData is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: null,
      );
    }

    final user = User.fromMap(userData as Map<String, dynamic>);
    return LoginResponse(
      user: user,
      responseCode: code,
      message: message,
    );
  }*/

}
