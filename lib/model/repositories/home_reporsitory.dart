import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:kauction/model/models/category.dart';
import 'package:kauction/model/models/homereponse.dart';
import 'package:kauction/model/models/product.dart';

class HomeRepository {
  Future<HomeResponse> getInfoHome() async {
    final request = await Dio()
        .get<String>('http://134.122.118.107/kuwaitauction/api/HomeScreen');

    if (request == null || request.statusCode != 200 || request.data == null) {
      return HomeResponse(responseCode: 0, message: "");
    }

    final dynamic dataJson = jsonDecode(request.data.toString());

    if (dataJson == null) {
      return HomeResponse(responseCode: 0, message: "");
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;

    final items_List = dataJson["result"];
    final posts = List<HomeModel>.from(items_List.map((p) {
      final pst = p as Map<String, dynamic>;
      final post = HomeModel.fromMap(pst);
      return post;
    }).toList());

    print(posts);
    return HomeResponse(responseCode: code, message: message, home: posts);
  }
}
