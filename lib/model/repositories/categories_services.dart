import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/model/models/category.dart';

class CategoriesService {
  RestService rest = new RestService();

  get_all_categories() async {
    var pr_resp = await rest.get('Categories');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    List sp = pr_resp["data"]["results"];
    print(sp);
    return sp
        .map((var contactRaw) => new Category.fromMap(contactRaw))
        .toList();
  }

  get_sub_categories(cat_id) async {
    var pr_resp = await rest.get('subCategories/' + cat_id.toString());
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    if (pr_resp.isEmpty)
      return [];
    else {
      List sp = pr_resp["data"]["results"];
      return sp
          .map((var contactRaw) => new Category.fromMap(contactRaw))
          .toList();
    }
  }

  get_brand_categories(cat_id) async {
    var pr_resp = await rest.get('Brands/' + cat_id.toString());
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    if (pr_resp.isEmpty)
      return [];
    else {
      List sp = pr_resp["data"]["results"];
      return sp
          .map((var contactRaw) => new Category.fromMap(contactRaw))
          .toList();
    }
  }



  get_modules_categories(cat_id) async {
    var pr_resp = await rest.get('Modules?catId=' + cat_id.toString());
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    if (pr_resp.isEmpty)
      return [];
    else {
      List sp = pr_resp["data"]["results"];
      return sp
          .map((var contactRaw) => new Category.fromMap(contactRaw))
          .toList();
    }
  }
}
