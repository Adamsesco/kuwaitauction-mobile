import 'package:kauction/app/utils/rest_service.dart';

class VerifyNumberRepository {
  RestService rest = new RestService();

  verify_phone(String phone) async {///"+965" +
    var res = await rest.post("checkPhone/", {"phone": phone});

    return res;
  }

  verify_code(String phone, String code) async {
    print(phone);
    print(code);

    var res =
        await rest.post("checkOtp/", {"phone":  phone, "otp": code});

    return res;
  }
}
