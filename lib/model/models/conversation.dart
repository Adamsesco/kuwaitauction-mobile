class Conversation {
  late String id;
  late String latmessage;
  late String image;
  late DateTime date;
  late bool seen = false;

  Conversation(
      {required this.latmessage,
      required this.id,
      this.seen = false,
      required this.date,
      required this.image});

  Conversation.fromJson(map, String key)
      : latmessage = map["content"],
        image = map["image"],
        seen = map["seen"],
        id = key,
        date = DateTime.fromMillisecondsSinceEpoch(map["timestamp"]);
}
