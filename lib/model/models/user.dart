import 'package:kauction/model/models/product.dart';

class User {
  String id;
  String image;
  String token;
  String token_notification;

  String email;
  String mobile;
  String userename;
  DateTime created;
  var password;
  var type;
  bool isActive = false;
  List<Product>? items;

  User(
      {this.id = "",
      required this.image,
      this.password = "",
      this.token = "",
        this.token_notification="",
      required this.created,
      this.items,
      this.mobile = "",
      this.isActive = false,
      required this.userename,
      required this.email});

  User.fromMap(Map<String, dynamic> map, id_user)
      : id = id_user,
        // password = map["password"],
        created = DateTime.now() /*: DateTime.parse(map["date"])*/,
        image = map["image"],
        items = List<Product>.from(map.containsKey("items") == true
            ? map["items"].map((e) => Product.fromMap2(e)).toList()
            : []),
        userename = map["username"],
        email = map["email"],
        token = map["token"],
        token_notification = map["token_notification"],

      mobile = map["phone"];
}
