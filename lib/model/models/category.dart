import 'package:meta/meta.dart';

class Category {
  final String id;
  final String image;
  final String name;
  bool check;
  var items;
  String priceless;

  Category(
      {required this.id,
      required this.name,
      required this.image,
      this.priceless = "0",
      this.items,
      required this.check});

  factory Category.fromMap(Map<String, dynamic> map) {
    return Category(
      check: false,
      id: map['id'] as String,
      items: map["items"].toString(),
      priceless: map["priceless"].toString(),
      image: map['image'] as String,
      name: map['name'] as String,
    );
  }
}

class CategoryModel {
  final String category_id;
  final String image;
  final String category_name;
  bool check;
  var items;
  final String category_name_ar;

  CategoryModel(
      {required this.category_id,
      required this.image,
      required this.category_name,
      required this.category_name_ar,
      this.items,
      required this.check});

  factory CategoryModel.fromMap(Map<String, dynamic> map) {
    return CategoryModel(
      check: false,
      category_id: map['category_id'] as String,
      items: map["items"].toString(),
      image: map['image'] as String,
      category_name_ar: map['category_name_ar'] as String,
      category_name: map['category_name'] as String,
    );
  }
}
