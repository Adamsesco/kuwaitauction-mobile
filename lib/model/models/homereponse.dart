import 'package:kauction/model/models/category.dart';
import 'package:kauction/model/models/product.dart';
import 'package:meta/meta.dart';

class HomeResponse {
  final int responseCode;
  final String message;

  List<HomeModel>? home;

  HomeResponse({required this.responseCode, required this.message, this.home});
}

class HomeModel {
  final String category_name;
  final List<Product> products;
  final String category_id;
  String category_name_ar;

  HomeModel(
      {required this.category_name,
      required this.products,
        required this.category_name_ar,
      required this.category_id});

  factory HomeModel.fromMap(Map<String, dynamic> map) {

    return HomeModel(
      category_name_ar: map["category_name_ar"],

      category_name: map["category_name"],
      category_id: map["category_id"],
      products: map.containsKey("items")
          ? List<Product>.from(
              map["items"].map((pack) => Product.fromMap(pack)).toList())
          : [],
    );
  }
}
