class Message {
  //String id;
  String lastmessage = "";
  DateTime? timestamp;
  String nameUser = "";
  String? idUser;

  String photoURL = "";
  String key = "";
  String idOther = "";
  String imageURL = "";
  String name = "";
  String avatar = "";
  String text = "";

  Message(
      {this.lastmessage = "",
      this.timestamp,
      this.idOther = "",
      this.idUser = "",
      this.name = "",
      this.text = "",
      this.avatar = "",
      this.key = "",
      this.imageURL = "",
      this.nameUser = "",
      this.photoURL = ""});

  Message.fromMap(Map<String, dynamic> map)
      : lastmessage = "${map['lastmessage']}",
        name = "${map['senderName']}",
        avatar = "${map['senderPhotoUrl']}",
        key = "${map['key']}",
        text = "${map['text']}",
        timestamp = DateTime.parse("${map['timestamp']}"),
        imageURL = "${map['imageUrl']}";
}

