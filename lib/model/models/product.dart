import 'package:meta/meta.dart';

class Product {
  final String id;
  final List<Photo> images;
  final String description;
  final String name;
  String user_id;
  var date;
  String phone;
  bool enable_extend;
  bool enable_close;
  List<Bid> bids = [];
  final String totalitems;
  String token_notification;
  String priceless;

  DateTime? close_date;
  var category_id;
  String category_name_en;
  String category_name_ar;
  String image = "";

  String type;
  String avatar;
  String username;
  String subcategory_name;
  String brand_name;
  String module_name;
  String last_price;
  String time;
  String category;
  String l_desc;

  Product(
      {required this.id,
      required this.name,
      required this.type,
      required this.category,
      required this.username,
      required this.bids,
      this.totalitems = "",
      required this.images,
      required this.subcategory_name,
      required this.category_id,
      required this.close_date,
      required this.date,
      required this.category_name_en,
      required this.category_name_ar,
      required this.l_desc,
      this.priceless = "0",
      required this.description,
      required this.user_id,
      required this.avatar,
      this.token_notification = "",
      this.image = "",
      required this.phone,
      this.enable_extend = false,
      this.enable_close = false,
      required this.last_price,
      required this.time,
      this.brand_name = "",
      this.module_name = ""});

  factory Product.fromMap2(Map<String, dynamic> map) {
    return Product(
      id: map['id'] as String,
      token_notification: map['token_notification'] as String,
      phone: '',
      image: "",
      totalitems: "",
      brand_name: "",
      module_name: "",
      category_id: "",
      category_name_ar: "",
      category_name_en: "",
      bids: [],
      images: map.containsKey('images') == false
          ? [Photo(id: "", url: "")]
          : map['images'] == null
              ? [Photo(url: "", id: "")]
              : List<Photo>.from(
                  map["images"].map((e) => Photo.fromMap(e)).toList()),
      l_desc: "",
      name: map['title'] as String,
      subcategory_name: map['subcategory_name'] == null
          ? ""
          : map['subcategory_name'] as String,
      user_id: "",
      avatar: "",
      type: "",
      priceless: map['priceless'].toString() as String,
      enable_extend: map['enable_extend'] as bool,
      enable_close: map['enable_close'] as bool,
      time: map['date'] as String,
      description: "",
      last_price: map['starting_price'].toString() == "null"
          ? map['price'].toString()
          : map['starting_price'].toString() as String,
      category: "",
      username: "",
      date: map['date'] == null ? null : DateTime.parse(map['date']),
      close_date: DateTime.now(),
    );
  }

  factory Product.fromMap(Map<String, dynamic> map) {
    print("dhdhdhdhdhhd");
    print(map["bids"].runtimeType);

    return Product(
      id: map['id'] as String,
      priceless: map['priceless'].toString() as String,
      phone: map['phone'] as String,
      token_notification: map['token_notification'] as String,
      image: map["image"],
      totalitems: map['totalitems'].toString() == "null"
          ? "0"
          : map['totalitems'].toString() as String,
      brand_name: map['brand_name'] == null ? "" : map['brand_name'] as String,
      module_name:
          map['module_name'] == null ? "" : map['module_name'] as String,
      category_id: map['category_id'],
      category_name_ar: map['category_name_ar'] == null
          ? ""
          : map['category_name_ar'] as String,
      enable_extend: map['enable_extend'] as bool,
      enable_close: map['enable_close'] as bool,
      category_name_en:
          map['category_name'] == null ? "" : map['category_name'] as String,
      bids: map.containsKey('bids') == false
          ? []
          : map['bids'] == null
              ? []
              : map["bids"].runtimeType == int
                  ? []
                  : List<Bid>.from(
                      map["bids"].map((e) => Bid.fromMap(e)).toList()),

      /*image: map.containsKey('images') == true
          ? map['image'] == null
              ? [""]
              : List<Photo>.from(map['image'])
          :  map['images'] == null
              ? [Photo(id: "", url: "")]
              : List<String>.from(map['images']),*/
      images: map.containsKey('images') == false
          ? [Photo(id: "", url: "")]
          : map['images'] == null
              ? [Photo(url: "", id: "")]
              : List<Photo>.from(
                  map["images"].map((e) => Photo.fromMap(e)).toList()),
      l_desc: map['description'] as String,
      name: map['title'] as String,
      subcategory_name: map['subcategory_name'] == null
          ? ""
          : map['subcategory_name'] as String,
      user_id: map['user_id'] as String,
      avatar: map['avatar'] as String,
      type: map['type'] as String,
      time: map['date'] as String,
      description: map['description'] as String,
      last_price: map['starting_price'].toString() == "null"
          ? map['price'].toString()
          : map['starting_price'].toString() as String,
      category: map['category'] as String,
      username: map['username'] as String,
      date: map['date'] == null ? null : DateTime.parse(map['date']),
      close_date:
          map['close_date'] == null ? null : DateTime.parse(map['close_date']),
    );
  }
}

class Photo {
  String id;
  String url;

  Photo({required this.id, required this.url});

  factory Photo.fromMap(Map<String, dynamic> map) {
    return Photo(id: map['id'].toString() as String, url: map['url'] as String);
  }
}

class Bid {
  String user_id;
  String price;
  String username;
  String avatar;
  DateTime date;

  Bid(
      {required this.user_id,
      required this.price,
      required this.avatar,
      required this.date,
      required this.username});

  factory Bid.fromMap(Map<String, dynamic> map) {
    return Bid(
        user_id: map['user_id'].toString() as String,
        date: DateTime.parse(map["date"]),
        avatar: map['avatar'].toString() as String,
        username: map['username'].toString() as String,
        price: map['price'] as String);
  }
}

/**
    {

    "username":"Mishal",
    "date":"2022-02-01 20:07:25",
    "avatar":"http:\/\/134.122.118.107\/kuwaitauction\/images\/default_avatar.png"
    },
 */
