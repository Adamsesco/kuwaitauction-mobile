import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/views/custom_widgets/primary_button.dart';
import 'package:kauction/views/home/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguagesPage extends StatefulWidget {
  const LanguagesPage({Key? key}) : super(key: key);

  @override
  _LanguagesPageState createState() => _LanguagesPageState();
}

class _LanguagesPageState extends State<LanguagesPage> {
  bool choose_eng = true;
  bool choose_ar = false;

  eng() async {
    context.locale = Locale('en');
    setState(() {
      choose_eng = true;
      choose_ar = false;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("lng", "en");
  }

  arab() async {
    context.locale = Locale('ar');
    setState(() {
      choose_ar = true;
      choose_eng = false;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("lng", "ar");
  }

  getlanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("lng", "ar");
    var lng = prefs.getString("lng");
    if (!this.mounted) return;
    if (lng == "ar") {
      context.locale = Locale('ar');

      setState(() {
        choose_ar = true;
        choose_eng = false;
      });
    }
    else {
      context.locale = Locale('en');

      setState(() {
        choose_eng = true;
        choose_ar = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    getlanguage();
  }

  @override
  Widget build(BuildContext context) {
    Widget widget_item(
            String text, Color color, String icon, onTap, color_text) =>
        InkWell(
            onTap: () {
              onTap();
            },
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        // color: color,
                        borderRadius: BorderRadius.circular(12.5.r)),
                    padding: EdgeInsets.only(
                        top: 12.h, bottom: 12.h, left: 12.w, right: 12.w),
                    child: Row(
                      children: [
                        Container(
                          width: 16.w,
                        ),
                        SvgPicture.asset(
                          icon,
                          height: ScreenUtil().setWidth(40),
                          width: ScreenUtil().setWidth(40),
                          fit: BoxFit.cover,
                        ),
                        Container(
                          width: 8.w,
                        ),
                        Text(
                          text,
                          style: TextStyle(
                              height: 1.2,
                              color: color_text,
                              fontWeight: FontWeight.w900,
                              fontSize: 15.5.sp),
                        ),
                        Expanded(child: Container()),
                        Icon(
                          Icons.check,
                          color: color,
                        ),
                        Container(
                          width: 24.w,
                        ),
                      ],
                    ),
                  )
                ]));

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 88.h,
          ),
          Hero(
              tag: "DemoTag",
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    "assets/images/logo.svg",
                    width: 40.w,
                  ),
                  Container(
                    width: 8.w,
                  ),
                  Text(
                    "Kauction",
                    style: TextStyle(
                        fontFamily: "poppins_medium",
                        color: ColorsConst.col_app,
                        fontSize: 24.5.sp,
                        fontWeight: FontWeight.w900),
                  )
                ],
              )),
          Expanded(child: Container(height: 45.h)),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.w),
              child: Text(
                "chse".tr(),
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: ColorsConst.col_black, fontWeight: FontWeight.w800),
              )),
          Container(
            height: 12.h,
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 24.w),
              child: Text(
                "welcome".tr(),
                textAlign: TextAlign.center,
                style: TextStyle(color: ColorsConst.col_black, fontSize: 12.sp),
              )),
          widget_item(
              "English",
              choose_eng == true
                  ? Color(0xff06035e)
                  : ColorsConst.col_app_white,
              "assets/icons/uk.svg",
              eng,
              choose_eng == true
                  ? Color(0xff06035e)
                  : ColorsConst.col_text_grey),
          Container(
            height: 18.h,
          ),
          widget_item(
              "Arabic",
              choose_ar == true ? Color(0xff06035e) : ColorsConst.col_app_white,
              "assets/icons/kuwait.svg",
              arab,
              choose_ar == true
                  ? Color(0xff06035e)
                  : ColorsConst.col_text_grey),
          Container(
            height: 12.h,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
                width: 300.w,
                child: PrimaryButton(
                  textStyle: TextStyle(),
                  disabledColor: ColorsConst.col_app,
                  fonsize: 16.5.sp,
                  icon: "",
                  isLoading: false,
                  colorText: ColorsConst.col_app_white,
                  prefix: Container(),

                  text: "continue".tr().toUpperCase(),
                  //color_text: ColorsApp.col_app,
                  color: ColorsConst.col_app,
                  onTap: () {
                    Navigator.push(context,
                        new MaterialPageRoute(builder: (BuildContext context) {
                      return HomeScreen();
                    }));
                  },
                ))
          ]),
          Container(height: 45.h)
        ],
      ),
    );
  }
}
