import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/messenger/conversation.dart';
import 'package:kauction/messenger/online_widget.dart';
import 'package:kauction/messenger/util/settings.dart';
import 'package:kauction/model/models/user.dart';

import 'package:rflutter_alert/rflutter_alert.dart';

class ChatItem extends StatefulWidget {
  DataSnapshot snapshot;
  String id;
  User user_me;

  var reload;

  ChatItem(this.snapshot, this.id, this.reload, this.user_me, {Key? key, snap})
      : super(key: key);

  @override
  _ChatItemState createState() => _ChatItemState();
}

class _ChatItemState extends State<ChatItem> {
  // UserServices userp = new UserServices();
  late bool load, load_p = false;
  User user =
      new User(image: "", created: DateTime.now(), userename: "", email: "");
  late SlidableController slidableController;
  bool vu = true;
  late Settings_Services userp = new Settings_Services();

  getUserinfo(id) async {
    setState(() {
      load = true;
    });
    var a = await userp.user_informations(id);

    if (!this.mounted) return;

    setState(() {
      user = a;
      load = false;
    });
  }

  late Animation<double> _rotationAnimation;
  Color _fabColor = Colors.blue;

  checkviewMessage(key, his_id) async {
    String idp = widget.snapshot.key!.split("_")[2];

    FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(key + "_" + idp + "/vu_" + widget.id)
        .onValue
        .listen((val) {
      var d = val.snapshot.value;

      if (d != null) {
        //Message lu
        if (d == "0") {
          try {
            setState(() {
              vu = true;
            });
          } catch (e) {}
        }
        //Message non lu
        else {
          try {
            setState(() {
              vu = false;
            });
          } catch (e) {}
        }
      }
    });
  }

  TextStyle text11 = TextStyle(
      color: ColorsConst.col_grey,
      fontSize: ScreenUtil().setSp(10),
      fontWeight: FontWeight.w500);

  void handleSlideAnimationChanged(Animation<double> slideAnimation) {
    setState(() {
      _rotationAnimation = slideAnimation;
    });
  }

  void handleSlideIsOpenChanged(bool isOpen) {
    setState(() {
      _fabColor = isOpen ? Colors.green : Colors.blue;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    slidableController = SlidableController(
      onSlideAnimationChanged: handleSlideAnimationChanged,
      onSlideIsOpenChanged: handleSlideIsOpenChanged,
    );
    super.initState();

    List sp =
        new Map<String, dynamic>.from(widget.snapshot.value as dynamic)['key']
            .split("_");

    String idOther = sp[1];

    getUserinfo(idOther).then((na) {
      /* print(na);
          print("33333333");
          print(na.name);
          print(na.token);
          try {
            setState(() {
              user.name = na.name;
              user.image = na.image;
              user.id = idOther;
              user.token= na.token;
              print("123456");
              print(user.name);
              print(user.token);
            });
          } catch (e) {}
*/
      checkviewMessage(widget.id + "_" + user.id.toString(), user.id);
    });
  }

  deleteMessage() async {
    try {} catch (e) {}
    FirebaseDatabase.instance
        .reference()
        .child("message")
        .child(widget.snapshot.key!.split("_")[2])
        .remove()
        .then((_) {
      FirebaseDatabase.instance
          .reference()
          .child("room")
          .child(widget.snapshot.key!)
          .remove();
    });

    Navigator.of(context, rootNavigator: true).pop(true);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle text1 = TextStyle(
        color: vu ? ColorsConst.col_grey : ColorsConst.col_app,
        fontSize: ScreenUtil().setSp(vu ? 13 : 14.5),
        fontWeight: vu ? FontWeight.w800 : FontWeight.w900);
    TextStyle text2 = TextStyle(
        color: ColorsConst.col_grey,
        fontSize: ScreenUtil().setSp(13),
        fontWeight: FontWeight.w500);

    Widget item = user.mobile.toString() == ""
        ? Container()
        : InkWell(
            onTap: () {
              String idpost = widget.snapshot.key!.split("_")[2];

              print("hdhdhdhdhdhdhdhdhdhdhdhdhdhdhdhdhdh");
              print(widget.user_me.id);
              print(user.id);
              print("hdhdhdhdhdhdhdhdhdhdhdhdhdhdhdhdhdh");

              Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(
                  builder: (BuildContext context) {
                    return Conversation(widget.user_me, user, idpost);
                  },
                ),
              );
            },
            child: Container(
              margin: EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
              height: ScreenUtil().setHeight(91),
              decoration: new BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    //                    <--- top side
                    color: ColorsConst.col_grey.withOpacity(0.2),
                    width: 1.0,
                  ),
                ),
                //   color: vu ? Colors.white : Config.col_blue.withOpacity(0.5),
              ),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width * 0.01,
                    ),
                    SizedBox(
                        height: ScreenUtil().setWidth(62),
                        width: ScreenUtil().setWidth(62),
                        child: Stack(children: <Widget>[
                          CircleAvatar(
                            radius: ScreenUtil().setWidth(58.5),
                            child: ClipOval(
                              child: Image.network(
                                user.image == null ? "" : user.image,
                                height: ScreenUtil().setWidth(87),
                                width: ScreenUtil().setWidth(87),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Positioned(
                              top: -1,
                              left: 0,
                              child: OnlineWidget(user, false)),
                        ])),
                    Container(
                      width: ScreenUtil().setWidth(12),
                    ),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              width: ScreenUtil().setWidth(270),
                              child: Row(
                                children: [
                                  Text(
                                    user.userename.toString(),
                                    style: text1,
                                  ),
                                  Expanded(
                                    child: Container(),
                                  ),
                                  Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Text(
                                          new DateFormat('yyyy-MM-dd HH:mm')
                                              .format(new DateTime
                                                      .fromMillisecondsSinceEpoch(
                                                  new Map<String, dynamic>.from(
                                                          widget.snapshot.value
                                                              as dynamic)[
                                                      'timestamp'])),
                                          style: text2,
                                        ),
                                        vu == false
                                            ? Container(
                                                margin: EdgeInsets.only(
                                                    top: 8.h, right: 18.w),
                                                child: CircleAvatar(
                                                  radius: ScreenUtil()
                                                      .setWidth(3.5),
                                                  backgroundColor:
                                                      ColorsConst.col_app,
                                                ))
                                            : Container(),
                                      ])
                                ],
                              )),
                          Container(
                            height: ScreenUtil().setHeight(12),
                          ),
                          Container(
                              width: ScreenUtil().setWidth(260),
                              child: Text(
                                new Map<String, dynamic>.from(widget
                                    .snapshot.value as dynamic)["lastmessage"],
                                style: text11,
                                maxLines: 2,
                              ))
                        ])
                  ]),
            ));

    Widget _getActionPane(int index) {
      switch (index % 4) {
        case 0:
          return SlidableBehindActionPane();
        case 1:
          return SlidableStrechActionPane();
        case 2:
          return SlidableScrollActionPane();
        case 3:
          return SlidableDrawerActionPane();
        default:
          return SlidableBehindActionPane();
      }
    }

    return Slidable(
      key: Key(""),
      controller: slidableController,
      direction: Axis.horizontal,
      /* dismissal: SlidableDismissal(
        child: SlidableDrawerDismissal(),
        onDismissed: (actionType) {},
      ),*/
      actionPane: _getActionPane(0),
      actionExtentRatio: 0.25,
      child: item,
      actions: <Widget>[],
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: '',
          color: const Color(0xffFF0000),
          iconWidget: Center(
              child: SvgPicture.asset(
            "assets/icons/remove.svg",
            color: Colors.white,
          )),
          onTap: () {
            // deleteMessage();

            Alert(
              context: context,
              type: AlertType.error,
              title: "delm",
              desc: "delc",
              buttons: [
                DialogButton(
                  child: Text(
                    "Confirm",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                  onPressed: () {
                    deleteMessage();
                  },
                  width: 120,
                )
              ],
            ).show();
          },
        ),
      ],
    );
  }
}
