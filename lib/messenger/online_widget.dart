import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/model/models/user.dart';

class OnlineWidget extends StatefulWidget {
  OnlineWidget(this.user, this.show_text);

  User user;
  bool show_text = false;

  @override
  _OnlineWidgetState createState() => _OnlineWidgetState();
}

class _OnlineWidgetState extends State<OnlineWidget> {
  check_online() {
    FirebaseDatabase.instance
        .reference()
        .child("status")
        .child(widget.user.id)
        .onValue
        .listen((val) {
      var d;
      try {
        setState(() {
          d = val.snapshot.value;
          if ((val.snapshot.value as Map<String,dynamic>)["online"] == true) {
            setState(() {
              widget.user.isActive = true;
            });
          } else {
            setState(() {
              widget.user.isActive = false;
            });
          }
        });
      } catch (e) {}
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    check_online();
  }

  @override
  Widget build(BuildContext context) {

    getidget() {
      switch (widget.show_text) {
        case true:
          return Text(
            widget.user.isActive == true ? "Online now" : "Offline",
            style: TextStyle(
                color: widget.user.isActive == true
                    ? ColorsConst.col_app
                    : Color(0xFF4E596F).withOpacity(0.5),
                fontSize: ScreenUtil().setSp(11).toDouble(),
                fontWeight: FontWeight.bold),
          );
          break;
        case false:
          return widget.user.isActive == true
              ? Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.white),
                  padding: EdgeInsets.all(2.5),
                  child: CircleAvatar(
                    radius: 8,
                    backgroundColor: ColorsConst.col_app,
                  ),
                )
              : Container(
                  width: 0.0,
                  height: 0.0,
                );
          break;
        default:
          break;
      }
    }

    return getidget()!;
  }
}
