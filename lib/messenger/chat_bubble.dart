import 'dart:async';
import 'dart:math';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/views/custom_widgets/audio_playback.dart';

class ChatBubble extends StatefulWidget {
  final String id_user,
      message,
      time,
      username,
      type,
      replyText,
      replyName,
      file,
      recorderTime;
  List imageUrl;
  final bool isMe, isGroup, isReply;
  var func_reply;

  ChatBubble(this.func_reply,
      {required this.id_user,
      required this.message,
      required this.imageUrl,
      required this.time,
      required this.isMe,
      required this.isGroup,
      required this.username,
      required this.recorderTime,
      required this.file,
      required this.type,
      required this.replyText,
      required this.isReply,
      required this.replyName});

  @override
  _ChatBubbleState createState() => _ChatBubbleState();
}

class _ChatBubbleState extends State<ChatBubble> {
  List colors = Colors.primaries;
  static Random random = Random();
  int rNum = random.nextInt(18);
  late SlidableController slidableController;

  late Animation<double> _rotationAnimation;
  Color _fabColor = Colors.blue;

  void handleSlideAnimationChanged(Animation<double> slideAnimation) {
    setState(() {
      _rotationAnimation = slideAnimation;
    });
  }

  void handleSlideIsOpenChanged(bool isOpen) {
    setState(() {
      _fabColor = isOpen ? Colors.green : Colors.blue;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    slidableController = SlidableController(
      onSlideAnimationChanged: handleSlideAnimationChanged,
      onSlideIsOpenChanged: handleSlideIsOpenChanged,
    );
  }

  static Widget _getActionPane(int index) {
    switch (index % 4) {
      case 0:
        return SlidableBehindActionPane();
      case 1:
        return SlidableStrechActionPane();
      case 2:
        return SlidableScrollActionPane();
      case 3:
        return SlidableDrawerActionPane();
      default:
        return SlidableBehindActionPane();
    }
  }

  @override
  Widget build(BuildContext context) {
    final bg = widget.isMe ? ColorsConst.col_app : Color(0xffF5F5F5);
    final align =
        widget.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start;
    final radius = widget.isMe
        ? BorderRadius.only(
            topLeft: Radius.circular(5.0),
            bottomLeft: Radius.circular(5.0),
            bottomRight: Radius.circular(10.0),
          )
        : BorderRadius.only(
            topRight: Radius.circular(5.0),
            bottomLeft: Radius.circular(10.0),
            bottomRight: Radius.circular(5.0),
          );

    Widget wid =
        Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.all(3.0),
            padding: const EdgeInsets.all(5.0),
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width / 1.1,
              minWidth: 20.0,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                widget.isMe
                    ? SizedBox()
                    : widget.isGroup
                        ? Padding(
                            padding: EdgeInsets.only(right: 48.0),
                            child: Container(
                              child: Text(
                                widget.username,
                                style: TextStyle(
                                  fontSize: 13,
                                  color: colors[rNum],
                                  fontWeight: FontWeight.bold,
                                ),
                                textAlign: TextAlign.left,
                              ),
                              alignment: Alignment.centerLeft,
                            ),
                          )
                        : SizedBox(),
                widget.isGroup
                    ? widget.isMe
                        ? SizedBox()
                        : SizedBox(height: 5)
                    : SizedBox(),
                widget.isReply
                    ? Container(
                        decoration: BoxDecoration(
                          color:
                              !widget.isMe ? Colors.grey[50] : Colors.blue[50],
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        ),
                        constraints: BoxConstraints(
                          minHeight: 25,
                          maxHeight: 100,
                          minWidth: 80,
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(5),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                child: Text(
                                  widget.isMe ? "You" : widget.replyName,
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 12,
                                  ),
                                  maxLines: 1,
                                  textAlign: TextAlign.left,
                                ),
                                alignment: Alignment.centerLeft,
                              ),
                              SizedBox(height: 2),
                              Container(
                                child: Text(
                                  widget.replyText,
                                  style: TextStyle(
                                    color: ColorsConst.col_grey,
                                    fontSize: 10,
                                  ),
                                  maxLines: 2,
                                ),
                                alignment: Alignment.centerLeft,
                              ),
                            ],
                          ),
                        ),
                      )
                    : SizedBox(width: 2),
                widget.isReply ? SizedBox(height: 5) : SizedBox(),
                widget.type == "image"
                    ? Container(
                        decoration: BoxDecoration(
                          color: bg,
                          borderRadius: radius,
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            child: Image.network(
                              widget.imageUrl[0] as String,
                              fit: BoxFit.fitWidth,
                            )))
                    : Container(
                        decoration: BoxDecoration(
                          color: bg,
                          borderRadius: radius,
                        ),
                        padding: EdgeInsets.all(12),
                        // margin: EdgeInsets.all(12),
                        child: !widget.isReply
                            ? Text(
                                widget.message,
                                style: TextStyle(
                                  color:
                                      widget.isMe ? Colors.white : Colors.black,
                                ),
                              )
                            : Container(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  widget.message,
                                  style: TextStyle(
                                    color: widget.isMe
                                        ? Colors.white
                                        : Colors.black,
                                  ),
                                ),
                              )),
              ],
            ),
          ),
          Padding(
            padding: widget.isMe
                ? EdgeInsets.only(
                    right: 10,
                    bottom: 10.0,
                  )
                : EdgeInsets.only(
                    left: 10,
                    bottom: 10.0,
                  ),
            child: Text(
              widget.time,
              style: TextStyle(
                color: Colors.black,
                fontSize: 10.0,
              ),
            ),
          ),
        ],
      ),
      IconButton(
        onPressed: () {
          widget.func_reply(widget.id_user, widget.message);
        },
        icon: SvgPicture.asset(
          "assets/icons/reply.svg",
          width: MediaQuery.of(context).size.width * 0.06,
        ),
      ),
    ]);

    void _showSnackBar(BuildContext context, String text) {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(text)));
    }

    return widget.type == "audio"
        ? Row(children: [
            Container(
                width: 340.w,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      AudioPlayback(
                          downloadwidget: Container(), url: widget.file),
                      Align(
                        alignment: AlignmentDirectional.bottomEnd,
                        child: Padding(
                            padding: EdgeInsets.only(
                                top: 10,
                                bottom: 8,
                                right:
                                    MediaQuery.of(context).size.width * 0.12),
                            child: Text(
                              DateFormat('HH:mm')
                                  .format(DateTime.parse(widget.time)),
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(13).toDouble(),
                                fontWeight: FontWeight.w500,
                                color: Color(0xFFC2C4CA),
                              ),
                            )),
                      )
                    ]))
          ])
        : wid;
  }
}

/*

Slidable(
     key: Key(widget.message),
     controller: slidableController,
     direction: Axis.horizontal,
     dismissal: SlidableDismissal(
       child: SlidableDrawerDismissal(),
       onDismissed: (actionType) {
         _showSnackBar(
             context,
             actionType == SlideActionType.primary
                 ? 'Dismiss Archive'
                 : 'Dimiss Delete');
         setState(() {

         });
       },
     ),
     actionPane:  SlidableBehindActionPane(),
     actionExtentRatio: 0.25,
     child: wid,
     actions: <Widget>[
       IconSlideAction(
         caption: 'Archive',
         color: Colors.blue,
         icon: Icons.archive,
         onTap: () => _showSnackBar(context, 'Archive'),
       ),
       IconSlideAction(
         caption: 'Share',
         color: Colors.indigo,
         icon: Icons.share,
         onTap: () => _showSnackBar(context, 'Share'),
       ),
     ],
     secondaryActions: <Widget>[
       IconSlideAction(
         caption: 'More',
         color: Colors.grey.shade200,
         icon: Icons.more_horiz,
         onTap: () => _showSnackBar(context, 'More'),
         closeOnTap: false,
       ),
       IconSlideAction(
         caption: 'Delete',
         color: Colors.red,
         icon: Icons.delete,
         onTap: () => _showSnackBar(context, 'Delete'),
       ),
     ],
   )
 */
