import 'dart:async';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter/services.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/messenger/chat_bubble.dart';
import 'package:kauction/messenger/online_widget.dart';
import 'package:kauction/model/models/product.dart';

import 'dart:math';

import 'package:kauction/model/models/user.dart';
import 'package:kauction/model/repositories/product_repository.dart';
import 'package:easy_localization/easy_localization.dart';

class Conversation extends StatefulWidget {
  Conversation(this.user_m, this.user_him, this.id_post, {this.post});

  User user_m;
  User user_him;
  String id_post;
  Product? post;

  @override
  _ConversationState createState() => _ConversationState();
}

class _ConversationState extends State<Conversation> {
  static Random random = Random();
  bool typing = false;
  String id_typing = "";
  late StreamSubscription? _recorderSubscription;
  late StreamSubscription? _dbPeakSubscription;
  bool _isRecording = false;
  late String _path;

  //String name = names[random.nextInt(10)];
  RestService rest = new RestService();
  String _recorderTxt = '00:00:00';
  FocusNode _focus = new FocusNode();

  bool _isComposing = false;
  String _messageText = "";

  bool uploading = false;
  final TextEditingController _textController = new TextEditingController();
  Product? post;

  late Timer? searchOnStoppedTyping;

  bool show_textfield = true;
  DatabaseReference? gMessagesDbRef, gMessagesDbRef_inv;
  String idLast = "";
  bool vu = false;
  late DatabaseReference gMessagesDbRef2;
  late DatabaseReference gMessagesDbRef3;
  bool show = false;
  bool isReply = false;
  String idReply = "";
  String replyText = "";
  FlutterSound flutterSound = FlutterSound();

  void stopRecorder() async {
    try {
      String result = await flutterSound.stopRecorder();
      print('stopRecorder: $result');

      if (_recorderSubscription != null) {
        _recorderSubscription!.cancel();
        _recorderSubscription = null;
      }
      if (_dbPeakSubscription != null) {
        _dbPeakSubscription!.cancel();
        _dbPeakSubscription = null;
      }
    } catch (err) {
      print('stopRecorder error: $err');
    }

    this.setState(() {
      this._isRecording = false;
    });
  }

  _onRecordCancel() {
    stopRecorder();
  }

  Widget _buildMsgBtn({required Function onP}) {
    return Material(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0),
        child: IconButton(
          icon: Icon(Icons.send, color: ColorsConst.col_app),
          onPressed: () {
            onP();
          },
          color: Colors.black,
        ),
      ),
      color: Colors.white,
    );
  }

  String getKey(a, b) => a.toString() + "_" + b.toString();

  void _onRecorderPreesed() async {
    try {
      String result = await flutterSound.startRecorder(
          // codec: t_CODEC.CODEC_AAC,
          );

      print('startRecorder: $result');

      _recorderSubscription = flutterSound.onRecorderStateChanged.listen((e) {
        DateTime date =
            new DateTime.fromMillisecondsSinceEpoch(e.currentPosition.toInt());
        String txt = DateFormat('mm:ss', 'en_US').format(date);
        this.setState(() {
          this._isRecording = true;
          this._recorderTxt = txt.substring(0, 5);
          this._path = result;
        });
      });
    } catch (err) {
      print('startRecorder error: $err');
      setState(() {
        this._isRecording = false;
      });
    }
  }

  messageviewedbyme(a, b) async {
    gMessagesDbRef3 = FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(getKey(a, b) + "_" + widget.id_post);
    var snapshot = await FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(getKey(a, b) + "_" + widget.id_post)
        .once();
    /* if (snapshot/*.value*/ != null) {
      FirebaseDatabase.instance
          .reference()
          .child("lastm")
          .child(getKey(a, b) + "_" + widget.id_post)
          .update({"vu_" + a: "0"});
      try {
        setState(() {
          idLast = (snapshot.value as Map<String,dynamic>)["id_last"];
        });
      } catch (e) {}
    }*/
  }

  func_remply(idreply, textreply) {
    setState(() {
      isReply = true;
    });
    replyText = textreply;
    idReply = idreply;
  }

  _onSendRecord() async {
    stopRecorder();
    File recordFile = File(_path);
    bool isExist = await recordFile.exists();

    if (isExist) {
      String fileName = DateTime.now().millisecondsSinceEpoch.toString();
      Reference reference = FirebaseStorage.instance.ref().child(fileName);

      UploadTask uploadTask = reference.putFile(recordFile);
      TaskSnapshot storageTaskSnapshot = await uploadTask;

      storageTaskSnapshot.ref.getDownloadURL().then((recordUrl) {
        print('download record File: $recordUrl');
        _sendMessage(
            audio: recordUrl,
            text: "",
            recorderTime: _recorderTxt,
            type: "audio");

        print('Recorder text: $_recorderTxt');
      }, onError: (err) {});
    }
  }

  Widget _buildRecordingView() {
    return Container(
      padding: EdgeInsets.only(
        left: ScreenUtil().setWidth(26).toDouble(),
        right: ScreenUtil().setWidth(26).toDouble(),
      ),
      height: 80,
      width: double.infinity,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          IconButton(icon: Icon(Icons.close), onPressed: _onRecordCancel),
          Container(
            width: 16,
          ),
          Container(
            child: Text(
              this._recorderTxt,
              style: TextStyle(
                fontSize: 14.0,
                fontWeight: FontWeight.bold,
                color: ColorsConst.col_grey,
              ),
            ),
          ),
          Expanded(
            child: Container(),
          ),
          _buildMsgBtn(onP: _onSendRecord)
        ],
      ),
    );
  }

  checkviewMessage(key) async {
    FirebaseDatabase.instance
        .reference()
        .child(
            "lastm/" + key + "_" + widget.id_post + "/vu_" + widget.user_him.id)
        .onValue
        .listen((val) {
      var d = val.snapshot.value;

      if (d != null) {
        //Message lu
        if (d == "0") {
          try {
            setState(() {
              vu = true;
            });
          } catch (e) {}
        }
        //Message non lu
        else {
          try {
            setState(() {
              vu = false;
            });
          } catch (e) {}
        }
      }
    });
  }

  List list = [];
  ProductSServices prod = ProductSServices();

  /*get_post(cat_id) async {
    var pr_resp = await rest.get('item/' + cat_id.toString());
    if (pr_resp == "No Internet") return "No Internet";

    var sp = pr_resp;

    return new Product.fromMap(sp);
  }*/

  getpost() async {
    var res = await prod.get_details_product(widget.id_post);

    if (!this.mounted) return;
    setState(() {
      post = res;
    });
  }

  prepare_database() {
    messageviewedbyme(widget.user_m.id, widget.user_him.id);
    checkviewMessage(getKey(widget.user_m.id, widget.user_him.id));

    setState(() {
      gMessagesDbRef2 = FirebaseDatabase.instance
          .reference()
          .child("room")
          .child(widget.user_m.id.toString() +
              '_' +
              widget.user_him.id.toString() +
              "_" +
              widget.id_post.toString());
      gMessagesDbRef = FirebaseDatabase.instance
          .reference()
          .child("message/" + widget.id_post)
          .child(getKey(widget.user_m.id, widget.user_him.id));
      gMessagesDbRef_inv = FirebaseDatabase.instance
          .reference()
          .child("message/" + widget.id_post)
          .child(getKey(widget.user_him.id, widget.user_m.id));
    });
  }

  initPostMessage() async {
    if (widget.post == null) {
      await getpost();
    } else {
      post = widget.post!;
    }
    prepare_database();
  }

  @override
  initState() {
    super.initState();

    initPostMessage();
  }

  late String owner, message;
  bool show_this = false;

  func(ow, mess) {
    setState(() {
      show_this = true;
      owner = ow;
      message = mess;
    });
  }

  List images = [];

  bool image_show = false;
  bool uploading1 = false;

  gallery() async {
    images = [];
    PickedFile platformVersion;
    setState(() {
      uploading1 = true;
      image_show = true;
    });
    try {
      platformVersion =
          (await ImagePicker.platform.pickImage(source: ImageSource.gallery))!;
      //for (var i in platformVersion) {
      //images.add(i);
      await compress(File(platformVersion.path));
      setState(() {
        uploading1 = false;
        //image_show = true;
      });
    } on PlatformException {}

    if (!mounted) return;
  }

  Future compress(image) async {
    File compressedFile = await FlutterNativeImage.compressImage(
        image.path as String,
        quality: 60);
    await save_image(compressedFile, ".jpg");
    return true;
  }

  ///image
  _handleCameraButtonPressed() async {
    images = [];

    PickedFile? image =
        await ImagePicker.platform.pickImage(source: ImageSource.camera);
    await compress(File(image!.path));
    // _sendMessage(imageUrl: images, text: "", type: "image");
  }

  open_bottomsheet() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => new Dialog(
            child: new Container(
                height: 150.0,
                child: new Container(
                    // padding: const EdgeInsets.only(left: 8.0,right: 8.0),
                    child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                      new ListTile(
                          dense: true,
                          onTap: () {
                            Navigator.of(context).pop();
                            _handleCameraButtonPressed();
                          },
                          title: new Text(
                            "Camera",
                            style: TextStyle(
                              fontSize: 16.0,
                            ),
                          )),
                      Container(
                          height: 1.0, width: 1000.0, color: Colors.grey[200]),
                      new ListTile(
                          dense: true,
                          onTap: () {
                            Navigator.of(context).pop();
                            gallery();
                          },
                          title: new Text("Gallerie",
                              style: TextStyle(
                                fontSize: 16.0,
                              ))),
                      Container(
                          height: 1.0, width: 1000.0, color: Colors.grey[200]),
                      Container(
                          height: 1.0, width: 1000.0, color: Colors.grey[200]),
                    ])))));
  }

  save_image(File image, ext) async {
    setState(() {
      uploading = true;
    });

    int timestamp = new DateTime.now().millisecondsSinceEpoch;
    Reference storageReference = FirebaseStorage.instance
        .ref()
        .child("profile/img_" + timestamp.toString() + (ext as String));
    var val = await storageReference.putFile(image);
    var va = await val.ref.getDownloadURL();
    //if (!mounted) return;

    setState(() {
      images.add(va.toString());
      uploading = false;
    });

    return true;
  }

  update_typing(bl) {
    FirebaseDatabase.instance
        .reference()
        .child("room")
//        .child(widget.user_him.id + '_' + widget.user_m.id)
        .child(widget.user_m.id + '_' + widget.user_him.id)
        .update({"typing": bl});
  }

  _onChangeHandler(String value) {
    setState(() {
      _messageText = value;
    });

    if (typing != true) {
      update_typing(true);
    }

    const duration = Duration(
        milliseconds:
            800); // set the duration that you want call search() after that.
    /* if (searchOnStoppedTyping != null) {
      setState(() => searchOnStoppedTyping!.cancel()); // clear timer
    }

    setState(
        () => searchOnStoppedTyping = new Timer(duration, () => search(value)));*/
  }

  search(value) {
    update_typing(false);
  }

  _sendMessage({String text = "", imageUrl, type, audio, recorderTime}) async {
    try {
      setState(() {
        show_textfield = true;
      });
    } catch (e) {}
    print(widget.user_m.id);

    gMessagesDbRef!.push().set({
      'replyText': replyText,
      'isReply': isReply,
      'idReply': idReply,
      'timestamp': ServerValue.timestamp,
      'messageText': text,
      'idUser': widget.user_m.id,
      'audio': audio,
      'imageUrl': imageUrl,
      'recorderTime': recorderTime,
      'type': type
    });

    gMessagesDbRef_inv!.push().set({
      'replyText': replyText,
      'isReply': isReply,
      'idReply': idReply,
      'timestamp': ServerValue.timestamp,
      'messageText': text,
      'idUser': widget.user_m.id,
      'audio': audio,
      'imageUrl': imageUrl,
      'recorderTime': recorderTime,
      'type': type
    });

    var lastmsg = "";

    lastmsg = text;

    gMessagesDbRef2.set({
      //"token": user_o.token,
      "name": widget.user_m.userename,
      "me": true,
      "token": widget.user_him.token_notification,
      "idUser": widget.user_m.id,
      "lastmessage": lastmsg,
      "key": getKey(
        widget.user_m.id,
        widget.user_him.id,
      ),
      "timestamp": ServerValue.timestamp /*new DateTime.now().toString()*/,
    });

    FirebaseDatabase.instance
        .reference()
        .child("room")
        .child(
            widget.user_him.id + "_" + widget.user_m.id + "_" + widget.id_post)
        .set({
      "me": false,
      "idUser": widget.user_him.id,
      "lastmessage": text,
      "key": getKey(widget.user_him.id, widget.user_m.id),
      "timestamp": ServerValue.timestamp /*new DateTime.now().toString()*/,
    });

    var gMessagesDbRe =
        FirebaseDatabase.instance.reference().child("notif_new");
    gMessagesDbRe.set({widget.user_him.id: true});

    gMessagesDbRef3.set({
      "vu_" + widget.user_m.id.toString(): "0",
      "vu_" + widget.user_him.id.toString(): "0",
      "id_last": widget.user_m.id.toString()
    });

    FirebaseDatabase.instance
        .reference()
        .child("lastm")
        .child(getKey(
              widget.user_him.id,
              widget.user_m.id,
            ) +
            "_" +
            widget.id_post)
        .set({
      "vu_" + widget.user_m.id.toString(): "0",
      "vu_" + widget.user_him.id.toString(): "1",
      "id_last": widget.user_m.id.toString()
    });
    setState(() {
      isReply = false;
    });
    replyText = "";
    idReply = "";
  }

  _handleMessageSubmit(String text) async {
    try {
      setState(() => _isComposing = false);
      //
    } catch (e) {}

    if (images.isNotEmpty) {
      await _sendMessage(imageUrl: images, text: "", type: "image");
      setState(() {
        images = [];
        image_show = false;
      });
    } else if (_textController.text != "") _sendMessage(text: text);
    _textController.clear();
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  func_reply() {}

  @override
  Widget build(BuildContext context) {
    List<Widget> widgets() {
      return images
          .map((file) => new Stack(children: <Widget>[
                new Container(
                    padding: new EdgeInsets.all(4.0),
                    height: ScreenUtil().setHeight(60),
                    width: ScreenUtil().setWidth(60),
                    child: ClipRRect(
                        //<--clipping image
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        child: new Image.network(
                          file.toString(),
                          fit: BoxFit.cover,
                        ))),
                new Positioned(
                    top: 0.0,
                    right: 0.0,
                    child: new InkWell(
                      child: new Center(
                          child: new SvgPicture.asset(
                        "assets/images/remove.svg",
                        width: 20.w,
                        height: 20.w,
                      )),
                      onTap: () {
                        setState(() {
                          images.remove(file);
                          image_show = false;
                        });
                      },
                    ))
              ]))
          .toList();
    }

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,

          actions: [
            /* IconButton(
            icon: SvgPicture.asset(
                /*_index==4?"assets/images/icons/edit.svg":*/
                "assets/images/icons/menu.svg"),
            onPressed: () {
              /*Navigator.push(context,
                  new MaterialPageRoute(builder: (BuildContext context) {
                return CategoriesList();
              }));*/
            },
          )*/
          ],
          iconTheme: IconThemeData(color: ColorsConst.col_grey),
          title: Text(
            widget.user_him.userename.toString(),
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: false,

          elevation: 3.0,
          bottom: new PreferredSize(
              preferredSize: new Size(MediaQuery.of(context).size.width,
                  ScreenUtil().setHeight(175.82)),
              child: post.toString() == "null"
                  ? Container(

                      height: ScreenUtil().setHeight(175.82),
                      color: Colors.white,
                      child: Center(
                        child: Theme(
                            data: ThemeData(
                                cupertinoOverrideTheme: CupertinoThemeData(
                                    brightness: Brightness.light)),
                            child: CupertinoActivityIndicator()),
                      ))
                  : post.toString() == "null"
                      ? Center(
                          child: CupertinoActivityIndicator(),
                        )
                      : Container(
                          height: ScreenUtil().setHeight(175.82),
                          color: Colors.white,
                          child: Column(children: [
                            Container(height: ScreenUtil().setHeight(14)),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: 34.w,
                                ),
                                SizedBox(
                                    height: ScreenUtil().setWidth(49),
                                    width: ScreenUtil().setWidth(49),
                                    child: Stack(children: <Widget>[
                                      CircleAvatar(
                                        radius: ScreenUtil().setWidth(24.5),
                                        child: ClipOval(
                                          child: Image.network(
                                            widget.user_him.image == null
                                                ? ""
                                                : widget.user_him.image,
                                            height: ScreenUtil().setWidth(87),
                                            width: ScreenUtil().setWidth(87),
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                          top: -3,
                                          left: -2,
                                          child: OnlineWidget(
                                              widget.user_him, false)),
                                    ])),
                                Container(
                                  width: 14.w,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      widget.user_him.userename.toString(),
                                      style: TextStyle(
                                          color: ColorsConst.col_grey,
                                          fontSize: ScreenUtil().setSp(15.5),
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Container(height: 4.h),
                                    OnlineWidget(widget.user_him, true)
                                  ],
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                                ClipRRect(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(16)),
                                    child: Container(
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                                image: AssetImage(
                                                    "assets/images/placeholder.png"),
                                                fit: BoxFit.contain)),
                                        child: FadeInImage.assetNetwork(
                                          image: post!.images[0].url,
                                          placeholder:
                                              "assets/images/placeholder.png",
                                          height: ScreenUtil().setHeight(72.82),
                                          width: ScreenUtil().setWidth(100.82),
                                          fit: BoxFit.cover,
                                        ))),
                                Container(
                                  width: ScreenUtil().setWidth(14),
                                )
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 32.w),

                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(height: ScreenUtil().setHeight(14)),
                                  new Text(post!.name,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: new TextStyle(
                                          fontSize: ScreenUtil().setSp(13.0),
                                          height: 1.1,
                                          fontWeight: FontWeight.bold,
                                          color: ColorsConst.col_grey)),
                                  // Container(height: 16.0,),

                                  /* new Text(post.last_price + " KD",
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: new TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white)),*/
                                  Container(
                                    height: 10.h,
                                  ),

                                  new Text(post!.description.toString(),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: new TextStyle(
                                          fontSize: ScreenUtil().setSp(15.0),
                                          height: 1.1,
                                          fontWeight: FontWeight.w600,
                                          color: ColorsConst.col_grey)),
                                ],
                              ),
                            )
                          ]))),

          /*
        125,82
         */

          /* bottom: new PreferredSize(
      preferredSize: new Size(MediaQuery.of(context).size.width,
        ScreenUtil.getInstance().setWidth(80)),

    child: new Container(
    padding: EdgeInsets.only(left: ScreenUtil.getInstance().setWidth(70),right: ScreenUtil.getInstance().setWidth(70)),
    child: Row(
    children: <Widget>[
    new ClipOval(
    child: new Container(
    width: 67.46,
    height: 67.46,
    child: new Image.network(
    im_post,
    fit: BoxFit.cover,
    ),
    )),
    Container(width: 16,),
    Column(crossAxisAlignment: CrossAxisAlignment.start,children: <Widget>[
    Text(widget.text,style: TextStyle(color: Fonts.col_blue,fontWeight: FontWeight.w700),),
   // Text("23 Members",style: TextStyle(color: Fonts.col_grey),),
   // Text("4 Online",style: TextStyle(color: Fonts.col_green ),),
    ],)

    ],
    )))*/
          //: LinearGradient(colors: [Colors.blue, Colors.purple, Colors.red])
        ),
        /*AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_backspace,
          ),
          onPressed: ()=>Navigator.pop(context),
        ),
        titleSpacing: 0,
        title: InkWell(
          child: Row(

            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(left: 0.0, right: 10.0),
                child: CircleAvatar(
                  backgroundImage: AssetImage(
                    "assets/cm${random.nextInt(10)}.jpeg",
                  ),
                ),
              ),

              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 15.0),
                    Text(
                      name,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14,
                      ),
                    ),
                    SizedBox(height: 5),
                    Text(
                      "Online",
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 11,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),

          onTap: (){},
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.more_horiz,
            ),
            onPressed: (){},
          ),
        ],
      )*/

        body: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus) {
              currentFocus.unfocus();
            }
          },
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: <Widget>[
                SizedBox(height: 10),
                Flexible(
                    child: new Container(
                  // padding: new EdgeInsets.only(bottom: 76.0),
                  //color: Fonts.backcolor,
                  child: gMessagesDbRef == null
                      ? Container()
                      : new FirebaseAnimatedList(
                          // defaultChild: new Center(child: new Text("..."),),
                          padding: new EdgeInsets.only(
                              top: 8.0, bottom: 0.0, left: 16.0, right: 16.0),
                          query: gMessagesDbRef!,
                          sort: (a, b) => Map<String, dynamic>.from(
                                  b.value as dynamic)['timestamp']
                              .compareTo(Map<String, dynamic>.from(
                                  a.value as dynamic)['timestamp']),
                          reverse: true,
                          itemBuilder: (_, DataSnapshot snap,
                              Animation<double> animation, int a) {
                            print("heeeeey");

                            return ChatBubble(
                              func_remply,
                              file: Map<String, dynamic>.from(
                                  snap.value as dynamic)["audio"],
                              imageUrl: Map<String, dynamic>.from(
                                  snap.value as dynamic)["imageUrl"],
                              type: Map<String, dynamic>.from(
                                  snap.value as dynamic)["type"],
                              recorderTime: Map<String, dynamic>.from(
                                  snap.value as dynamic)["recorderTime"],
                              id_user: Map<String, dynamic>.from(
                                  snap.value as dynamic)["idUser"],
                              message: Map<String, dynamic>.from(
                                  snap.value as dynamic)["messageText"],
                              username: Map<String, dynamic>.from(
                                          snap.value as dynamic)["idUser"] ==
                                      widget.user_m.id
                                  ? widget.user_m.userename
                                  : widget.user_him.userename,
                              time: new DateFormat('yyyy-MM-dd HH:mm').format(
                                  new DateTime.fromMillisecondsSinceEpoch(
                                      Map<String, dynamic>.from(
                                          snap.value as dynamic)['timestamp'])),
                              replyText: Map<String, dynamic>.from(
                                  snap.value as dynamic)["replyText"],
                              isMe: Map<String, dynamic>.from(
                                          snap.value as dynamic)["idUser"] ==
                                      widget.user_m.id
                                  ? true
                                  : false,
                              isGroup: false,
                              isReply: Map<String, dynamic>.from(
                                              snap.value as dynamic)["isReply"]
                                          .toString() ==
                                      "null"
                                  ? false
                                  : Map<String, dynamic>.from(
                                      snap.value as dynamic)["isReply"],
                              replyName: Map<String, dynamic>.from(
                                          snap.value as dynamic)["idReply"] ==
                                      widget.user_m.id
                                  ? widget.user_m.userename
                                  : widget.user_him.userename,
                            ) /*new ChatMessage("",
                        msg1: widget.user_m,
                        msg2: widget.user_him,
                        snapshot: snap,
                        animation: animation,
                        func: func,
                        owner: owner,
                        message: message)*/
                                ;
                          },
                          duration: new Duration(milliseconds: 1000),
                        ),
                )
                    /*ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: 10),
                itemCount: conversation.length,
                reverse: true,
                itemBuilder: (BuildContext context, int index) {
                  Map msg = conversation[index];
                  return ChatBubble(
                    message: msg['type'] == "text"
                        ?messages[random.nextInt(10)]
                        :"assets/cm${random.nextInt(10)}.jpeg",
                    username: msg["username"],
                    time: msg["time"],
                    type: msg['type'],
                    replyText: msg["replyText"],
                    isMe: msg['isMe'],
                    isGroup: msg['isGroup'],
                    isReply: msg['isReply'],
                    replyName: name,
                  );
                },
              ),*/
                    ),
                isReply
                    ? Container(
                        color: Colors.white,
                        padding: EdgeInsets.only(left: 16, right: 16),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                new Text(
                                  idReply == widget.user_m.id
                                      ? "You"
                                      : widget.user_him.userename,
                                  style: TextStyle(color: ColorsConst.col_app),
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                                InkWell(
                                  onTap: () {
                                    setState(() {
                                      replyText = "";
                                      isReply = false;
                                      idReply = "";
                                    });
                                  },
                                  child: Icon(
                                    Icons.close,
                                    color: Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text(
                                  replyText,
                                  style: TextStyle(
                                    color: ColorsConst.col_grey,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                  ),
                                  maxLines: 1,
                                  textAlign: TextAlign.left,
                                ),
                                Expanded(child: Container()),
                              ],
                            ),
                          ],
                        ),
                        alignment: Alignment.centerLeft,
                      )
                    : Container(
                        height: 0.01,
                      ),
                Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          typing
                              ? id_typing == widget.user_him.id
                                  ? Text(
                                      widget.user_him.userename + " is typing")
                                  : Container()
                              : Container(),
                          Flexible(
                            child: _isRecording
                                ? Container()
                                : new Container(
                                    //height: MediaQuery.of(context).size.height * 0.10,

                                    // width: MediaQuery.of(context).size.width * 0.85,
                                    child: Row(children: <Widget>[
                                    new Container(
                                      padding: EdgeInsets.only(
                                          left: ScreenUtil()
                                              .setWidth(8)
                                              .toDouble(),
                                          top: ScreenUtil()
                                              .setHeight(16)
                                              .toDouble(),
                                          bottom: ScreenUtil()
                                              .setHeight(16)
                                              .toDouble(),
                                          right: ScreenUtil()
                                              .setWidth(0)
                                              .toDouble()),
                                      child: Container(
                                          width: ScreenUtil()
                                              .setWidth(326)
                                              .toDouble(),
                                          decoration: BoxDecoration(
                                              color: const Color(0xffCACFCD)
                                                  .withOpacity(0.2),
                                              borderRadius:
                                                  BorderRadius.circular(32.0)),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Container(
                                                width: 8,
                                              ),

                                              /* image_show==true
                                              ? Container()
                                              : IconButton(
                                                  padding: EdgeInsets.all(0),
                                                  onPressed: () {
                                                    open_bottomsheet();
                                                  },
                                                  icon: SvgPicture.asset(
                                                      'assets/images/attach.svg'),
                                                ),*/
                                              Container(
//                                        height: ScreenUtil()
//                                            .setHeight(41)
//                                            .toDouble(),
                                                  margin: EdgeInsets.only(
                                                      left: 12.0,
                                                      right: 12.0,
                                                      top: 0,
                                                      bottom: 0),
                                                  width: ScreenUtil()
                                                      .setWidth(250)
                                                      .toDouble(),
                                                  child: image_show == true
                                                      ? uploading1
                                                          ? CupertinoActivityIndicator()
                                                          : new Container(
                                                              height:
                                                                  ScreenUtil()
                                                                      .setHeight(
                                                                          87),
                                                              child: new Row(
                                                                  children:
                                                                      widgets()))
                                                      : Center(child: Builder(
                                                          builder: (context) {
                                                          int? linesCount = _messageText
                                                                      .split(
                                                                          '\n')
                                                                      .length >=
                                                                  3
                                                              ? 3
                                                              : _messageText
                                                                  .split('\n')
                                                                  .length;
                                                          if (linesCount ==
                                                                  null ||
                                                              linesCount < 2) {
                                                            linesCount = null;
                                                          }

                                                          return new TextField(
                                                            onChanged:
                                                                _onChangeHandler,
                                                            focusNode: _focus,
                                                            textInputAction:
                                                                TextInputAction
                                                                    .done,
                                                            /*textDirection:
                                                    prefix0.TextDirection.rtl,*/
                                                            keyboardType:
                                                                TextInputType
                                                                    .multiline,

                                                            controller:
                                                                _textController,
                                                            maxLines:
                                                                linesCount,

                                                            onSubmitted:
                                                                _isComposing
                                                                    ? null
                                                                    : null,
                                                            decoration: new InputDecoration(
                                                                border:
                                                                    InputBorder
                                                                        .none,
                                                                enabledBorder:
                                                                    InputBorder
                                                                        .none,
                                                                isDense: true,
                                                                hintText:
                                                                    "type_m".tr(),
                                                                hintStyle: new TextStyle(
                                                                    fontSize:
                                                                        17.0,
                                                                    color: Colors
                                                                        .grey)),
                                                            /*onSaved: (String text) {
                                                try {
                                                  setState(() => _isComposing =
                                                      text.length > 0);
                                                } catch (e) {}
                                          },*/

                                                            // See GitHub Issue https://github.com/flutter/flutter/issues/10006
                                                          );
                                                        }))),
                                              Expanded(
                                                child: Container(),
                                              ),
                                              Row(
                                                children: <Widget>[
                                                  IconButton(
                                                      onPressed: () {
                                                        _handleMessageSubmit(
                                                            _textController
                                                                .text);
                                                      },
                                                      icon: Icon(Icons.send,
                                                          color: _textController
                                                                      .text !=
                                                                  ""
                                                              ? ColorsConst
                                                                  .col_app
                                                              : Color(
                                                                  0xff57647a))),
                                                ],
                                              ),
                                              Container(
                                                width: ScreenUtil()
                                                    .setWidth(0)
                                                    .toDouble(),
                                              ),
                                            ],
                                          )),
                                    ),
                                    Expanded(
                                      child: Container(),
                                    ),

                                    image_show == true
                                        ? Container()
                                        : Container(
                                            margin: EdgeInsets.symmetric(
                                                horizontal: 1.0),
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                shape: BoxShape.circle),
                                            child: IconButton(
                                              icon: SvgPicture.asset(
                                                  "assets/icons/audio.svg"),
                                              onPressed: _onRecorderPreesed,
                                              color: Colors.black,
                                            ),
                                          ),

                                    //jiji
                                  ])),
                          ),
                        ],
                      ),
                    ),
                    _isRecording ? _buildRecordingView() : SizedBox()
                  ],
                )
                /*Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Flexible(
                    child: show_textfield
                        ? new Container(
                            height: MediaQuery.of(context).size.height * 0.10,
                            padding: EdgeInsets.only(
                                top: 8, bottom: 8, left: 8, right: 8),
                            // width: MediaQuery.of(context).size.width * 0.85,
                            child: new Card(
                                elevation: 1.0,
                                color: Colors.white,
                                child: new Container(
                                  padding:
                                      EdgeInsets.only(left: 12.0, right: 12.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.7,
                                          child: new TextFormField(
                                            textDirection:
                                                prefix0.TextDirection.rtl,
                                            keyboardType:
                                                TextInputType.multiline,

                                            controller: _textController,
                                            maxLines: null,

                                            onFieldSubmitted:
                                                _isComposing ? null : null,
                                            decoration:
                                                new InputDecoration.collapsed(
                                                    hintText: "SEND",
                                                    hintStyle: new TextStyle(
                                                        fontSize: 17.0,
                                                        color: Colors.grey)),
                                            onSaved: (String text) {
                                              try {
                                                setState(() => _isComposing =
                                                    text.length > 0);
                                              } catch (e) {}
                                            },

                                            // See GitHub Issue https://github.com/flutter/flutter/issues/10006
                                          )),
                                      Expanded(
                                        child: Container(),
                                      ),
                                      InkWell(
                                          onTap: () {
                                            _handleMessageSubmit(
                                                _textController.text);
                                          },
                                          child: new RotatedBox(
                                              quarterTurns: 2,
                                              child: Icon(Icons.send)))
                                    ],
                                  ),
                                )))
                        : Container(),
                  ),
                ],
              ),
            ),*/
              ],
            ),
          ),
        ));
  }
}
