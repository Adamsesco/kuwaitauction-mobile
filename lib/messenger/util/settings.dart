
import 'package:kauction/app/utils/rest_service.dart';
import 'package:kauction/model/models/notification.dart';
import 'package:kauction/model/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Settings_Services {
  RestService rest = new RestService();


  user_informations(id) async {
    var pr_resp = await rest.get('profile/' + id);
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    Map<String, dynamic> sp = pr_resp["data"];
    return new User.fromMap(sp, id);
  }



  get_notifs() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? token = prefs.getString("token");
    var pr_resp = await rest.get('notifications?token=$token');
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

   // print(pr_resp);
    List sp = pr_resp["notifications"];
    if (sp == null)
      return List<LNotification>.from([]);
    else
      return sp
          .map((var contactRaw) => new LNotification.fromMap(contactRaw))
          .toList();
  }


  user_info_other(id) async {

    var pr_resp = await rest.get('profile/' + id.toString());
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    print(pr_resp);
    Map<String, dynamic> sp = pr_resp;
    return new User.fromMap(sp["data"], id);
  }

  user_info() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? id = prefs.getString("id");
    var pr_resp = await rest.get('profile/' + id.toString());
    if (pr_resp == "No Internet") return "No Internet";
    if (pr_resp == "error") return "error";

    print(pr_resp);
    Map<String, dynamic> sp = pr_resp;
    return new User.fromMap(sp["data"], id);
  }
}
