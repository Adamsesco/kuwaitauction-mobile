import 'package:flutter/material.dart';
import 'package:kauction/views/home/home_screen.dart';
import 'package:kauction/views/home/publish_screens/categories_list_page.dart';
import 'package:kauction/views/phone_verification/phone_verification_screen.dart';
import 'package:kauction/views/phone_verification/verification_code_page.dart';
import 'package:kauction/views/signin_screen/signin.dart';
import 'package:kauction/views/signup_screen/signup_screen.dart';

class AppRouter {
  static const String HOME = '/home';
  static const String PHONEVerification = '/Phoneverification';
  static const String REGISTER = '/register';
  static const String LOGIN = '/Login';
  static const String CAtEgoriesWidgetList = '/CategoriesWidgetList';

  static const String PHONECODE = '/phonecode';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case PHONEVerification:
        return MaterialPageRoute(builder: (_) => PhoneVerificationScreen());

      case CAtEgoriesWidgetList:
        return MaterialPageRoute(builder: (_) => CategoriesWidgetList());
      case LOGIN:
        return MaterialPageRoute(builder: (_) => SigninScreen(settings.arguments as String));
      case PHONECODE:
        return MaterialPageRoute(
            builder: (_) => VerificationCodePage(settings.arguments as String));
      case HOME:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case REGISTER:
        return MaterialPageRoute(
            builder: (_) => SignupScreen((settings.arguments as String)));

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
