import 'package:equatable/equatable.dart';
import 'package:kauction/model/models/user.dart';
import 'package:meta/meta.dart';

abstract class SigninState extends Equatable {
  const SigninState();
}

class InitialSignintate extends SigninState {
  @override
  List<Object> get props => [];
}

class SigninProgress extends SigninState {
  @override
  List<Object> get props => [];
}

class SigninFailure extends SigninState {
  final int errorCode;
  final String errorMessage;

  SigninFailure({required this.errorCode, required this.errorMessage});

  @override
  List<Object> get props => [errorCode, errorMessage];
}

class SigninSuccess extends SigninState {
  final User user;

  SigninSuccess({required this.user});

  @override
  List<Object> get props => [user];
}

