import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kauction/app/signin_bloc/signin_event.dart';
import 'package:kauction/app/signin_bloc/signin_state.dart';
import 'package:kauction/app/verify_phone/bloc.dart';
import 'package:kauction/model/repositories/register_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SigninBloc extends Bloc<SigninEvent, SigninState> {
  RegisterRepository registerRepository;

  SigninBloc({required this.registerRepository}) : super(InitialSignintate()) {
    registerRepository = RegisterRepository();
  }

  @override
  SigninState get initialState => InitialSignintate();

  @override
  Stream<SigninState> mapEventToState(
    SigninEvent event,
  ) async* {
    if (event is SignInRequested) {
      yield SigninProgress();

      final result =
          await registerRepository.signin(event.phone, event.password);
      if (result.code == 0) {
        yield SigninFailure(errorCode: 2, errorMessage: result.message);
      } else {
        //  signInBloc = BlocProvider.of<SignInBloc>(event.context);

        SharedPreferences prefs = await SharedPreferences.getInstance();

        // prefs.setString("id", result.user.id);
        // prefs.setString("session", result.user.session);
        //signInBloc.add(SignInSucccessEvent(user: result.user));
        yield SigninSuccess(user: result.user);
      }
    } else if (event is SignInSucccessEvent) {
      yield SigninSuccess(user: event.user);
    } else if (event is SignInSucccessWithoutEvent) {
      yield SigninSuccess(user: event.user);
    }
  }
}
