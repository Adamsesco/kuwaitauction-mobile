import 'package:equatable/equatable.dart';
import 'package:kauction/model/models/user.dart';
import 'package:meta/meta.dart';

abstract class SigninEvent extends Equatable {
  const SigninEvent();
}

class SignInRequested extends SigninEvent {
  final String phone;
  final String password;

  var context;

  SignInRequested({required this.phone,required this.password, this.context});

  @override
  List<Object> get props => [phone,password, context];
}


class SignInSucccessEvent extends SigninEvent {
  User user;

  SignInSucccessEvent({required this.user});

  @override
  List<Object> get props => [user];
}


class SignInSucccessWithoutEvent extends SigninEvent {
  User user;

  SignInSucccessWithoutEvent({required this.user});

  @override
  List<Object> get props => [user];
}



