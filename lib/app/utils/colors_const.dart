
import 'package:flutter/material.dart';

class ColorsConst {


  static const Color hintColor =     const Color(0xff949F99);
  static const Color col_app =     const Color(0xff49C691);
  static const Color col_app_blue =     const Color(0xff4263EB);

  static const Color col_clair =     const Color(0xffFFFFFF);
  static const Color col_app_yellow =     const Color(0xffF3BC33);
  static const Color col_app_white =     const Color(0xffFFFFFF);
  static const Color col_back_button =     const Color(0xffE6EAE9);
  static const Color col_grey =     const Color(0xffB4B4B4);
  static const Color col_black =     const Color(0xff000000);
  static const Color col_appbar =     const Color(0xffE5F5EB);
  static const Color col_background =     const Color(0xffF7F5F5);
  static const Color col_text_grey =     const Color(0xff909090);
  static const Color col_select_card =     const Color(0xffFFF3D8);

  static const Color fill_color =     const Color(0xffF9FAFA);
  static const Color border_color =     const Color(0xffDBDBDB);
  static const Color col_grey_fon =     const Color(0xff211F32);
  static const Color col_black2 =     const Color(0xff323045);

}
