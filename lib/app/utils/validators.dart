import 'package:easy_localization/easy_localization.dart';

class Validators {
  static String? validatePhone(String? value) {
    if (value.toString().length < 6) return 'valid phone number is required !';
    /*else
      return null as String;*/
  }

  static String? validatefirstname(String? value) {
    if (value!.length == 0)
      return "Please enter your name !";
  }

  static String? validateprice(String? value) {
    if (value!.length == 0)
      return "Please enter the price !".tr();
  }


  static String? validatetitle(String? value) {
    if (value!.length == 0)
      return "Please enter the title ! ".tr();
  }

  static String? validatedesc(String? value) {
    if (value!.length == 0)
      return "descv".tr();
  }


  static String? validatefirstnamee(String? value) {
    if (value!.length ==0)
      return "Veuillez entrer le prénom !";
  }

  static String? validatePassword(String? value) {
    if (value!.length < 5)
      return "Le mot de passe doit contenir au moins 6 caractères !";
  }

  static String? validateEmail(String? value) {
    if (!RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(value.toString()))
      return 'Veuillez entrer une adresse email valide !';
    /*  else
      return null as String;*/
  }

  static String? validatelastname(String? value) {
    if (value!.length == 0)
      return "Veuillez entrer le nom !";
  }

}
