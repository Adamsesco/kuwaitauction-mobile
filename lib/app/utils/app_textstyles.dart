import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kauction/app/utils/colors_const.dart';

class AppTextStyles {
  static var footer =
      new TextStyle(color: ColorsConst.col_app, fontSize: 9.5.sp);

  static  appBar( ctx ) => TextStyle(
      fontSize: 22.0.sp,
      fontWeight: FontWeight.w700,
      fontFamily:ctx != "en"?"cairo_bold":"poppins_bold",
      color: ColorsConst.col_app);

  static  appBar_tt(ctx) => TextStyle(
      fontSize: 20.0.sp,
      fontWeight: FontWeight.w700,
      fontFamily: ctx == "en"?"poppins_bold":"cairo_bold" ,
      height: 0.8,
      color: ColorsConst.col_app);

  static var hint =
      new TextStyle(fontSize: 14.5.sp, color: ColorsConst.col_app);

  static  title(ctx) => new TextStyle(
      fontSize: 30.5.sp,
      fontWeight: FontWeight.w500,
      color: ColorsConst.col_black,
  fontFamily:ctx != "en"?"cairo_bold":"poppins_bold",
     );

  static  title2(ctx) => new TextStyle(
      fontSize: 18.5.sp,
      fontWeight: FontWeight.w800,
      color: ColorsConst.col_app,
      fontFamily: ctx != "en"?"cairo_bold":"poppins_bold");

  static  date1(ctx) => new TextStyle(
      fontSize: 16.5.sp,
      fontWeight: FontWeight.w800,
      color: ColorsConst.col_app_yellow,
      fontFamily: ctx != "en"?"cairo_bold":"poppins_bold");

  static var small_title2 = new TextStyle(
      fontSize: 15.5.sp,
      fontWeight: FontWeight.w500,
      color: ColorsConst.col_app);


  static  cat_title2(ctx) => new TextStyle(
      fontSize: 15.5.sp,
      fontWeight: FontWeight.w900,
      fontFamily: ctx != "en"?"cairo_bold":"poppins_bold"
     );


  static var small_title4 = new TextStyle(
      fontSize: 15.5.sp,
      fontWeight: FontWeight.w500,
      color: ColorsConst.col_black);

  static var small_title = new TextStyle(
      fontSize: 14.5.sp,
      fontWeight: FontWeight.w500,
      color: ColorsConst.col_text_grey);

  static  small_title_bold(ctx) => new TextStyle(
      fontSize: 15.5.sp,
      color: ColorsConst.col_black,
      fontWeight: FontWeight.w700,
      fontFamily: ctx != "en"?"cairo_bold":"poppins_bold");

  static  small_title_little(ctx) => new TextStyle(
      fontSize: 15.5.sp,
      color: ColorsConst.col_black,
      fontWeight: FontWeight.w900,
      fontFamily: ctx != "en"?"cairo_bold":"poppins_bold");
}
