import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:kauction/model/repositories/verify_phone_repository.dart';
import './bloc.dart';

class VerifyMemberBloc
    extends Bloc<VerifyMembreEventRequested, VerifyMemberState> {
  VerifyNumberRepository verifyRepository;

  VerifyMemberBloc({required this.verifyRepository})
      : super(InitialVerifyMember());

  @override
  VerifyMemberState get initialState => InitialVerifyMember();

  @override
  Stream<VerifyMemberState> mapEventToState(
      VerifyMembreEventRequested event) async* {
    if (event is VerifyMembreEventRequested) {
      yield VerifyMemberProgress();
      final result = await verifyRepository.verify_phone(event.phone);

      if (result["status"].toString() != "200") {
        yield VerifyMemberFailure(errorCode: 2, errorMessage: "Error ! ");
      } else {
        yield VerifyMemberSuccess(
            code: result["code"].toString() == "2"
                ? ""
                : result["results"]["otp"].toString());
      }
    }

    if (event is VerifyMembreEventRequestedCode) {
      yield VerifyMemberProgress();
      final result = await verifyRepository.verify_phone(event.phone);

      if (result["status"] != 200) {
        yield VerifyMemberFailure(errorCode: 2, errorMessage: "Error ! ");
      } else {
        yield VerifyMemberSuccess(code: result["results"]["otp"].toString());
      }
    }
  }
}
