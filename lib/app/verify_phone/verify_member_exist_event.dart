import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';


abstract class VerifyMembreEvent extends Equatable {
  const VerifyMembreEvent();
}

class VerifyMembreEventRequested extends VerifyMembreEvent {
  final String phone;

  VerifyMembreEventRequested({required this.phone});

  @override
  List<Object> get props => [phone];
}


class VerifyMembreEventRequestedCode extends VerifyMembreEvent {
  final String phone;
  final String code;

  VerifyMembreEventRequestedCode({required this.phone, required this.code});

  @override
  List<Object> get props => [phone, code];
}