

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class VerifyMemberState extends Equatable{
  const VerifyMemberState();
}


class InitialVerifyMember extends VerifyMemberState {
  @override
  List<Object> get props => [];
}

class VerifyMemberProgress extends VerifyMemberState {
  @override
  List<Object> get props => [];
}

class VerifyMemberFailure extends VerifyMemberState {
  final int errorCode;
  final String errorMessage;

  VerifyMemberFailure({required this.errorCode, required this.errorMessage});

  @override
  List<Object> get props => [errorCode, errorMessage];
}

class VerifyMemberSuccess extends VerifyMemberState {
  final String code;


  VerifyMemberSuccess({required this.code});

  @override
  List<Object> get props => [code];
}


