import 'package:equatable/equatable.dart';
import 'package:kauction/model/models/homereponse.dart';
import 'package:meta/meta.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class InitialFeedState extends HomeState {
  @override
  List<Object> get props => [];
}

class FeedLoadInProgress extends HomeState {
  @override
  List<Object> get props => [];
}

class FeedLoadSuccess extends HomeState {
  final HomeResponse homeResponse;

  FeedLoadSuccess({required this.homeResponse});

  @override
  List<Object> get props => [homeResponse];
}

class FeedLoadFailure extends HomeState {
  final HomeResponse homeResponse;

  FeedLoadFailure({required this.homeResponse});

  @override
  List<Object> get props => [homeResponse];
}
