import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:kauction/app/home_bloc/bloc.dart';
import 'package:kauction/model/repositories/home_reporsitory.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeRepository homeRepository;

  HomeBloc({required this.homeRepository}) : super(InitialFeedState()) {
    homeRepository = HomeRepository();
  }

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is Homeequested) {
      yield FeedLoadInProgress();
      final feedResponse = await homeRepository.getInfoHome();
      if (feedResponse != null) {
        yield FeedLoadSuccess(homeResponse: feedResponse);
      } else {
        yield FeedLoadFailure(homeResponse: feedResponse);
      }
    }
  }

  @override
  // TODO: implement initialState
  HomeState get initialState => InitialFeedState();
}
