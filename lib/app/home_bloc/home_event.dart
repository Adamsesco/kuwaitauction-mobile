import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class Homeequested extends HomeEvent {

  Homeequested();

  @override
  List<Object> get props => [];
}
