import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:kauction/views/home/settings/settings.dart';
import 'package:meta/meta.dart';

part 'bottom_navigation_event.dart';

part 'bottom_navigation_state.dart';

class BottomNavigationBloc
    extends Bloc<BottomNavigationEvent, BottomNavigationState> {
  BottomNavigationBloc(
      )
      :
        super(PageLoading());


  int currentIndex = 0;

  @override
  Stream<BottomNavigationState> mapEventToState(
      BottomNavigationEvent event) async* {
    if (event is AppStarted) {
      this.add(PageTapped(index: this.currentIndex));
    }
    if (event is PageTapped) {
      this.currentIndex = event.index;
      yield CurrentIndexChanged(currentIndex: this.currentIndex);
      yield PageLoading();

      if (this.currentIndex == 0) {
        //  String data = await _getFirstPageData();
        // yield FirstPageLoaded(text: data);
      }
      if (this.currentIndex == 1) {
        int data = await _getSecondPageData();
        yield SecondPageLoaded();
      }

      if (this.currentIndex == 2) {
       // int data = await _getSecondPageData();
        yield ThiredPage();
      }

      if (this.currentIndex == 3) {
        // int data = await _getSecondPageData();
        yield ForthPage();
      }

      if (this.currentIndex == 4) {
        // int data = await _getSecondPageData();
        yield LastPage();
      }


    }
  }

  Future _getSecondPageData() async {
    /*int data = secondPageRepository.data;
    if (data == null) {
      await secondPageRepository.fetchData();
      data = secondPageRepository.data;
    }
    return data;*/
  }
}
