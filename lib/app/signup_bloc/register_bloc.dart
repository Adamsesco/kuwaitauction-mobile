import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kauction/app/signin_bloc/signin_bloc.dart';
import 'package:kauction/app/signin_bloc/signin_event.dart';
import 'package:kauction/app/signup_bloc/register_event.dart';
import 'package:kauction/app/signup_bloc/register_state.dart';
import 'package:kauction/app/verify_phone/bloc.dart';
import 'package:kauction/model/repositories/register_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterRepository registerRepository;
  VerifyMemberBloc signInBloc;

  RegisterBloc({required this.registerRepository, required this.signInBloc})
      : super(InitialRegistertate()) {
    registerRepository = RegisterRepository();
  }

  @override
  RegisterState get initialState => InitialRegistertate();

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is RegisterRequested) {
      yield RegisterProgress();

      final result = await registerRepository.registeer_account(event.user);
      if (result.code == 0) {
        yield RegisterFailure(errorCode: 2, errorMessage: result.message);
      } else {
        //  signInBloc = BlocProvider.of<SignInBloc>(event.context);



        //  prefs.setString("session", result.user.session);
        //signInBloc.add(SignInSucccessEvent(user: result.user));
        yield RegisterSuccess(user: result.user);
      }
    }
  }
}
