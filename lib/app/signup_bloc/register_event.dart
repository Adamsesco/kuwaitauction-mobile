import 'package:equatable/equatable.dart';
import 'package:kauction/model/models/user.dart';
import 'package:meta/meta.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class RegisterRequested extends RegisterEvent {
  final User user;
  var context;

  RegisterRequested({required this.user,this.context});

  @override
  List<Object> get props => [user,context];
}