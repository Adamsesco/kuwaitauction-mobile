import 'package:equatable/equatable.dart';
import 'package:kauction/model/models/user.dart';
import 'package:meta/meta.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();
}

class InitialRegistertate extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterProgress extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterFailure extends RegisterState {
  final int errorCode;
  final String errorMessage;

  RegisterFailure({required this.errorCode, required this.errorMessage});

  @override
  List<Object> get props => [errorCode, errorMessage];
}

class RegisterSuccess extends RegisterState {
  final User user;

  RegisterSuccess({required this.user});

  @override
  List<Object> get props => [user];
}
