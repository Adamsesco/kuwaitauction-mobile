import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kauction/app/bottom_navigation_bloc/bottom_navigation_bloc.dart';
import 'package:kauction/app/home_bloc/home_bloc.dart';
import 'package:kauction/app/routes/router.dart';
import 'package:kauction/app/signin_bloc/signin_bloc.dart';
import 'package:kauction/app/signup_bloc/register_bloc.dart';
import 'package:kauction/app/utils/colors_const.dart';
import 'package:kauction/app/verify_phone/bloc.dart';
import 'package:kauction/languages_page.dart';
import 'package:kauction/model/repositories/home_reporsitory.dart';
import 'package:kauction/model/repositories/register_repository.dart';
import 'package:kauction/model/repositories/verify_phone_repository.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:kauction/views/home/home_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

void main() async {
  final HomeRepository homerepository = new HomeRepository();
  VerifyNumberRepository verifyrepository = new VerifyNumberRepository();
  final registerRepository = RegisterRepository();
  VerifyMemberBloc sign = VerifyMemberBloc(verifyRepository: verifyrepository);
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await Firebase.initializeApp();

 /* FirebaseMessaging.instance
      .requestPermission(sound: true, badge: true, alert: true);*/


  runApp(ScreenUtilInit(
      designSize: const Size(390, 844),
      builder: () => MultiBlocProvider(
              providers: [
                BlocProvider<HomeBloc>(
                  create: (_) => HomeBloc(homeRepository: homerepository),
                ),
                BlocProvider<SigninBloc>(
                  create: (_) =>
                      SigninBloc(registerRepository: registerRepository),
                ),
                BlocProvider<VerifyMemberBloc>(
                  create: (_) =>
                      VerifyMemberBloc(verifyRepository: verifyrepository),
                ),
                BlocProvider<BottomNavigationBloc>(
                    create: (context) => BottomNavigationBloc(
                        //  blRepos: BlRepository(),
                        //secondPageRepository: SecondPageRepository(),
                        )),
                BlocProvider<RegisterBloc>(
                  create: (_) => RegisterBloc(
                      registerRepository: registerRepository, signInBloc: sign),
                ),
              ],
              child: EasyLocalization(
                  // key: Key('KAUCTION'),  // <--- add key

                  supportedLocales: [Locale('en'), Locale('ar')],
                  path: 'assets/translations',
                  fallbackLocale: Locale('en'),
                  startLocale: Locale('en'),
                  useOnlyLangCode: true,
                  child: MyApp()))));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String result = "";

  getlanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var lng = prefs.getString("lng");
    if (!this.mounted) return;
    if (lng == "ar") {
      context.locale = Locale('ar');
    } else {
      context.locale = Locale('en');
    }
  }

  getprefl() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String p = prefs.getString("lng").toString();
    if (!this.mounted) return;
    setState(() {
      result = p;
    });


    if (result != "null") {
      getlanguage();
    }
  }

  @override
  void initState() {
    super.initState();
    getprefl();
  }

  @override
  Widget build(BuildContext context) {
    print(context.locale.toString() == "en");

    return MaterialApp(
        title: 'Kauction',
        supportedLocales: context.supportedLocales,
        localizationsDelegates: context.localizationDelegates,
        debugShowCheckedModeBanner: false,
        locale: context.locale,
        theme: ThemeData(
            appBarTheme: AppBarTheme(brightness: Brightness.light),
            visualDensity: VisualDensity.adaptivePlatformDensity,
            fontFamily:
                context.locale.toString() == "en" ? "poppins" : "cairo"),
        onGenerateRoute: AppRouter.generateRoute,
        home: SplashScreen(
            seconds: 4,
            navigateAfterSeconds: result == "null" || result == ""
                ? LanguagesPage()
                : HomeScreen() /*new HomeScreen()*/,
            title: new Text(''),
            useLoader: false,
            image: Hero(
                tag: "DemoTag",
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      "assets/images/logo.svg",
                      width: 40.w,
                    ),
                    Container(
                      width: 8.w,
                    ),
                    Text(
                      "Kauction",
                      style: TextStyle(
                          fontFamily: context.locale == "ar"
                              ? "cairo"
                              : "poppins_medium",
                          color: ColorsConst.col_app,
                          fontSize: 24.5.sp,
                          fontWeight: FontWeight.w900),
                    )
                  ],
                )),
            backgroundColor: Colors.white,
            styleTextUnderTheLoader: new TextStyle(),
            photoSize: 100.0,
            loaderColor:
                Colors.red) /*HomeScreen()*/ /*PhoneVerificationScreen()*/);
  }
}
